package zoomba.lang.test;

import org.junit.Ignore;
import org.junit.Test;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import static org.junit.Assert.assertFalse;

/**
 */
public class FunctionsTest {

    final String BASIC = "samples/test/functions_basic.zm" ;

    final String EXTENDED = "samples/test/functions_extended.zm" ;

    final String THREADING = "samples/test/sys_thread.zm" ;

    final String PROCESS = "samples/test/sys_proc.zm" ;

    final String DATABASE = "samples/test/sys_db.zm" ;

    final String IO = "samples/test/sys_io.zm" ;

    final String WEB = "samples/test/sys_web.zm" ;

    final String TYPES = "samples/test/functions_types.zm" ;

    final String DATE = "samples/test/date.zm" ;

    @Test
    public void testBasicFunctions()  {
        InterpreterTest.danceWithScript( BASIC );
    }

    @Test
    public void testExtendedFunctions()  {
        InterpreterTest.danceWithScript( EXTENDED );
    }

    @Test
    public void testThreadFunctions()  {
        InterpreterTest.danceWithScript( THREADING );
    }

    @Test
    public void testProcessFunctions()  {
        InterpreterTest.danceWithScript( PROCESS );
    }

    @Test
    @Ignore
    public void testDBFunctions()  {
        InterpreterTest.danceWithScript( DATABASE );
    }

    @Test
    public void testIOFunctions()  {
        InterpreterTest.danceWithScript( IO );
    }

    @Test
    public void testWebFunctions()  {
        InterpreterTest.danceWithScript( WEB );
    }

    @Test
    public void testTypeFunctions()  {
        InterpreterTest.danceWithScript( TYPES );
    }

    @Test
    public void testDateFunctions()  {
        InterpreterTest.danceWithScript( DATE );
    }
}
