package zoomba.lang.test;

import org.apfloat.Apfloat;
import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZNumber;

import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;


/**
 */
public class AccessorTest {

    final Object[] arr = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ; ;

    final List list = new ZList(arr);

    @Test
    public void testIntegerIndexAccessor(){
        Function.MonadicContainer c =  ZJVMAccess.getProperty(arr,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length-1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c =  ZJVMAccess.setProperty(arr, 0, 'A');
        assertFalse( c.isNil() );
        assertTrue( c == Function.SUCCESS );
        assertEquals( 'A', arr[0] );


        c = ZJVMAccess.getProperty(list,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.setProperty(list, 1, 'B');
        assertFalse( c.isNil() );
        assertEquals( 'B', list.get(1) );


        c = ZJVMAccess.getProperty(list,"size" );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );
        int size = (int)c.value() ;

        c =  ZJVMAccess.getProperty(list, size - 1 );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(list, size );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

    }

    @Test
    public void testIndexer(){

        final Object[] keysNum = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

        final Object[] valuesChar = new Object[ ]{ 'a', 'b', 'c', 'd', 'e', 'f' } ;

        final Map map = new ZMap(keysNum, valuesChar );

        Function.MonadicContainer c =  ZJVMAccess.getProperty(map,1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Character );

        c = ZJVMAccess.getProperty(map,42);
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c = ZJVMAccess.setProperty(map,42, "42" );
        assertFalse( c.isNil() );
        assertEquals("42", map.get(42) );

        c = ZJVMAccess.getProperty(map,"size");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.size());

        c = ZJVMAccess.getProperty(map,"keys");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.keySet());

        c = ZJVMAccess.getProperty(map,"values");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.values());

        c = ZJVMAccess.getProperty(map,"entries");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.entrySet());
    }

    @Test
    public void testMethodProperty(){

        String numberString  = "0.119079023727583064502346502356025206076076076" ;
        ZNumber zn = new ZNumber(numberString);

        Function.MonadicContainer c =  ZJVMAccess.getProperty(zn.apFloat(),"precision");
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Long );
        assertEquals(c.value() , (long)numberString.length() -2  );

        c =  ZJVMAccess.setProperty(zn.apFloat(),"precision", 1 );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Apfloat );
        assertEquals( ((Apfloat)c.value()).precision(), 1l );

    }
}
