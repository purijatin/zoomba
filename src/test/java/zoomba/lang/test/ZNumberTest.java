package zoomba.lang.test;

import org.apfloat.Apfloat;
import org.apfloat.Apint;
import org.junit.Test;
import zoomba.lang.core.types.ZNumber;
import static org.junit.Assert.*;

/**
 */
public class ZNumberTest {

    @Test
    public void testAssignment(){
        ZNumber zn = new ZNumber(0);

        assertTrue( zn.actual() instanceof Integer );
        zn = new ZNumber(0.00);

        assertTrue( zn.actual() instanceof Double );

        // test auto assign
        zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertFalse( zn.actual() instanceof Apint);
        assertTrue( zn.actual() instanceof Apfloat);

        zn = new ZNumber("0.12101001110");
        assertFalse( zn.actual() instanceof Apint);
        assertTrue( zn.actual() instanceof Double);

        zn = new ZNumber(1134234229);
        assertTrue( zn.actual() instanceof Integer );

        zn = new ZNumber(1134234229998989l);
        assertTrue( zn.actual() instanceof Long );

        zn = new ZNumber("1134234229998989979473948759348759834793");
        assertTrue( zn.actual() instanceof Apint );

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertTrue( n0.equals(d0) );

        ZNumber d01 = new ZNumber(0.01);
        assertFalse( n0.equals(d01) );
        assertFalse( d0.equals(d01) );

        assertEquals( n0.compareTo(d0) , 0 );
        assertEquals( d0.compareTo(d01) , -1 );
        assertEquals( n0.compareTo(d01) , -1 );

    }

    @Test
    public void testAbs(){

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);

        assertEquals( n0._abs_() , 0 );
        assertEquals( d0._abs_() , 0.0 );

        ZNumber d01 = new ZNumber(0.01);
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);
        d01 = new ZNumber(-0.01);
        assertTrue( d01._abs_().doubleValue() > 0 );
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);

        ZNumber zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn._abs_() instanceof Apfloat);

    }

    @Test
    public void testAdd(){
        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertEquals( d0._add_(n0), 0.0 );
        assertEquals( n0._add_(d0), 0 );
        long ln = -123213131312311131l ;
        ZNumber zln = new ZNumber(ln);
        // must upcast?
        Object r = zln._add_(d0);
        assertEquals( ln, r);

    }

    @Test
    public void testPower(){
        ZNumber zn = new ZNumber(12331201231.12321312312301301d);
        long p = 0 ;
        Object o = zn._pow_(p);
        assertEquals( o, 1.0d );
        zn = new ZNumber("121213123182012810280129824109481098");
        o = zn._pow_(p);
        assertEquals( o, 1 );
    }

    @Test
    public void testMultiplication(){
        int i = 6 ;
        ZNumber n = new ZNumber(7);
        Object o = n._mul_(i);
        assertEquals(o,42);
        n = new ZNumber(7.0);
        o = n._mul_(i);
        assertEquals(o,42.0);
        o = n._mul_(6.00);
        assertEquals(o,42.0);
    }

    @Test
    public void testDivision(){
        int i = 6 ;
        ZNumber n = new ZNumber(42);
        Object o = n._div_(i);
        assertEquals(o,7);

        n = new ZNumber(42.0);
        o = n._div_(6);
        assertEquals(o,7.0);

        ZNumber a = new ZNumber("0.11213131010801823013821093221097319873211381981111109099080921");
        ZNumber b = new ZNumber("0.9809809809890119018230981203981309173891481640872630164012746127");
        o = a._div_(b);
        assertTrue( o instanceof Apfloat );
    }
}
