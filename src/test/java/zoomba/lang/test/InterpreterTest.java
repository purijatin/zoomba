package zoomba.lang.test;

import org.junit.Test;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static zoomba.lang.core.interpreter.ZInterpret.NANO_TO_SEC;

/**
 */
public class InterpreterTest {

    final String BASIC = "samples/test/basic.zm" ;

    final String CONDITION = "samples/test/conditions.zm" ;

    final String ITERATORS = "samples/test/iterators.zm" ;

    final String FUNCTIONS = "samples/test/functions.zm" ;

    final String PREDICATES = "samples/test/predicates.zm" ;

    final String NUMBERS = "samples/test/numbers.zm" ;

    final String PERFORMANCE = "samples/test/perf.zm" ;

    final String ARITHMETIC = "samples/test/arithmetic.zm" ;

    final String ATOMIC = "samples/test/atomic.zm" ;

    public static void danceWithScript(String scriptLocation){
        ZScript script = new ZScript(scriptLocation,null);
        script.debug(true);
        Function.MonadicContainer mc = script.execute();
        if ( mc.value() instanceof Throwable ){
            System.err.printf("%s\n", mc.value() );
        }
        assertFalse(mc.value() instanceof Throwable);
    }

    public static Function.MonadicContainer returnDance(String scriptLocation){
        ZScript script = new ZScript(scriptLocation,null);
        script.debug(true);
        Function.MonadicContainer mc = script.execute();
        if ( mc.value() instanceof Throwable ){
            System.err.printf("%s\n", mc.value() );
        }
        assertFalse(mc.value() instanceof Throwable);
        return mc;
    }

    @Test
    public void danceWithBasicFile()  {
        danceWithScript(BASIC);
    }

    @Test
    public void danceWithConditionFile()  {
        danceWithScript(CONDITION);
    }

    @Test
    public void danceWithIteratorFile()  {
        danceWithScript(ITERATORS);
    }

    @Test
    public void danceWithFunctionsFile()  {
        danceWithScript(FUNCTIONS);
    }

    @Test
    public void danceWithPredicatesFile()  {
        danceWithScript(PREDICATES);
    }

    @Test
    public void danceWithNumbersFile()  {
        danceWithScript(NUMBERS);
    }

    @Test
    public void danceWithArithmetic()  {
        danceWithScript(ARITHMETIC);
    }

    @Test
    public void danceWithAtomic()  {
        danceWithScript(ATOMIC);
    }

    int testJava(){
        int j = 0 ;
        for ( int i = 0 ; i < 1000000 ; i++ ){
            j = i ;
        }
        return j;
    }

    public static final int MAX_LIMIT = 200 ;

    @Test
    public void dancePerformance()  {
        Function.MonadicContainer mc = returnDance(PERFORMANCE);
        Number num = (Number)mc.value();
        long cur = System.nanoTime();
        testJava();
        long delta = System.nanoTime() - cur ;
        double doubleDeltaInSec = delta * NANO_TO_SEC ;
        System.out.printf("Java %s (sec): ZoomBA %s (sec)\n", doubleDeltaInSec, mc.value());
        double ratio = num.doubleValue()/doubleDeltaInSec ;
        System.out.printf("Java is %s times faster than ZoomBA with limit %d\n", ratio, MAX_LIMIT );
        assertTrue( (int)ratio < MAX_LIMIT );
    }
}
