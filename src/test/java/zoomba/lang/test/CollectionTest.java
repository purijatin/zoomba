package zoomba.lang.test;

import org.junit.Test;
import zoomba.lang.core.collections.Combinatorics;

/**
 */
public class CollectionTest {

    final String COLLECTION_BASIC = "samples/test/collection_basic.zm" ;

    final String MAP_BASIC = "samples/test/map_basic.zm" ;

    final String FILE_BASIC = "samples/test/file_basic.zm" ;

    final String STRUCTURED_DATA = "samples/test/structured_data.zm" ;

    final String PROTO_TEST = "samples/test/proto_class.zm" ;

    final String ITERATOR_TEST = "samples/test/iterator_method.zm" ;

    final String CARDINALITY_TEST = "samples/test/cardinality.zm" ;

    final String EXOTIC_TEST = "samples/test/collections_exotic.zm" ;

    final String LOGICAL = "samples/test/collections_logical.zm" ;

    final String RANDOM = "samples/test/collections_random.zm" ;

    final String SORTED = "samples/test/collection_sorted.zm" ;

    final String COMBINATORICS = "samples/test/combinatorics.zm" ;

    @Test
    public void testCollections()  {
        InterpreterTest.danceWithScript( COLLECTION_BASIC );
    }

    @Test
    public void testMap()  {
        InterpreterTest.danceWithScript( MAP_BASIC );
    }

    @Test
    public void testFiles()  {
        InterpreterTest.danceWithScript( FILE_BASIC );
    }

    @Test
    public void testStructuredData()  {
        InterpreterTest.danceWithScript( STRUCTURED_DATA );
    }

    @Test
    public void testProtoObject()  {
        InterpreterTest.danceWithScript( PROTO_TEST );
    }

    @Test
    public void testIteratorMethods()  {
        InterpreterTest.danceWithScript( ITERATOR_TEST );
    }

    @Test
    public void testCardinality()  {
        InterpreterTest.danceWithScript( CARDINALITY_TEST );
    }

    @Test
    public void testExotic()  {
        InterpreterTest.danceWithScript( EXOTIC_TEST );
    }

    @Test
    public void testLogical()  {
        InterpreterTest.danceWithScript( LOGICAL );
    }

    @Test
    public void testRandom()  {
        InterpreterTest.danceWithScript( RANDOM );
    }

    @Test
    public void testSortedCollections()  {
        InterpreterTest.danceWithScript( SORTED );
    }

    @Test
    public void testPermutations(){
        InterpreterTest.danceWithScript( COMBINATORICS );
    }
}
