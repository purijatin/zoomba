package zoomba.lang.test;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZException.Break ;
import zoomba.lang.core.types.ZException.Continue ;
import java.util.*;

/**
 */
public abstract class DummyFunction implements Function {

    public abstract Object returnValue();

    @Override
    public String body() {
        return String.format("def %s(){ %s }", name(), returnValue() );
    }

    @Override
    public MonadicContainer execute(Object...args) {
        return new MonadicContainerBase(returnValue());
    }

    public static final class RandomPredicate extends  DummyFunction {

        private final Random r = new Random();

        @Override
        public Object returnValue() {
            return r.nextBoolean() ;
        }

        @Override
        public String name() {
            return "_pr_";
        }
    }

    public static final class BreakFunction implements  Function {

        int breakAt = 0 ;

        public int breakAt(){ return breakAt ; }

        public void breakAt(int iteration){ breakAt = iteration ; }

        @Override
        public String body() {
            return String.format("def %s(){  }", name() );
        }

        @Override
        public MonadicContainer execute(Object...args) {
            int index = (int)args[0];
            if ( index == breakAt ){
                return new ZException.Break("Broken at : " + breakAt );
            }
            return new MonadicContainerBase("Not Broken at : " + index );
        }

        @Override
        public String name() {
            return "_db_" ;
        }
    }

    public static final class ContinueFunction implements  Function {

        Set continueAt  ;

        public Set continueAt(){ return continueAt ; }

        public void continueAt(int... iteration){
            continueAt = new HashSet<>();
            for ( int i : iteration ){
                continueAt.add(i);
            }
        }


        @Override
        public String body() {
            return String.format("def %s(){  }", name() );
        }

        @Override
        public MonadicContainer execute(Object...args) {
            int index = (int)args[0];
            if ( continueAt.contains(index) ){
                return ZException.CONTINUE ;
            }
            return new MonadicContainerBase("At : " + index );
        }

        @Override
        public String name() {
            return "_dc_" ;
        }
    }

    public static final class IdentityMapFunction implements  Function {

        @Override
        public String body() {
            return String.format("def %s(){  }", name() );
        }

        @Override
        public MonadicContainer execute(Object...args) {
            return new MonadicContainerBase( args[1] );
        }

        @Override
        public String name() {
            return "_imf_" ;
        }
    }

    public static final class FoldFunction implements Function{

        @Override
        public String body() {
            return "def _string_(){ str( __args__ ) }" ;
        }

        @Override
        public MonadicContainer execute(Object...args) {
            Object item = args[1];
            StringBuilder buf = null ;
            if ( args[3] instanceof StringBuilder ) {
                buf = (StringBuilder) args[3];
            }
            if ( buf == null ) {
                System.out.printf("%s,", item );
                return Void;
            }
            buf.append(String.valueOf(item)).append(",");
            return new MonadicContainerBase(buf);
        }

        @Override
        public String name() {
            return "_string_";
        }
    }

}

