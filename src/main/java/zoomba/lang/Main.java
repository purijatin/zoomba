package zoomba.lang;

import jline.console.ConsoleReader;
import jline.console.completer.StringsCompleter;
import jline.console.history.FileHistory;
import zoomba.lang.core.interpreter.ZContext.*;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static zoomba.lang.core.interpreter.ZContext.ArgContext.EMPTY_ARGS_CONTEXT;
import static zoomba.lang.core.interpreter.ZContext.EMPTY_CONTEXT;

/**
 */
public class Main {

    public static final String PROMPT = "(zoomba)";

    public static final String QUIT = "//q";

    public static final String VARS = "//v";

    public static final String HELP = "//h";

    public static final String RUN = "//r";


    public static String debugContext(FunctionContext context){
        StringBuilder buf = new StringBuilder();
        buf.append("context : { \n");
        for ( String s : context.map.keySet() ){
            Object o = context.map.get(s);
            String v = String.valueOf( o );
            buf.append(s).append(" => ").append(v);
            if ( o != null ){
                buf.append(" /* ").append( o.getClass() ).append(" */");
            }
            buf.append("\n");
        }
        buf.append("}\n");
        return buf.toString();
    }

    public static void showHelp(Console console){
        console.printf("//h brings this help.\n");
        console.printf("//q quits REPL.\n");
        console.printf("//v shows variables.\n");
        console.printf("//r loads and runs a script from REPL.(Not Implemented)\n");
        console.printf("Enjoy ZoomBA.....\n");
    }

    public static void stepDance() throws Exception {
        Console console = System.console();
        ConsoleReader consoleReader = new ConsoleReader();
        consoleReader.addCompleter(
                new StringsCompleter(
                        new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/keyWords.txt"),
                                StandardCharsets.UTF_8)).lines().collect(Collectors.toList())
                )
        );
        Path historyFile = Paths.get(System.getProperty("user.home"), ".zm_cli_history");

        if (!historyFile.toFile().exists()) {
            Files.createFile(historyFile);
        }
        FileHistory history = new FileHistory(historyFile.toFile());

        consoleReader.setHistory( history );
        consoleReader.setHistoryEnabled(true);
        consoleReader.setExpandEvents(false);
        consoleReader.setPrompt(PROMPT);

        ZScript zs ;
        Function.MonadicContainer c;
        FunctionContext replContext = new FunctionContext(EMPTY_CONTEXT, EMPTY_ARGS_CONTEXT);
        showHelp(console);
        while (true) {
            String txt = consoleReader.readLine();
            txt = txt.trim();
            if ( txt.startsWith(QUIT) ) break;
            if ( VARS.equals(txt) ){
                // show variables
                console.printf("%s\n", debugContext(replContext) );
                continue;
            }
            if ( HELP.equals(txt) ){
                showHelp(console);
                continue;
            }

            try {
                zs = new ZScript(txt);
                zs.setExternalContext(replContext);
                c = zs.execute();
                if ( c.isNil() ) continue;
                Object o = c.value();
                String v = ZTypes.string(o);
                String t = ZInterpret.type(o).getSimpleName() ;
                console.printf("%s // %s\n", v, t );
                history.flush();
            } catch (Throwable t) {
                Throwable underlying = t.getCause();
                if ( underlying instanceof ZException ){
                    if ( underlying instanceof ZException.Parsing ) {
                        ZException.Parsing p = (ZException.Parsing) underlying;
                        console.printf("Parse Error: %s\n", p.errorMessage);
                        console.printf("Options were : %s\n", ZTypes.string(p.correctionOptions));
                        continue;
                    }
                }
                console.printf("%s\n", underlying != null ? underlying  : t );
            }
        }
    }

    public static void dance(String[] args) {
        String file = args[0];
        ZScript zs = null;
        try {
            zs = new ZScript(file, null);
        }catch (Throwable t){
            if ( t.getCause() instanceof ZException.Parsing ){
                ZException.Parsing p = (ZException.Parsing)t.getCause();
                System.err.printf("Parse Error: %s\n", p.errorMessage );
                System.err.printf("Options were : %s\n", ZTypes.string(p.correctionOptions) );
            }else{
                System.err.printf("Error in parsing due to : %s\n", t.getCause() );
            }
            System.exit(2);
        }
        Object[] scripArgs = ZTypes.shiftArgsLeft(args);
        Function.MonadicContainer mc = zs.execute(scripArgs);
        if (mc.isNil()) {

        } else {
            if (mc.value() instanceof Throwable) {
                Throwable underlying = (Throwable) mc.value();
                if ( underlying instanceof ZException){
                    if ( underlying instanceof ZException.ZTerminateException ){
                        System.exit(3);
                    }
                    System.err.println(underlying);
                    System.exit(4);
                }
                underlying.printStackTrace();
                System.exit(1);
            }
        }
        System.exit(0);
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            stepDance();
        } else {
            dance(args);
        }
    }
}
