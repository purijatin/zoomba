package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.nio.file.*;
import java.util.Iterator;
import java.util.List;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class ZFileSystem {

    public static class ZFItemIterator implements Iterator {

        public final File file;

        ZArray.ArrayIterator filesIterator;

        final boolean isDict;

        BufferedReader br;

        String line;

        public ZFItemIterator(File f) {
            file = f;
            if (!file.exists())
                throw new UnsupportedOperationException("Can not create iterator on non existent file!");
            if (file.isDirectory()) {
                filesIterator = new ZArray.ArrayIterator(file.listFiles());
                isDict = true;
                br = null;
            } else {
                try {
                    br = new BufferedReader(new FileReader(file));
                    line = br.readLine();
                    isDict = false;
                } catch (Exception e) {
                    throw new UnsupportedOperationException(e);
                }
            }
        }

        @Override
        public boolean hasNext() {
            if (isDict) return filesIterator.hasNext();
            return line != null;
        }

        @Override
        public Object next() {
            if (isDict) return filesIterator.next();
            String oldLine = line;
            line = null;
            try {
                line = br.readLine();
            } catch (Throwable t) {
            }
            return oldLine;
        }

    }

    public static class ZFile implements Iterable {

        public final File file;

        public ZFile(String path) {
            this.file = new File(path);
            ;
        }

        @Override
        public Iterator iterator() {
            return new ZFItemIterator(file);
        }

        public String all() throws Exception {
            Path p = file.toPath();
            byte[] bytes = Files.readAllBytes(p);
            return new String(bytes);
        }

        public List<String> lines() throws Exception {
            Path p = file.toPath();
            List<String> lines = Files.readAllLines(p);
            return lines;
        }

        public Object get(String value) throws Exception {
            Path p = file.toPath();
            return Files.getAttribute(p, value, LinkOption.NOFOLLOW_LINKS);
        }

        @Override
        public String toString() {
            return file.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ZFile zFile = (ZFile) o;

            return file.equals(zFile.file);

        }

        @Override
        public int hashCode() {
            return file.hashCode();
        }
    }

    public static ZFile file(String path) {
        return new ZFile(path);
    }

    public static Closeable open(Object... args) {
        if (args.length == 0) return System.in;
        String path = "";
        if (args[0] instanceof File) {
            path = args[0].toString();
        } else {
            path = String.valueOf(args[0]);
        }
        String mode = "r";
        if (args.length > 1) {
            mode = String.valueOf(args[1]);
        }
        mode = mode.toLowerCase();
        try {
            switch (mode) {
                case "w":
                    return new PrintStream(new FileOutputStream(path));
                case "a":
                    return new PrintStream(new FileOutputStream(path, true), true);

                case "r":
                default:
                    return new BufferedReader(new FileReader(path));
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static Path write(Object... args) {

        if (args.length == 0) {
            throw new UnsupportedOperationException("Can not write to nothing!");
        }
        try {
            if (args.length == 1) {
                return Files.write(new File(String.valueOf(args[0])).toPath(), new byte[]{});
            }
            boolean append = false;
            if ( args.length > 2 ){
                append = ZTypes.bool(args[2], false);
            }

            if (append) {
                return Files.write(new File(String.valueOf(args[0])).toPath(),
                        String.valueOf(args[1]).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
            return Files.write(new File(String.valueOf(args[0])).toPath(), String.valueOf(args[1]).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object read(Object... args) throws Exception {
        if (args.length == 0) return Function.NIL;
        ZFile zf = new ZFile(String.valueOf(args[0]));
        if (args.length == 1) {
            return zf.all();
        }
        if (args[1] instanceof Boolean) {
            if ((boolean) args[1]) {
                return zf.lines();
            }
        }
        return Function.NIL;
    }
}
