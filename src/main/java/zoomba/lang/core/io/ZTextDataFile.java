package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.ZMatrix;
import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;

/**
 */
public class ZTextDataFile extends ZMatrix.BaseZMatrix{

    public static final class ZTextDataFileLoader implements ZMatrixLoader{

        public static final ZTextDataFileLoader LOADER = new ZTextDataFileLoader();

        private ZTextDataFileLoader(){}

        @Override
        public ZMatrix load(String path,Object...args) {
            String sep = "\t" ;
            if ( args.length != 0 ){
                sep = String.valueOf(args[0]);
            }
            return new ZTextDataFile(path,sep);
        }

        @Override
        public Pattern loadPattern() {
            return Pattern.compile("^.+\\.(csv|tsv|txt|text)$", Pattern.CASE_INSENSITIVE );
        }
    }

    public final List<String> lines ;

    public final String sep;

    public final List<String> headers;

    public ZTextDataFile(String path,String sep){
        File f = new File(path);
        try {
            lines = Files.readAllLines(f.toPath() );
            this.sep = sep ;
            headers = new ZArray( lines.get(0).split(sep,-1),false );
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    Map<Integer,List> cachedData = new WeakHashMap<>();

    @Override
    public List row(int rowNum) {
        if ( cachedData.containsKey( rowNum ) ){
            return cachedData.get(rowNum);
        }
        List l = new ZArray( lines.get(rowNum).split(sep,-1),false ) ;
        cachedData.put(rowNum,l);
        return l;
    }

    @Override
    public int rows() {
        return lines.size() ;
    }

}
