package zoomba.lang.core.io;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 */
public class ZWeb {

    public static final String MY_AGENT
     = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11" ;

    public static final class ZWebCom{

        public final String path;

        public final Map map;

        public final String body;

        public final int status;

        ZWebCom(String p, Map m, String b, int status){
            path = p ;
            map = m ;
            body = b ;
            this.status = status;
        }

        ZWebCom(String p, Map m, String b){
            this(p,m,b,0);
        }
    }

    public final URL url;

    public ZWeb(Object o) {
        if ( o instanceof CharSequence ){
            try {
                url = new URL(o.toString());
            }catch (Exception e){
                throw new UnsupportedOperationException(e);
            }
        }else if ( o instanceof URL ){
            url = (URL)o;
        }else {
            throw new UnsupportedOperationException("Invalid type for URL!");
        }
        connectionProperties = new HashMap<>();
        connectionProperties.put("User-Agent", MY_AGENT);
    }

    public Map<String,String>  connectionProperties ;

    public int conTO ;

    public int readTO ;

    public static String payLoad(Map m) throws Exception {
        if ( m.isEmpty() ) return "" ;
        Iterator<Map.Entry> i = m.entrySet().iterator();
        StringBuilder buf = new StringBuilder();
        Map.Entry e = i.next();
        String v = URLEncoder.encode(e.getValue().toString(), "UTF-8");
        buf.append(e.getKey()).append("=").append(v);
        while ( i.hasNext() ){
            buf.append("&");
            v = URLEncoder.encode(e.getValue().toString(), "UTF-8");
            buf.append(e.getKey()).append("=").append(v);
        }
        return buf.toString();
    }

    public static ZWebCom readUrl(String urlString, int conTO, int readTO) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection  con = (HttpURLConnection)url.openConnection();
        con.setConnectTimeout(conTO);
        con.setReadTimeout(readTO);
        con.setRequestProperty("User-Agent", MY_AGENT );
        try {
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        StringBuilder buf = new StringBuilder();
        String inputLine ;
            while ((inputLine = in.readLine()) != null)
                buf.append(inputLine).append("\n");
            in.close();
            String resp = buf.toString();
            ZWebCom zc = new ZWebCom("", con.getHeaderFields(), resp, con.getResponseCode());
            return zc;
        }catch (Exception e){
            return new ZWebCom("", con.getHeaderFields(), "", con.getResponseCode());
        }
    }

    public ZWebCom readUrl(ZWebCom payLoad ) throws Exception {
        String myUrl = url.toString();
        if ( !payLoad.path.isEmpty()  ){
            myUrl += "/" + payLoad.path ;
        }
        String params = payLoad(payLoad.map);

        if ( !params.isEmpty() ){
            myUrl += "?" + params ;
        }
        return readUrl(myUrl,conTO, readTO );
    }

    public ZWebCom get( String path ) throws Exception {
        return send("get",path, Collections.EMPTY_MAP, "");
    }

    public ZWebCom get( String path, Map m ) throws Exception {
        return send("get",path, m, "");
    }

    public ZWebCom post( String path, Map m , String body ) throws Exception {
        return send("post",path, m, body );
    }

    public ZWebCom post( String path, Map m ) throws Exception {
        return post(path, m, "" );
    }

    public ZWebCom send(String protocol, String path, Map m, String body) throws Exception {
        return send(protocol, new ZWebCom(path,m,body));
    }

    public ZWebCom send(String protocol, ZWebCom payLoad) throws Exception {
        protocol = protocol.toUpperCase();
        String requestMethod = "" ;
        switch (protocol){
            case "HEAD" :
                requestMethod = "HEAD" ;
                break;
            case "PUT" :
                 requestMethod = "PUT" ;
                break;
            case "POST" :
                requestMethod = "POST" ;
                break;
            case "TRACE" :
                requestMethod = "TRACE" ;
                break;
            case "OPTIONS" :
                requestMethod = "OPTIONS" ;
                break;
            case "CONNECT" :
                requestMethod = "CONNECT" ;
                break;
            case "PATCH" :
                requestMethod = "PATCH" ;
                break;

            case "DELETE" :
                requestMethod = "DELETE" ;
                break;
            case "GET" :
            default:
                return readUrl( payLoad );
         }
        String urlPath = url.toString();
        if ( !payLoad.path.isEmpty() ){
            urlPath += (payLoad.path.startsWith("/")? "" : "/") + payLoad.path ;
        }
        String urlParameters = payLoad(payLoad.map );
        URL obj = new URL(urlPath);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(requestMethod);

        con.setConnectTimeout(conTO);
        con.setReadTimeout(readTO);
        for ( String prop : connectionProperties.keySet() ){
            con.setRequestProperty(prop, connectionProperties.get(prop) );
        }
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        try {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine).append("\n");
            }
            in.close();
            ZWebCom zc = new ZWebCom("", con.getHeaderFields(),
                    response.toString(), con.getResponseCode());
            return zc;
        }catch (Exception e){
            return new ZWebCom("", con.getHeaderFields(), "", con.getResponseCode());
        }
    }
}
