package zoomba.lang.core.types;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * Arbitrary large range in ZoomBA
 * With L : ApiInt support
 */
public class ZXRange extends ZRange{

    final ZNumber b;

    final ZNumber e;

    final ZNumber s;

    ZNumber c;

    final boolean decreasing;

    @Override
    public void reset() {
        c = new ZNumber(b.num);
    }

    public ZXRange(ZNumber b, ZNumber e, ZNumber s) {
        super();
        this.b = b ;
        this.e = e ;
        this.s = s ;
        decreasing = ( b.compareTo( e) > 0 );
        reset();
    }

    public ZXRange(ZNumber b, ZNumber e) {
        super();
        this.b = b ;
        this.e = e ;
        decreasing = ( b.compareTo( e) > 0 );
        if ( decreasing ) {
            s = new ZNumber(-1);
        }else{
            s = new ZNumber(1);
        }
        reset();
    }

    @Override
    public ZRange yield() {
        return new ZXRange( b, e, s );
    }

    @Override
    public Object object(long l) {
        return NIL;
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]" , b,e,s);
    }

    @Override
    public void toEnd() {
        c = new ZNumber(e);
        c.sub_mutable( s );
    }

    @Override
    public boolean hasNext() {
        if ( decreasing ) {
            return c.compareTo( e ) > 0 ;
        }
        return c.compareTo(e) < 0 ;
    }

    @Override
    public Object next() {
        Number next = c.actual() ;
        c.add_mutable(s);
        return next ;
    }

    @Override
    public Object get(Number n) {
        // n * s + b
        ZNumber l =   new ZNumber( n );
        l.mul_mutable(s);
        l.add_mutable( b );

        if ( decreasing ){
            if ( l.compareTo( e ) >= 0 ) return l.actual();
            return NIL;
        }
        if ( l.compareTo( e ) <= 0 ) return l.actual();
        return NIL;
    }

    @Override
    public boolean hasPrevious() {
        if ( decreasing ) {
            return c.compareTo( b ) <= 0 ;
        }
        return c.compareTo(b) >= 0 ;
    }

    @Override
    public Object previous() {
        Number prev = c.actual() ;
        c.sub_mutable(s);
        return prev ;
    }
}
