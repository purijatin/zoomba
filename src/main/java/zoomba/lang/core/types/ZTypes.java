package zoomba.lang.core.types;

import org.apfloat.Apint;
import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.parser.ASTPatternLiteral;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

import static zoomba.lang.core.operations.Function.FAILURE;
import static zoomba.lang.core.operations.Function.NIL;
import static zoomba.lang.core.operations.Function.SUCCESS;

/**
 */
public class ZTypes {

    public interface StringX {

        String string(Object... args);

    }

    public static boolean isa(Object a, Object b) {
        if (a == null) return (b == null);
        if (b == null) return false;
        Class ca = null;
        if (!(a instanceof Class)) {
            ca = a.getClass();
        }
        if (b instanceof Class) {
            return ((Class) b).isAssignableFrom(ca);
        } else {
            if (b instanceof ASTPatternLiteral) {
                ASTPatternLiteral pl = (ASTPatternLiteral) b;
                return pl.matches(ca.getName());
            }
        }
        Class cb = b.getClass();
        return cb.isAssignableFrom(ca);
    }

    public static Boolean bool(Object... args) {

        if (args.length == 0) return null;
        if (args[0] == null) {
            if (args.length > 1) {
                return bool(args[1]);
            }
            return null;
        }

        if (args[0] instanceof Boolean) {
            return (Boolean) args[0];
        }
        Boolean fallBack = null;
        if (args.length > 1) {
            if ( args[1] !=null && args[1].getClass().isArray()){
                args[1] = new ZArray(args[1],false);
            }
            if (args[1] instanceof List) {
                List l = (List)args[1];
                if (l.size() < 2) return null;
                Object f = l.get(0);
                Object t = l.get(1);
                if (args[0].equals(t)) return true;
                if (args[0].equals(f)) return false;
                return null;
            }
            fallBack = bool(args[1]);
        }
        if (args[0] instanceof CharSequence) {
            String s = String.valueOf(args[0]);
            s = s.toLowerCase().trim();
            if ("true".equals(s)) return true;
            if ("false".equals(s)) return false;
            return fallBack;
        }
        return fallBack;
    }

    public static String string(Iterable c, String sep) {
        if ( !c.iterator().hasNext() ) return "";
        StringBuilder buf = new StringBuilder();
        Iterator i = c.iterator();
        Object o = i.next();
        buf.append(String.valueOf(o));
        while (i.hasNext()) {
            o = i.next();
            buf.append(sep).append(String.valueOf(o));
        }
        return buf.toString();
    }

    public static String string(Function map, Object col, String sep) {
        if ( col == null ) return "" ;
        Iterable c ;
        if ( col instanceof Iterator ){
            c = BaseZCollection.fromIterator( (Iterator)col );
        }else if ( col instanceof Iterable ){
            c = (Iterable)col;
        }else{
            if ( col instanceof CharSequence ){
                col = col.toString().toCharArray();
            }
            c = new ZArray(col,false);
        }

        if ( !c.iterator().hasNext() ) return "";
        StringBuilder buf = new StringBuilder();
        int index = 0 ;
        Iterator i = c.iterator();
        Object o = i.next();
        Function.MonadicContainer result = map.execute( index, o, c, NIL );
        if ( !result.isNil() ){
            buf.append(String.valueOf(result.value()));
        }
        while (i.hasNext()) {
            index++;
            o = i.next();
            result = map.execute( index, o, c, NIL );
            if ( !result.isNil() ){
                buf.append(sep).append(String.valueOf(result.value()));
            }
            if ( result instanceof ZException.Break ) { break; }
        }
        return buf.toString();
    }

    public static String string(Object... args) {
        if (args.length == 0) return "";
        if (args[0] == null) return "null";
        if ( args[0] instanceof ZString ){
            return args[0].toString();
        }
        if  ( args[0] instanceof Date ){
            ZDate dt = new ZDate((Date) args[0]);
            args = shiftArgsLeft(args);
            return dt.string(args);
        }
        if (args[0] instanceof Number) {
            if ( Arithmetic.isInfinity(args[0]) ) return String.valueOf(args[0]);
            Number n = (Number) args[0];
            args = shiftArgsLeft(args);
            if (n instanceof ZNumber) {
                return ((ZNumber) n).string(args);
            }
            ZNumber zn = new ZNumber(n);
            return zn.string(args);
        }
        Iterable c = null;
        if (args[0].getClass().isArray()) {
            c = new ZArray(args[0], false);
        } else if (args[0] instanceof Iterable) {
            c = (Iterable) args[0];
        }
        String sep = ",";
        if (args.length > 1) {
            sep = String.valueOf(args[1]);
            if ( args.length > 2 && args[2] instanceof Function ){
                return string((Function)args[2], c, sep );
            }
        }
        if (args[0] instanceof StringX) {
            StringX sx = (StringX)args[0];
            args = shiftArgsLeft(args);
            return sx.string(args);
        }

        if (args[0] instanceof String) {
            String fmt = (String) args[0];
            if (args.length > 1 && fmt.contains("%")) {
                // confirm
                args = shiftArgsLeft(args);
                return String.format(fmt, args);
            }
        }
        // rest fall back
        if (c != null) {
            if ( args.length > 2 ){
                return jsonString(c);
            }
            return string(c, sep);
        }
        if ( args.length > 1 ){
            return jsonString(args[0]);
        }
        return String.valueOf(args[0]);
    }

    public static boolean equals(Object o1, Object o2) {
        if ( o1 == null ){
            if ( o2 == null ) return true ;
            return false ;
        }
        if ( o2 == null) return false ;
        try {
            if (o1 instanceof Comparable) {
                // java is retarded. Even if o1 is not null, if o2 is null it throws nullpointer. How pathetic is that?
                // mathematically, x.compareTo(null) must return 1, anything is greater than null.
                return (((Comparable) o1).compareTo(o2) == 0);
            }
        }catch (Throwable e){
            // yep, can not compare...
        }
        return Objects.equals(o1, o2);
    }

    public static Object[] shiftArgsLeft(Object[] args) {
        if (args.length == 0) return args;
        Object[] newArgs = new Object[args.length - 1];
        for (int i = 0; i < newArgs.length; i++) {
            newArgs[i] = args[i + 1];
        }
        return newArgs;
    }

    public static ZRange range(Object... args) {
        if (args[0] instanceof Number) {
            boolean xr = args[0] instanceof Apint || args[1] instanceof Apint ;
            if ( xr ){
                ZNumber b = new ZNumber( args[0].toString() );
                ZNumber e = new ZNumber( args[1].toString() );
                if (args.length == 3) {
                    ZNumber s = new ZNumber( args[2].toString());
                    return new ZXRange(b, e, s);
                }
                return new ZXRange( b,e);
            }

            long b = ((Number) args[0]).longValue();
            long e = Long.valueOf(args[1].toString());

            if (args.length == 3) {
                long s = Long.valueOf(args[2].toString());
                return new ZRange.NumRange(b, e, s);
            }
            return new ZRange.NumRange(b, e);
        }
        if (args[0] instanceof String) {
            char b = args[0].toString().charAt(0);
            char e = args[1].toString().charAt(0);

            if (args.length == 3) {
                int s = Integer.valueOf(args[2].toString());
                return new ZRange.CharRange(b, e, s);
            }
            return new ZRange.CharRange(b, e);
        }
        if (args[0] instanceof ZDate ) {
            ZDate s = (ZDate)args[0];
            ZDate e = (ZDate)args[1];
            if ( args.length > 2 ){
                String d = String.valueOf( args[2] );
                return new ZRange.DateRange(s,e,d);
            }
            return new ZRange.DateRange(s,e);
        }
        return ZRange.EMPTY_RANGE;
    }

    private final static String DEFAULT_EXCEPTION_MESSAGE = "Vogons destroyed Earth!" ;

    public static void raise(Object...args) {
        if ( args.length == 0 ){
            throw new RuntimeException( DEFAULT_EXCEPTION_MESSAGE );
        }
        String exceptClassName = String.valueOf( args[0] );
        Class exceptClass;
        try{
            exceptClass =  Class.forName( exceptClassName );
        }catch (Exception e){
            // treat like message
            throw new ZAssertionException(exceptClassName);
        }
        if ( !Throwable.class.isAssignableFrom( exceptClass ) ){
                throw new ZAssertionException( exceptClass.getName() );
        }
        Object[] params = ZTypes.shiftArgsLeft( args );
        Throwable t;
        try {
            t = (Throwable) ZJVMAccess.construct(exceptClass, params);
        }catch (Exception e){
            throw new ZAssertionException(exceptClass.getName());
        }
        if ( t instanceof RuntimeException ) throw (RuntimeException)t;
        throw new ZAssertionException(t);
    }

    public static void bye(Object... args) {
        if (args.length == 0) {
            throw new ZException.ZTerminateException( DEFAULT_EXCEPTION_MESSAGE );
        }
        System.err.printf("%s\n",args[0] );
        throw new ZException.ZTerminateException(String.valueOf(args[0]));
    }

    public enum NilEnum{
        NIL
    }

    public static Object enumCast(Object...args){
        if ( args.length == 0 ) return NilEnum.NIL  ;
        Object o = args[0];
        if ( o instanceof String ){
            try {
                o = Class.forName((String)o);
            }catch (Exception e){
                // may be the whole name is there?
                throw new UnsupportedOperationException(e);
            }
        }
        if ( o instanceof Class ){
            Class ce = (Class)o;
            if ( !ce.isEnum() ){ return NilEnum.NIL ; }
            Object[] ca = ce.getEnumConstants();
            HashMap m = new HashMap();
            for ( int i = 0 ; i < ca.length; i++ ) {
                m.put(ca[i].toString(), ca[i]);
                m.put(i, ca[i]);
            }
            Map em = Collections.unmodifiableMap(m);
            if ( args.length > 1 ){
                return em.get(args[1]);
            }
            return em;
        }
        return NilEnum.NIL ;
    }

    public static Object json(Object... args) {
        try {
            if (args.length == 0) return Collections.EMPTY_MAP;
            if (args.length > 1) {
                boolean file = bool(args[1], false);
                if (file) {
                    Object o = ZMethodInterceptor.Default.read(args[0]);
                    String text ;
                    if ( o instanceof ZWeb.ZWebCom ){
                        text = ((ZWeb.ZWebCom) o).body;
                    }else{
                        text = String.valueOf(o);
                    }
                    args[0] = text ;
                }
            }

            if (args[0] instanceof CharSequence) {
                String f = args[0].toString();
                // format when needed
                //{} --> { : }
                f = f.replaceAll("\\{\\S*\\}", "{:}");
                f = f.replace("\r", "");
                f = f.replace("\n", "");

                ZScript zs = new ZScript(f);
                Function.MonadicContainer mc = zs.execute();
                if (mc.value() instanceof Throwable) {
                    throw (Throwable) mc.value();
                }
                return mc.value();
            }
        } catch (Throwable t) {
            throw new ZException(t, "Error in json()!");
        }
        return Collections.EMPTY_MAP;
    }

    public static String jsonString(Iterable i) {
        StringBuilder buf = new StringBuilder();
        Iterator iterator = i.iterator() ;
        if ( ! iterator.hasNext() ) return  "[]";
        buf.append( "[ " ) ;
        Object o = iterator.next() ;
        buf.append( jsonString(o) );
        while ( iterator.hasNext() ){
            o = iterator.next();
            buf.append(", ").append( jsonString(o) );
        }
        buf.append( " ]" ) ;
        return buf.toString();
    }

    public static String jsonString(Map m) {
        if ( m.isEmpty() ) return "{}" ;

        StringBuilder buf = new StringBuilder();
        buf.append( "{" ) ;
        Iterator<Map.Entry> i = m.entrySet().iterator() ;
        Map.Entry e = i.next();
        String key = jsonString(e.getKey());
        String value = jsonString(e.getValue());
        buf.append(key).append(" : ").append(value);
        while ( i.hasNext() ){
            e = i.next();
            key = jsonString(e.getKey());
            value = jsonString(e.getValue());
            buf.append(", ").append(key).append(" : ").append(value);
        }
        buf.append( "}" ) ;
        return buf.toString();
    }

    public static String jsonString(Object a) {
        if (a == null) return "null";
        if (a instanceof CharSequence) {
            return "\"" + a.toString() + "\"";
        }
        if (a instanceof Number) {
            ZNumber zn;
            if ( a instanceof ZNumber ){
                zn = (ZNumber)a;
            }else {
                zn = new ZNumber(a);
            }
            return String.valueOf(zn.actual());
        }
        if ( a.getClass().isArray() ){
            a = new ZArray(a,false);
        }

        if (a instanceof Iterable) {
            return  jsonString((Iterable) a) ;
        }

        if (a instanceof Map) {
            return jsonString((Map)a);
        }
        //  I do not know how to handle this, so...
        return String.format(" { \"%s\" : \"%s\" } ", a.getClass().getName(), a );
    }

    public static Function.MonadicContainer loadJar(Object... args){
        try {
            File file = new File(String.valueOf(args[0]));
            if ( !file.exists() ) {
                System.err.printf("Load Jar : File : %s : PATH DOES NOT EXIST!\n", file.getAbsolutePath() );
                return FAILURE;
            }
            URL url = file.toURI().toURL();
            URLClassLoader classLoader = (URLClassLoader)ClassLoader.getSystemClassLoader();
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(classLoader, url);
            return SUCCESS;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.err.println("Load Jar : UnSuccessful !!!" );
        return FAILURE ;
    }
}
