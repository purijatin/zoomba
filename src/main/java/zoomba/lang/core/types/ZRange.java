package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.operations.Function;

import java.time.Duration;
import java.util.*;
import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public abstract class ZRange implements ListIterator {

    public class ZRangeList implements List{

        public final ZRange zRange ;

        public ZRangeList(ZRange range) {
            zRange = range.yield();
            zRange.reset();
        }

        @Override
        public boolean add(Object o) {
            return false;
        }

        @Override
        public int size() {
            return zRange.size();
        }

        @Override
        public boolean isEmpty() {
            return (size() == 0 );
        }

        @Override
        public boolean contains(Object o) {
            ZRange r  = zRange.yield();
            while ( r.hasNext() ){
                if (  ZTypes.equals( r.next() , o ) ) return true ;
            }
            return false;
        }

        @Override
        public Iterator iterator() {
            ZRange r = zRange.yield();
            r.reset();
            return r;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public Object[] toArray(Object[] a) {
            return new Object[0];
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection c) {
            for ( Object o : c ){
                if ( !contains(o) ) return false ;
            }
            return true ;
        }

        @Override
        public boolean addAll(Collection c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Object get(int index) {
            return  zRange.get( index );
        }

        @Override
        public Object set(int index, Object element) {
            return Function.NIL;
        }

        @Override
        public void add(int index, Object element) {

        }

        @Override
        public Object remove(int index) {
            return Function.NIL;
        }

        @Override
        public int indexOf(Object o) {
            return -1;
        }

        @Override
        public int lastIndexOf(Object o) {
            return -1;
        }

        @Override
        public ListIterator listIterator() {
            return zRange.yield();
        }

        @Override
        public ListIterator listIterator(int index) {
            ZRange r = zRange.yield();
            r.toIndex(index);
            return r;
        }

        @Override
        public List subList(int fromIndex, int toIndex) {
            return Collections.EMPTY_LIST ;
        }

    }

    public static final ZRange EMPTY_RANGE = new NumRange(0,0);

    protected final long begin;

    protected final long end;

    protected final long step;

    protected long current;

    public int size(){
        return (int) ( (end - begin )/step  );
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]", object(begin),object(end), step );
    }

    public List asList(){
        return new ZRangeList(this);
    }

    public List list(){
        ZRange r = yield();
        ZList l = new ZList();
        while ( r.hasNext() ){
            l.add( r.next() );
        }
        return l;
    }

    public void reset(){
        current = begin - step ; // bug here, there is a  possibility of underflow , but, does not matter
    }

    public void toEnd(){
        current = end ;
    }

    public void toIndex(int index){
        long l = begin + index * step ;
        current = l ;
    }

    protected ZRange(){
        begin = end = step = 0 ;
    }

    public ZRange(long b, long e, long s){
        begin = b ;
        end = e ;
        step = s ;
        reset();
    }

    public ZRange(long b, long e){
        begin = b ;
        end = e ;
        if ( b <= e ){
            step = 1 ;
        }else{
            step = -1 ;
        }
        reset();
    }

    public abstract ZRange yield();

    public abstract Object object(long l);

    public Object get(Number n){
        long l = begin + step * n.longValue() ;
        if ( begin <= end ) {
            if (l <= end) return object(l);
            return NIL;
        }
        if (l >= end) return object(l);
        return NIL;
    }

    @Override
    public boolean hasNext() {
        if ( step >= 0 ) return ( current + step < end );
        return current + step > end ;
    }

    @Override
    public Object next() {
        int i = (int)(current += step);
        if ((long)i == current  ){
            return i;
        }
        return current ;
    }

    @Override
    public boolean hasPrevious() {
        if ( step >= 0 ) {
            return (current - step >= begin);
        }
        return (current - step <= begin) ;
    }

    @Override
    public Object previous() {
        int i = (int)(current -= step);
        if ((long)i == current  ){
            return i;
        }
        return current ;
    }

    @Override
    public int nextIndex() {
        return (int)(current + step) ;
    }

    @Override
    public int previousIndex() {
        return (int)(current - step) ;
    }

    @Override
    public void remove() { }

    @Override
    public void set(Object o) { }

    @Override
    public void add(Object o) { }

    public static class NumRange extends ZRange{

        @Override
        public Object object(long l) {
            int i = (int)(l);
            if ((long)i == l  ){
                return i;
            }
            return l;
        }

        public NumRange(long b, long e, long s){
            super(b,e,s);
        }

        public NumRange(long b, long e){
            super(b,e);
        }

        @Override
        public ZRange yield() {
            NumRange nr = new NumRange(begin,end,step);
            nr.current = current ;
            return nr;
        }
    }

    public static class CharRange extends ZRange {

        String string;

        @Override
        public Object object(long l) {
            return new Character((char) l);
        }

        @Override
        public ZRange yield() {
            CharRange nr = new CharRange((char)begin,(char)end,(char)step);
            nr.current = current ;
            return nr;
        }

        public String string(){
            if ( string == null ){
                CharRange cr = (CharRange) yield();
                StringBuilder buf = new StringBuilder();
                while ( cr.hasNext() ){
                    buf.append(cr.next());
                }
                string = buf.toString();
            }
            return string ;
        }

        public CharRange(char b, char e, int step){
            super(b,e,step);
        }

        public CharRange(char b, char e){
            super(b,e);
        }

        @Override
        public int size() {
            // because the end is inclusive here...
            return super.size() + 1 ;
        }

        @Override
        public boolean hasNext() {
            return (current + step <= end) ;
        }

        @Override
        public Object next() {
            int i = (int)super.next();
            return (char)i;
        }

        @Override
        public Object previous() {
            long l = (long)super.previous();
            return (char)l;
        }
    }

    public static class DateRange extends ZRange{

        public static final long MS_IN_A_DAY = 24 * 60 * 60 * 1000 ;

        @Override
        public Object object(long l) {
            return new Date(l);
        }

        @Override
        public ZRange yield() {
            DateRange dr = new DateRange(begin,end,step);
            dr.current = current ;
            return dr;

        }

        protected DateRange(long b, long e, long s ){
            super(b,e,s);
        }

        public DateRange(Date b, Date e, long s) {
            super(b.getTime(), e.getTime(), s);
        }

        public DateRange(Date b, Date e) {
            this(b, e, MS_IN_A_DAY );
        }

        public DateRange(ZDate b, ZDate e, long s) {
            this(b.date(), e.date(), s);
        }

        public DateRange(ZDate b, ZDate e) {
            this(b, e, MS_IN_A_DAY );
        }

        public DateRange(ZDate b, ZDate e, Duration s) {
            this(b.date(), e.date(), s.toMillis() );
        }

        public DateRange(ZDate b, ZDate e, String s) {
            this(b, e, Duration.parse(s) );
        }

        @Override
        public Object next() {
            long l = (long)super.next();
            return new Date(l);
        }

        @Override
        public Object previous() {
            long l = (long) super.previous();
            return new Date(l);
        }
    }
}
