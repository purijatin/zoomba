package zoomba.lang.core.types;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;
import org.apfloat.Apint;
import org.apfloat.ApintMath;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Arithmetic.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UnknownFormatConversionException;

/**
 */
public class ZNumber extends Number implements
        BasicArithmeticAware, NumericAware , LogicAware , ZTypes.StringX {

    public static Number integer(Object...args){
        if ( args.length == 0 ) return null;
        if ( args.length == 3 ){
            // num,base,default
            try{
                BigInteger i = new BigInteger(String.valueOf(args[0]),
                                      integer(args[1]).intValue() );
                return integer(i);
            }catch (Exception e){
                return integer(args[2]);
            }
        }
        if ( args[0] instanceof Number ){
            if ( args[0] instanceof Integer ){
                return (Number)args[0];
            }

            if ( args[0] instanceof Long ){
                return (Number)args[0];
            }

            Apfloat f;
            if ( args[0] instanceof Apfloat ){
                f = (Apfloat)args[0];
            }else{
                f = new Apfloat(args[0].toString());
            }
            Apint ai = f.truncate();
            long l = ai.longValue();
            if ( Long.MAX_VALUE == l || Long.MIN_VALUE == l ) return ai;
            int i = (int)l;
            if ( (long)i == l ) return i;
            return l;
        }
        try {
            Apfloat f = new Apfloat(args[0].toString());
            return integer(f);
        }catch (Exception e){
            try {
                return integer(args[1]);
            }catch (Exception ee){}
        }
        if ( args[0] instanceof Date ){
            return ((Date)args[0]).getTime() ;
        }
        if ( args[0] instanceof ZDate ){
            return ((ZDate)args[0]).date().getTime() ;
        }
        if ( args[0] instanceof Character ){
            return (int)((Character)args[0]).charValue();
        }
        return null;
    }

    public static Apint INT(Object...args){
        Number n = integer(args);
        if ( n == null) return null;
        return new Apint(n.toString());
    }

    public static Apfloat FLOAT(Object...args){
        Number n = floating(args);
        if ( n == null) return null;
        return new Apfloat(n.toString());
    }

    public Number integer(){
        return integer(num);
    }

    public Number floating(){
        return floating(num);
    }

    public static Number floating(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Character){
            Integer i = (int)((Character)args[0]).charValue() ;
            return i.doubleValue();
        }
        if ( args[0] instanceof Number ){
            Number num = (Number)args[0];
            if ( num instanceof Double || num instanceof Float ){
                return num ;
            }
            if ( num instanceof Integer || num instanceof Long || num instanceof Short || num instanceof Byte ){
                return num.doubleValue();
            }

            Apfloat f;
            if ( num instanceof Apfloat ){
                f = (Apfloat)args[0];
            }else{
                f = new Apfloat( num.doubleValue() );
            }
            double d = f.doubleValue();
            Apfloat df = new Apfloat( d );
            if ( f.equals(df) ) return d;
            // try once again
            Double D = d; // box it
            if ( f.toString(true).equals(D.toString() ) ) return d;
            // finally
            return f;
        }
        try {
            Apfloat f = new Apfloat(String.valueOf(args[0]));
            return floating(f);
        }catch (Exception e){
            try {
                return floating(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    public static Number number(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Number ){
            return (Number)args[0];
        }
        try {
            ZNumber zn = new ZNumber(args[0]);
            return zn.actual();
        }catch (Exception e){
            String s = String.valueOf(args[0]);
            if ( "inf".equalsIgnoreCase(s) ) return Arithmetic.POSITIVE_INFINITY ;
            if ( "-inf".equalsIgnoreCase(s) ) return Arithmetic.NEGATIVE_INFINITY ;

            try {
                return number(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    public static final Apfloat HALF = new Apfloat(0.5);

    public static Number round(Number n, Object...args){
        if ( n instanceof Integer || n instanceof Long || n instanceof Apint ){
            return n;
        }
        int numDigits = 0;
        if ( args.length > 0 ){
            numDigits = integer( args[0], 0 ).intValue() ;
        }

        if ( n instanceof Apfloat ){
            boolean toInt = false ;
            if ( numDigits < 1 ){
                numDigits = 1 ;
                toInt = true ;
            }
            int sign = ((Apfloat)n).signum() ;
            RoundingMode mode ;
            if ( args.length < 2 ){
                if ( sign > 0 ){
                    mode = RoundingMode.HALF_UP ;
                }else{
                    mode = RoundingMode.HALF_DOWN ;
                }
            }else{
                mode = (RoundingMode)ZTypes.enumCast( RoundingMode.class , String.valueOf( args[1] ) );
            }
            Apfloat f =  ApfloatMath.round((Apfloat)n, numDigits , mode );
            if ( toInt ){

                if ( sign > 0 ){
                    f = f.add( HALF ).truncate();
                }
                else if ( sign < 0 ){
                    f = f.subtract( HALF ).truncate();
                }else{
                    return 0.0;
                }
            }
            return floating(f);
        }
        if ( n instanceof ZNumber ){
            return round(((ZNumber) n).num, numDigits );
        }
        return round( new ZNumber(n), numDigits );
    }

    public static Number floor(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof Apint ){
            return n;
        }
        if ( n instanceof Apfloat ){
            Apfloat f = ApfloatMath.floor((Apfloat)n);
            return integer(f);
        }
        if ( n instanceof ZNumber ){
            return floor(((ZNumber) n).num);
        }
        return floor(new ZNumber(n));
    }

    public static Number ceil(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof Apint ){
            return n;
        }
        if ( n instanceof Apfloat ){
            Apint i = ApfloatMath.ceil((Apfloat)n);
            return integer(i);
        }
        if ( n instanceof ZNumber ){
            return ceil(((ZNumber) n).num);
        }
        return ceil(new ZNumber(n));
    }

    public static Number narrowNumber(Apfloat f){
        if ( f instanceof Apint ){
            Apint api = (Apint)f;
            long l = api.longValue();
            if ( l == Long.MAX_VALUE || l == Long.MIN_VALUE ) return api;
            int i = (int)l;
            if ( (long)i == l ) return i;
            return l;
        }
        else{
            // can I get into double?
            double d = f.doubleValue();
            String sd = Double.toString(d);
            String me = f.toString(true);
            if ( me.length() > sd.length() ) return f;
            return d;
        }
    }

    protected Apfloat num;

    private void populateNumber (Number o){
        if (  o instanceof Integer || o instanceof Long ||
                o instanceof Short || o instanceof Byte ){
            num = new Apint( o.longValue() );
        }
        else if ( o instanceof Float || o instanceof Double ) {
            num = new Apfloat(o.toString());
        }
        else if ( o instanceof BigInteger ){
            num = new Apint((BigInteger)o);
        }
        else if ( o instanceof BigDecimal ){
            num = new Apfloat((BigDecimal)o);
        }
        else if ( o instanceof Apfloat ){
            num = (Apfloat) o;
        }else if ( o instanceof ZNumber ){
            num = ((ZNumber) o).num ;
        } else{
            num = new Apfloat(String.valueOf(o));
        }
    }

    private void init(Object o){
        if ( o instanceof Number ){
            populateNumber((Number) o);
        }else if ( o instanceof Date ){
            num = new Apint(((Date) o).getTime());
        } else if ( o instanceof Character ){
            num = new Apint ( (int) (Character) o);
        }
        else{
            String v = String.valueOf(o);
            try {
                num = new Apint(v);
            }catch (Exception e){
                try {
                    num = new Apfloat(v);
                }catch (Throwable t){
                    throw new UnknownFormatConversionException( "The object : [" + o + "] is not Numeric!" );
                }
            }
        }
    }

    public ZNumber(Object o){
       init(o);
    }

    @Override
    public int intValue() {
        return num.intValue();
    }

    @Override
    public long longValue() {
        return num.longValue();
    }

    @Override
    public float floatValue() {
        return num.floatValue();
    }

    @Override
    public double doubleValue() {
        return num.doubleValue();
    }

    @Override
    public Object _add_(Object o) {
        ZNumber zn = new ZNumber(o);
        Apfloat f = num.add(zn.num) ;
        if ( num instanceof Apint && zn.num instanceof Apint){
            f = f.truncate();
        }
        return narrowNumber(f);
    }

    @Override
    public Object _sub_(Object o) {
        ZNumber zn = new ZNumber(o);
        Apfloat f = num.subtract(zn.num);
        if ( num instanceof Apint && zn.num instanceof Apint){
            f = f.truncate();
        }
        return narrowNumber(f);
    }

    @Override
    public Object _mul_(Object o) {
        ZNumber zn = new ZNumber(o);
        Apfloat f = num.multiply( zn.num );
        if ( fractional() || zn.fractional() ){
            return floating(f);
        }
        return integer(f);
    }

    @Override
    public Object _div_(Object o) {
        ZNumber zn = new ZNumber(o);
        if ( zn.fractional() || fractional() ){
            if ( zn.actual() instanceof Apfloat || actual() instanceof Apfloat ){
                long myPrec = num.precision() ;
                long herPrec = zn.num.precision() ;
                long p = herPrec > myPrec ? herPrec : myPrec ;
                num.precision(p);
                return num.divide( zn.num );
            }
            return num.doubleValue() / zn.num.doubleValue() ;
        }else{
            Number r = apInt().divide( zn.apInt() );
            return integer(r);
        }
    }

    @Override
    public Object _pow_(Object o) {
        ZNumber zn = new ZNumber(o);
        if ( num instanceof Apint  &&  zn.num instanceof Apint && zn.num.compareTo(Apint.ZERO) >=0 ){
            if ( !( zn.actual() instanceof Apint) ) {
                Apint i = ApintMath.pow((Apint) num, zn.num.longValue()) ;
                return narrowNumber(i);
            }
        }
        // check if zn is actually representable in a long
        Apfloat f ;
        long l = zn.num.longValue() ;
        if ( zn.num.doubleValue() == l ){
            Apfloat n = num ;
            if ( l < 0 ) {
                l = -l;
                n = Apfloat.ONE.divide(n);
            }
            f = ApfloatMath.pow(n,l);
        }else{
            // yet to find a precision model, having headache
            long p = num.precision() * zn.num.precision() ;
            try {
                f = ApfloatMath.pow(num.precision(p), zn.num.precision(p));
            }catch (Exception e){
                f = ApfloatMath.pow(num, zn.num );
            }
        }
        return narrowNumber(f);
    }

    @Override
    public void add_mutable(Object o) {
        Object r = _add_(o);
        init(r);
    }

    @Override
    public void sub_mutable(Object o) {
        Object r = _sub_(o);
        init(r);
    }

    @Override
    public void mul_mutable(Object o) {
        Object r = _mul_(o);
        init(r);
    }

    @Override
    public void div_mutable(Object o) {
        Object r = _div_(o);
        init(r);
    }

    @Override
    public Object _and_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.and(bi) ) ;
    }

    @Override
    public Object _or_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.or( bi )) ;
    }

    @Override
    public Object _xor_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.xor( bi )) ;
    }

    @Override
    public void and_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = new Apint( me.and(bi) );
    }

    @Override
    public void or_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = new Apint( me.or(bi) );
    }

    @Override
    public void xor_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = new Apint( me.xor(bi) );
    }

    @Override
    public Number _abs_() {
        if ( fractional() ) {
            return narrowNumber(ApfloatMath.abs(num));
        }
        return narrowNumber(ApintMath.abs((Apint) num));
    }

    @Override
    public Number _not_() {
        return narrowNumber(num.negate());
    }

    @Override
    public void not_mutable() {
        num = num.negate();
    }

    @Override
    public Number _mod_(Number o ) {
        if ( fractional() ){
            return Apfloat.ZERO  ;
        }
        Apint i = ApintMath.modMultiply((Apint) num, Apint.ONE, new Apint(String.valueOf(o)));
        return integer(i);
    }

    @Override
    public void mod_mutable(Number o) {
        if ( fractional() ){ return; }
        num = ApintMath.modMultiply((Apint) num, Apint.ONE, new Apint(String.valueOf(o)));
    }

    @Override
    public Number actual() {
        return narrowNumber(num);
    }

    @Override
    public BigDecimal bigDecimal() {
        return new BigDecimal(num.toString() ) ;
    }

    @Override
    public Apfloat apFloat() {
        return num;
    }

    @Override
    public Apint apInt() {
        return num.truncate();
    }

    @Override
    public BigInteger bigInteger() {
        return num.truncate().toBigInteger();
    }

    @Override
    public boolean fractional() {
        return  !(num instanceof Apint) ;
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return -1;
        if ( o == this ) return 0 ;
        ZNumber zn = new ZNumber(o);
        return num.compareTo( zn.num );
    }

    @Override
    public Number ceil() {
        return narrowNumber( num.ceil() );
    }

    @Override
    public Number floor() {
        return narrowNumber( num.floor() );
    }

    @Override
    public String toString() {
        return num.toString(true);
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return toString();
        if ( args[0] instanceof Number ){
            int i = ((Number)args[0]).intValue() ;
            if ( num instanceof Apint ){
                BigInteger bi = ((Apint)num).toBigInteger();
                return bi.toString(i);
            }
            // the precision float thingie
            BigDecimal bd = new BigDecimal( num.toString(true) );
            String format = "%%.%df" ;
            format = String.format(format, i);
            String v = String.format(format,bd);
            return v;
        }
        if ( args[0] instanceof String ){
            String pattern = (String)args[0];
            DecimalFormat df = new DecimalFormat(pattern);
            return df.format( num );
        }

        return toString();
    }

    @Override
    public int hashCode() {
        Number n = narrowNumber(num);
        return n.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == this ) return true ;
        if ( obj == null ) return false ;
        ZNumber o = new ZNumber(obj);
        if ( o.fractional() && !fractional() ){
            if ( o.num.frac().equals(Apfloat.ZERO ) ) return apInt().equals(o.num);
            return false;
        }
        if( !o.fractional() && fractional() ){
            if ( num.frac().equals(Apfloat.ZERO ) ) return apInt().equals(o.num);
            return false;
        }
        return num.equals( o.num );
    }

    /**
     * Log base e
     * @return log(this,e)
     */
    public Number ln(){
        long p = num.toString(true).length();
        Apfloat l = ApfloatMath.log( apFloat().precision( p ) );
        ZNumber zn = new ZNumber(l);
        return zn.actual();
    }

    /**
     * Log of this base b
     * @param b the base
     * @return log(this,b)
     */
    public Number log(Object b){
        Apfloat base = new ZNumber(b).apFloat() ;
        long p = base.toString(true).length() ;
        Apfloat l = ApfloatMath.log( apFloat() , base.precision( p ) );
        ZNumber zn = new ZNumber(l);
        return zn.actual();
    }

    public static Number log(Object...args){
        if  ( args.length == 0 ) return Math.E ;
        ZNumber zn = new ZNumber( args[0]);
        if ( args.length == 1 ) return zn.ln();
        return zn.log(args[1]);
    }
}
