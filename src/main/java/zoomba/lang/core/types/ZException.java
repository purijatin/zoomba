package zoomba.lang.core.types;

import zoomba.lang.parser.ZoombaNode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 */
public class ZException extends RuntimeException {

    public static final Break BREAK = new Break();

    public static final Continue CONTINUE = new Continue();

    public static final Return RETURN = new Return();
    
    protected ZException(){
        super("Unknown, No idea!");
    }

    protected ZException(Throwable e){ super(e);}

    protected ZException(String message){ super(message);}

    protected ZException(Throwable e, String message){
        super(message,e);
    }

    public static final class Tokenization extends ZException{
        public Tokenization(Throwable e, String message) {
            super(e,message);
        }
    }

    public static final class Parsing extends ZException{

        public final String errorMessage;

        public final Set<String> correctionOptions;

        public Parsing(Throwable e, String message) {
            super(e,message);
            String s = e.getMessage();
            int firstNewLine = s.indexOf("\n");
            errorMessage = s.substring(0,firstNewLine);
            String opts = s.substring( firstNewLine+1);
            firstNewLine = opts.indexOf("\n");
            opts = opts.substring( firstNewLine+1);
            String[] arr = opts.split("\n");
            correctionOptions = new HashSet<>();
            for ( int i = 0 ; i < arr.length; i++ ){
                correctionOptions.add( arr[i].trim().replace("...", "" ) );
            }
        }

        @Override
        public String getMessage(){
            return errorMessage;
        }
    }

    public static final class Variable extends ZException{
        public Variable(ZoombaNode node, String name) {
            super( name + " : "  + node.locationInfo());
        }
    }

    public static final class Function extends ZException{
        public Function(ZoombaNode node, String method ) {
            super( method + " : " + node.locationInfo() );
        }
    }

    public static final class Property extends ZException{
        public Property(ZoombaNode node, Object property ) {
            super( String.valueOf(property ) + " : " + node.locationInfo() );
        }
    }

    public static final class Import extends ZException{
        public Import(ZoombaNode node, Object imported ) {
            super( String.valueOf(imported ) + " : " + node.locationInfo() );
        }
    }

    public abstract static class ZRuntimeAssertion extends ZException{

        public static final ZAssertionException NOT_AN_EXCEPTION = new ZAssertionException();

        public final Object cause;

        public final ZoombaNode here;

        public ZRuntimeAssertion(ZoombaNode n, Object[] o){
            if ( o.length == 0 ){
                cause = NOT_AN_EXCEPTION;
                here = n;
                return;
            }

            Object c = o[0];
            if ( c instanceof CharSequence ){
                cause = new ZAssertionException(c.toString());
            } else if ( c instanceof Throwable ){
                cause = new ZAssertionException((Throwable)c);
            }
            else {
                cause = o;
            }
            here = n ;
        }

        public abstract String myType();

        @Override
        public String toString() {
            return String.format( "%s --> [ %s ] caused by : %s", myType() , here.locationInfo(), cause);
        }
    }

    public static class Panic extends ZRuntimeAssertion{

        public Panic(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Panic";
        }
    }

    public static class Assertion extends ZRuntimeAssertion{

        public Assertion(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Assertion Failure";
        }
    }

    public static class MonadicException extends ZException implements zoomba.lang.core.operations.Function.MonadicContainer {

        public final Object value;

        public MonadicException(){
            super();
            value = zoomba.lang.core.operations.Function.NIL;
        }

        public MonadicException(Object o){
            super();
            value = o ;
        }

        @Override
        public boolean isNil(){ return value == zoomba.lang.core.operations.Function.NIL; }

        @Override
        public Object value() {
            return value;
        }
    }

    public static final class Return extends MonadicException{

        private Return(){ super();}

        public Return(Object o){ super(o);}

    }

    public static final class Continue extends MonadicException{

        private Continue(){ super();}

        public Continue(Object o){ super(o);}

    }

    public static final class Break extends MonadicException{

        private Break(){ super();}

        public Break(Object o){ super(o);}

    }

    public static final class ZTerminateException extends ZAssertionException {

        public ZTerminateException(String m){
            super(m);
        }
    }
}


class ZAssertionException extends ZException{

    public ZAssertionException(){
        super("NO_EXCEPTION");
    }

    public ZAssertionException(Throwable t){
        super(t);
    }

    public ZAssertionException(String m){
        super(m);
    }

}


