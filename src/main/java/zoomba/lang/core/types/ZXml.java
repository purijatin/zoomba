package zoomba.lang.core.types;

import org.w3c.dom.*;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.io.ZFileSystem;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class ZXml {

    public static Object xsl(Document xmlDoc, String xslFile, String outFile) {
        TransformerFactory factory = TransformerFactory.newInstance();
        File tFile = new File(xslFile);
        try {
            Source xslt;
            if (tFile.exists()) {
                xslt = new StreamSource(tFile);
            } else {
                xslt = new StreamSource(new ByteArrayInputStream(xslFile.getBytes("UTF-8")));
            }

            Transformer transformer = factory.newTransformer(xslt);
            Source text = new DOMSource(xmlDoc);
            Result outputTarget;
            ByteArrayOutputStream outStream = null;
            if (outFile != null) {
                outputTarget = new StreamResult(new File(outFile));
            } else {
                outStream = new ByteArrayOutputStream();
                outputTarget = new StreamResult(outStream);
            }
            transformer.transform(text, outputTarget);
            if (outStream != null) {
                return outStream.toString();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    public static ZXml string2xml(String... args) throws Exception {
        if (args.length == 0) return null;
        // this is funny ...
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        String encoding = "UTF-8";
        if (args.length > 1) {
            encoding = args[1];
        }
        Document doc = db.parse(new ByteArrayInputStream(args[0].getBytes(encoding)));
        doc.getDocumentElement().normalize();
        return new ZXml(doc);
    }

    public final Document document;

    public final ZXmlElement root;

    public ZXml(Document doc) {

        document = doc;
        root = new ZXmlElement(document.getDocumentElement());
    }

    public static class ZXmlElement {

        public static final XPath X_PATH = XPathFactory.newInstance().newXPath();

        public boolean exists(String expression) throws Exception {
            boolean exists = (boolean) X_PATH.compile("boolean(" + expression + ")").evaluate(this.node,
                    XPathConstants.BOOLEAN);
            return exists;
        }

        public final Node node;

        public ZXmlElement(Node n) {
            node = n;
        }

        public List<ZXmlElement> children() {
            NodeList nodeList = node.getChildNodes();
            ArrayList<ZXmlElement> elements = new ArrayList<>();
            int size = nodeList.getLength();
            for (int i = 0; i < size; i++) {
                Node node = nodeList.item(i);
                ZXmlElement e = new ZXmlElement(node);
                elements.add(e);
            }
            return elements;
        }

        public Object get(String name) throws Exception {
            if (name.contains("/")) {
                return element(name);
            }
            if (name.startsWith("@")) {
                // attributes
                NamedNodeMap map = node.getAttributes();
                name = name.substring(1);
                Node n = map.getNamedItem(name);
                if (n != null) {
                    return n.getTextContent();
                }
            }
            switch (name) {
                case "text":
                    return node.getTextContent();
                case "child":
                    return children();
                case "parent":
                    return new ZXmlElement(node.getParentNode());
                default:
                    return Function.NIL;
            }
        }

        public String xpath(String path, String defaultValue) throws Exception {
            boolean exists = exists(path);
            if (!exists) return defaultValue;
            String val = (String) X_PATH.compile(path).evaluate(this.node, XPathConstants.STRING);
            return val;
        }

        public String xpath(String path) throws Exception {
            return xpath(path, null);
        }

        public ZXmlElement element(String path) throws Exception {
            NodeList nodeList = (NodeList) X_PATH.compile(path).evaluate(
                    this.node, XPathConstants.NODESET);
            if (nodeList.getLength() > 0) {
                return new ZXmlElement(nodeList.item(0));
            }
            return null;
        }

        public List<ZXmlElement> elements(String path) throws Exception {
            NodeList nodeList = (NodeList) X_PATH.compile(path).evaluate(
                    this.node, XPathConstants.NODESET);
            ArrayList<ZXmlElement> elements = new ArrayList<>();
            int size = nodeList.getLength();
            for (int i = 0; i < size; i++) {
                Node node = nodeList.item(i);
                ZXmlElement e = new ZXmlElement(node);
                elements.add(e);
            }
            return elements;
        }

        @Override
        public String toString() {
            return node.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ZXmlElement that = (ZXmlElement) o;

            return node != null ? node.equals(that.node) : that.node == null;

        }

        @Override
        public int hashCode() {
            return node != null ? node.hashCode() : 0;
        }
    }

    public String xpath(String path, String defaultValue) throws Exception {
        return root.xpath(path, defaultValue);
    }

    public String xpath(String path) throws Exception {
        return xpath(path, null);
    }

    public ZXmlElement element(String path) throws Exception {
        return root.element(path);
    }

    public List<ZXmlElement> elements(String path) throws Exception {
        return root.elements(path);
    }

    public Object xsl(String xslFile, String outFile) throws Exception {
        return xsl(document, xslFile, outFile);
    }

    public static ZXml xml(Object... args) {
        if (args.length == 0) return null;
        boolean file = false;
        try {
            if (args[0] instanceof String) {
                if (args.length > 1) {
                    file = ZTypes.bool(args[1], false);
                }
                if (file) {
                    Object o = ZMethodInterceptor.Default.read(args[0]);
                    String text;
                    if (o instanceof ZWeb.ZWebCom) {
                        text = ((ZWeb.ZWebCom) o).body;
                    } else {
                        text = String.valueOf(o);
                    }
                    return string2xml(text);
                } else {
                    return string2xml((String) args[0]);
                }
            }
            return string2xml(String.valueOf(args[0]));
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
