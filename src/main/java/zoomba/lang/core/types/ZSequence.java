package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.operations.Function;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * A mathematical sequence generator, non terminating
 */
public interface ZSequence extends Iterator {

    @Override
    default boolean hasNext() {
        return true;
    }

    ZSequence NULL_SEQUENCE = new BaseSequence( Function.NOP , ZArray.EMPTY_Z_ARRAY );

    /**
     * Retain the last amount of items in the list
     * @param last the amount of items to be retained
     * @return -1 if store data indefinitely, +ve if it has a size
     */
    int retain(int last);

    ZSequence yield();

    final class BaseSequence implements ZSequence{

        public final List history;

        public final Function next;

        protected ZNumber index ;

        protected int last;

        public BaseSequence(Function f, List collector){
            history = new ZList(collector);
            next = f ;
            index = new ZNumber( history.size() - 1 );
            last = -1 ;
        }

        public BaseSequence(Function f){
            this( f, new ZList());
        }

        public BaseSequence(Function f, Object...args){
            this( f, new ZList( new ArrayList(Arrays.asList( args ))) );
        }

        @Override
        public Object next() {
            index.add_mutable(1); // ++ actually
            Function.MonadicContainer o = next.execute( index , history , this , history );
            if ( o.isNil() ) throw new RuntimeException("A sequence must not return NIL!");
            Object result = o.value();
            history.add( result );
            if ( last >= 0 ){
               while ( history.size() != last ){
                   history.remove(0);
               }
            }

            return result ;
        }

        @Override
        public int retain(int last) {
            int prev = this.last ;
            this.last = last ;
            return prev ;
        }

        @Override
        public ZSequence yield(){
            BaseSequence bs = new BaseSequence( next, history );
            bs.last = last ;
            return bs ;
        }

        public static ZSequence sequence( Function anon, Object...args){
            if ( anon == null ) return NULL_SEQUENCE ;
            if ( args.length == 0 ) return new BaseSequence( anon );
            if ( args[0] instanceof List ) return  new BaseSequence( anon, ((List)args[0]) );
            return new BaseSequence( anon, args ) ;
        }

        @Override
        public String toString() {
            Number start = last < 0 ? 0 : (Number) index._add_( - last + 1 );
            return String.format( "[(%s:%s) -> %s]", start, index, history );
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || o.getClass().isAssignableFrom( ZSequence.class ) ) return false;
            BaseSequence that = (BaseSequence) o;
            return toString().equals( that.toString() );
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }
    }
}
