package zoomba.lang.core.types;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Date;
import zoomba.lang.core.operations.Arithmetic.* ;

/**
 */
public class ZDate implements BasicArithmeticAware, ZTypes.StringX{

    public static ZDate time(Object...args){
        if ( args.length == 0 ) return new ZDate();
        if ( args[0] == null  ) return new ZDate();
        try {
            if (args[0] instanceof LocalDateTime) return new ZDate((LocalDateTime) args[0]);
            if (args[0] instanceof Date) {
                if (args.length > 1) return new ZDate((Date) args[0], args[1].toString());
                return new ZDate((Date) args[0]);
            }
            if (args[0] instanceof ZDate) {
                return new ZDate((ZDate) args[0]);
            }
            if (args[0] instanceof String) {
                if (args.length > 1) {
                    return new ZDate(args[0].toString(), args[1].toString());
                }
                return new ZDate(args[0].toString());
            }
            if (args[0] instanceof Number) {
                return new ZDate((Number) args[0]);
            }
        }catch (Throwable t){
            // log, later... now eat up ?
        }
        return null;
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return time.toString();
        if ( args[0] instanceof String ){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern((String)args[0]);
            try {
                return formatter.format(time);
            }catch (Throwable t){
                // perhaps does not have ms and all?
                return formatter.format( time.toLocalDate() );
            }
        }
        return time.toString();
    }

    @Override
    public String toString() {
        return time.toString();
    }

    static final ThreadLocal<SimpleDateFormat> defaultFormatter = new ThreadLocal<>();

    static {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        fmt.setLenient(false);
        defaultFormatter.set( fmt );
    }

    protected LocalDateTime time;

    public LocalDateTime getTime(){ return time ; }

    public Date date(){ return  Date.from(time.atZone(ZoneId.systemDefault()).toInstant()); }

    public Date date(String zoneId){ return  Date.from(time.atZone(ZoneOffset.of(zoneId)).toInstant()); }

    public ZDate(){
        time = LocalDateTime.now();
    }

    public ZDate(LocalDateTime time){
        this.time = time ;
    }

    public ZDate(ZDate zDate){
        this.time = zDate.time  ;
    }

    public ZDate(String dateTime){
        if ( dateTime.startsWith("Z") || dateTime.startsWith("+") || dateTime.startsWith("-")){
           time = LocalDateTime.now(ZoneOffset.of(dateTime));
        }else {
            try{
                Date date = defaultFormatter.get().parse(dateTime);
                time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            }catch (Exception e){
                throw new UnsupportedOperationException("Invalid date !");
            }
        }
    }

    public ZDate(String dateTime, String format){
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(format);
            fmt.setLenient(false);
            Date d = fmt.parse(dateTime);
            time = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        }catch (Exception e){
            throw new UnsupportedOperationException("Invalid date !");
        }
    }

    public ZDate(Date date){
        time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public ZDate(Date date, String  zoneId){
        time = date.toInstant().atZone(ZoneOffset.of(zoneId)).toLocalDateTime();
    }

    public ZDate(Number timeStamp){
        this( new Date(timeStamp.longValue() ));
    }

    @Override
    public Object _add_(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            LocalDateTime aTime = time.plus( ((Number) o).longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return new ZDate(aTime);
        }
        if ( !(o instanceof Duration) ){
            o = Duration.parse( String.valueOf(o));
        }
        if ( o instanceof Duration){
            long l = ((Duration) o).toMillis();
            LocalDateTime aTime = time.plus( l , ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return new ZDate(aTime);
        }
        throw new UnsupportedOperationException("Can not add non duration into Date!");
    }

    @Override
    public Object _sub_(Object o) {
        if ( o instanceof Date ){
            o = new ZDate((Date) o);
        }
        if ( o instanceof ZDate ){
            o = ((ZDate)o).time ;
        }
        if ( o instanceof LocalDateTime ){
            return Duration.between(time, (LocalDateTime)o);
        }
        // if o is Number, add to millis
        if ( o instanceof Number ){
            LocalDateTime aTime = time.minus( ((Number) o).longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return new ZDate(aTime);
        }
        if ( !(o instanceof Duration) ){
            o = Duration.parse( String.valueOf(o));
        }
        if ( o instanceof Duration){
            long l = ((Duration) o).toNanos();
            LocalDateTime aTime = time.minus( l , ChronoField.NANO_OF_DAY.getBaseUnit() );
            return new ZDate(aTime);
        }
        throw new UnsupportedOperationException("Can not subtract non duration into Date!");
    }

    @Override
    public Object _mul_(Object o) {
        throw new UnsupportedOperationException("Can not multiply date!");
    }

    @Override
    public Object _div_(Object o) {
        throw new UnsupportedOperationException("Can not divide date!");
    }

    @Override
    public Object _pow_(Object o) {
        throw new UnsupportedOperationException("Can not raise power of date!");
    }

    @Override
    public void add_mutable(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            time = time.plus( ((Number) o).longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return ;
        }
        if ( !(o instanceof Duration) ){
            o = Duration.parse( String.valueOf(o));
        }
        if ( o instanceof Duration){
            long l = ((Duration) o).toMillis();
            time = time.plus( l , ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return ;
        }
        throw new UnsupportedOperationException("Can not add non duration into Date!");
    }

    @Override
    public void sub_mutable(Object o) {
        // if o is Number, add to millis
        if ( o instanceof Number ){
            time = time.minus( ((Number) o).longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit() );
            return ;
        }
        if ( !(o instanceof Duration) ){
            o = Duration.parse( String.valueOf(o));
        }
        if ( o instanceof Duration){
            long l = ((Duration) o).toNanos();
            time = time.minus( l , ChronoField.NANO_OF_DAY.getBaseUnit() );
            return ;
        }
        throw new UnsupportedOperationException("Can not subtract non duration into Date!");
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply date!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Can not divide date!");
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1 ;
        if ( o == this ) return  0;
        if ( o instanceof ChronoLocalDateTime) return time.compareTo((ChronoLocalDateTime)o);
        if ( o instanceof ZDate ){
            ZDate other = (ZDate)o;
            return time.compareTo(other.time);
        }
        if ( o instanceof Date ){
            ZDate other = new ZDate((Date) o);
            return time.compareTo(other.time);
        }
        if ( o instanceof Number ){
            ZDate other = new ZDate((Number) o);
            return time.compareTo(other.time);
        }
        throw new UnsupportedOperationException("Can not compare against something which is not date!");
    }

    @Override
    public int hashCode() {
        return time.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null ) return false ;
        if ( obj == this ) return true ;
        return ( 0 == compareTo(obj) );
    }
}
