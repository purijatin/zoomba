package zoomba.lang.core.oop;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.interpreter.AnonymousFunctionInstance;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.interpreter.ZScriptMethod;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTArgDef;
import zoomba.lang.parser.ZoombaNode;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 */
public class ZObject extends ZMap implements
        ZMethodInterceptor, Arithmetic.BasicArithmeticAware , ZTypes.StringX , Arithmetic.LogicAware ,Iterable {

    /*
    * What does this $ convention means?
    *  1. $ stand for the item, $his.
    *  2. Hence, a function $xxx means $his function xxx
    *  3. A function $xxx$ means mutable $his xxx
    *  4. Clearly then $$ means the Init, because it is $his, and mutates $his.
    * */

    public static final String EQUAL = "$eq" ;

    public static final String HASH_CODE = "$hc" ;

    public static final String CMP = "$cmp" ;

    public static final String STR = "$str" ;

    public static final String INIT = "$$" ;

    public static final String ADD = "$add" ;

    public static final String SUB = "$sub" ;

    public static final String MUL = "$mul" ;

    public static final String DIV = "$div" ;

    public static final String POW = "$pow" ;

    public static final String ADD_M = "$add$" ;

    public static final String SUB_M = "$sub$" ;

    public static final String MUL_M = "$mul$" ;

    public static final String DIV_M = "$div$" ;

    public static final String AND = "$and" ;

    public static final String OR = "$or" ;

    public static final String XOR = "$xor" ;

    public static final String AND_M = "$and$" ;

    public static final String OR_M = "$or$" ;

    public static final String XOR_M = "$xor$" ;

    public static final String NEXT = "$next" ;

    public final String name ;

    ZInterpret cachedInterpreter;

    public ZObject(String name, Map m, ZInterpret interpret) {
        super(m);
        this.name = name ;
        this.cachedInterpreter = interpret ;
    }

    public ZObject(ZObject proto, ZInterpret interpret) {
        super(proto);
        this.name = proto.name ;
        this.cachedInterpreter = interpret ;
    }

    @Override
    public Object get(Object key) {
        if ( "name".equals(key) ) return name ;
        return super.get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        if ("name".equals(key) ) return value ;
        return super.put(key, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Function.MonadicContainer mc = execute(EQUAL,new Object[]{o});
        if ( !mc.isNil() ) { return ZTypes.bool(mc.value(),false) ; }
        ZObject zObject = (ZObject) o;
        if ( !name.equals( zObject.name ) ) return false ;
        return ZMap.difference(this,zObject).isEmpty() ;
    }

    @Override
    public int hashCode() {
        Function.MonadicContainer mc = execute(HASH_CODE,new Object[]{});
        if ( !mc.isNil() ) {
           Number hc = ZNumber.integer( mc.value() );
           if ( hc != null ) return hc.intValue();
           // log error about how hashCode is all wronged up ...
        }
        int result =  name.hashCode() ;
        for ( Object o : entrySet() ){
            result = 31 * result + o.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        Function.MonadicContainer mc = execute(STR, ZArray.EMPTY_ARRAY );
        if ( mc.isNil() ){
            // make a JSON proper...
            return String.format( "{ \"%s\" : %s }" , name, ZTypes.jsonString((Map)this)) ;
        }
        return String.valueOf( mc.value() ) ;
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return toString();
        Function.MonadicContainer mc = execute(STR,args);
        if ( mc.isNil() ) return super.toString();
        return String.valueOf( mc.value() ) ;
    }

    Function.MonadicContainer execute(String methodName, Object[] args){
        Object o = get(methodName);
        if ( !(o instanceof ZScriptMethod) ) return UNSUCCESSFUL_INTERCEPT ;
        ZScriptMethod m = (ZScriptMethod)o;
        ZScriptMethod.ZScriptMethodInstance instance = m.instance(cachedInterpreter,this);
        return instance.execute(args);
    }

    public static ZObject createFrom(ZObject proto, ZInterpret interpret, Object[] args){
        ZObject tmp = new ZObject(proto,interpret);
        Object o = tmp.get( INIT );
        if ( o instanceof ZScriptMethod ){
            ZScriptMethod m = (ZScriptMethod)o;
            ZScriptMethod.ZScriptMethodInstance instance = m.instance(interpret,tmp);
            instance.execute(args);
        }else {
            for (int i = 0; i < args.length; i++) {
                // setup stuff
                if ( args[i] instanceof ASTArgDef ){
                    String p = ((ZoombaNode)((ASTArgDef)args[i]).jjtGetChild(0)).image;
                    Object v = ((ASTArgDef)args[i]).jjtGetChild(1).jjtAccept(interpret,null);
                    tmp.put(p,v);
                }
            }
        }
        return tmp;
    }

    @Override
    public Function.MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                               Object[] args, List<Function> anons,
                                               ZoombaNode callNode) {
        cachedInterpreter = interpret ;
        return execute(methodName,args);
    }

    Object executeOp(Object o, String actualMethod, String opName){
        Function.MonadicContainer mc = execute(actualMethod,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            throw new UnsupportedOperationException("Object does not implement " + opName );
        }
        return mc.value();
    }

    void setSelf(ZObject other){
        for ( Object key : other.keySet() ){
            Object value = other.get(key);
            if ( !(value instanceof ZScriptMethod) ){
                this.put(key,value);
            }
        }
    }

    void executeMutable(Object o, String actualMethod,String fallBackMethod, String opName){
        Function.MonadicContainer mc = execute(actualMethod,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            mc = execute(fallBackMethod,new Object[]{o});
            if ( UNSUCCESSFUL_INTERCEPT == mc ){
                throw new UnsupportedOperationException("Object does not implement" + opName );
            }
            Object m = mc.value();
            if ( !(m instanceof ZObject) )
                throw new UnsupportedOperationException("Object does not implement" + opName ) ;
            setSelf((ZObject)m);
        }
    }


    @Override
    public Object _add_(Object o) {
        return executeOp(o,ADD,"Addition");
    }

    @Override
    public Object _sub_(Object o) {
        return executeOp(o,SUB,"Subtraction");
    }

    @Override
    public Object _mul_(Object o) {
        return executeOp(o,MUL,"Multiplication");
    }

    @Override
    public Object _div_(Object o) {
        return executeOp(o,DIV,"Division");
    }

    @Override
    public Object _pow_(Object o) {
        return executeOp(o,POW,"Power");
    }

    @Override
    public void add_mutable(Object o) {
        executeMutable(o,ADD_M,ADD, "Addition" );
    }

    @Override
    public void sub_mutable(Object o) {
        executeMutable(o,SUB_M,SUB, "Subtraction" );
    }

    @Override
    public void mul_mutable(Object o) {
        executeMutable(o,MUL_M,MUL, "Multiplication" );
    }

    @Override
    public void div_mutable(Object o) {
        executeMutable(o,DIV_M,DIV, "Division" );
    }

    @Override
    public int compareTo(Object o) {
        Function.MonadicContainer mc = execute(CMP,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            throw new UnsupportedOperationException("Object does not implement compare!");
        }
        if ( mc.value() instanceof Boolean ){
            boolean smaller = (boolean)mc.value();
            return smaller ? -1 : 1 ;
        }
        return ZNumber.integer( mc.value() ,-1 ).intValue() ;
    }

    @Override
    public Object _and_(Object o) {
        return executeOp(o,AND,"And");
    }

    @Override
    public Object _or_(Object o) {
        return executeOp(o,OR,"Or");
    }

    @Override
    public Object _xor_(Object o) {
        return executeOp(o,XOR,"XOR");
    }

    @Override
    public void and_mutable(Object o) {
        executeMutable(o,AND_M,AND, "And" );
    }

    @Override
    public void or_mutable(Object o) {
        executeMutable(o,OR_M,OR, "Or" );
    }

    @Override
    public void xor_mutable(Object o) {
        executeMutable(o,XOR_M,XOR, "XOR" );
    }

    @Override
    public Iterator iterator() {
        Object o = get(NEXT );
        if ( !(o instanceof ZScriptMethod) ) return Collections.EMPTY_LIST.iterator();
        return ((ZScriptMethod)o).instance(this.cachedInterpreter,this);
    }
}
