package zoomba.lang.core.sys;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

/**
 */
public class ZThread extends Thread implements Runnable, Function.MonadicContainer {

    public static final Function.MonadicContainer THREAD_RUNNING = new Function.MonadicContainerBase();

    protected Function f;

    protected Function.MonadicContainer state;

    Object threadArg;

    protected Function after;

    public ZThread(Function threadFunction, Object... arg){
        this.f = threadFunction ;
        // wrap it...
        threadArg = arg ;
        state  =  THREAD_RUNNING ;
        after = Function.NOP ;
    }

    public ZThread(Function threadFunction, Function after){
        this.f = threadFunction ;
        // wrap it...
        threadArg = ZArray.EMPTY_ARRAY  ;
        state  =  THREAD_RUNNING ;
        this.after = after ;
    }

    @Override
    public boolean isNil() {
        return state.isNil() ;
    }

    @Override
    public Object value() {
        return state.value();
    }

    @Override
    public void run() {
        // process args...
        Object[] args = new Object[4];
        // get the id
        args[0] = getId();
        // current
        args[1] = this ;
        // the args are the context
        args[2] = threadArg ;
        // there is no partial
        args[3] = Function.NIL;
        state = f.execute(args);
        after.execute( state.value() );
    }

    public static Runnable async( Function threadFunction, Function after){
        Thread t = new ZThread(threadFunction, after );
        t.start();
        return t;
    }

    /**
     * Poll until predicate is true
     * @param predicate the condition
     * @param args integers on no of loops and poll interval
     * @return true when polling successful false otherwise
     */
    public static boolean poll( Function predicate, Object[] args){
        if ( predicate == null ){
            predicate = Function.TRUE ;
        }
        int maxLoop = 42 ; // no of times
        int pollInterval = 420 ; // ms
        if ( args.length > 0 ){
            maxLoop = ZNumber.integer( args[0] , maxLoop ).intValue() ;
            if ( args.length > 1 ){
                pollInterval = ZNumber.integer( args[1] , pollInterval ).intValue() ;
            }
        }
        while( true ) {
            maxLoop--;
            Function.MonadicContainer container = predicate.execute( );
            if (ZTypes.bool( container.value() , false) ){
                return true ;
            }
            if ( maxLoop == 0 ) return false ;
            try { Thread.sleep( pollInterval ); } catch (Throwable t){}
        }
    }
}
