package zoomba.lang.core.sys;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;


/**
 */
public class ZProcess{

    public static final class ZProcInfo{

        public final String[] com;

        public String err;

        public String out;

        public int code;

        public ZProcInfo(String...args){
            com = args ;
            code = Integer.MAX_VALUE ;
            err = "" ;
            out = "" ;
        }

        public Process process(){
            try {
                ProcessBuilder pb = new ProcessBuilder();
                pb.command(new ZArray(com,false) );
                return pb.start();
            }catch (Exception e){
                throw new UnsupportedOperationException(e);
            }
        }

        public ZProcess start(){
            return new ZProcess(this);
        }
    }

    public static int system(Object...args){
        if ( args.length == 1 ){
            try {
                Process p = Runtime.getRuntime().exec(String.valueOf(args[0]));
                while ( p.isAlive() ){
                    Thread.sleep(1000);
                }
                return p.exitValue();
            }catch (Exception e){
            }
            return Integer.MIN_VALUE ;
        }

        String[] commands = new String[args.length];
        for ( int i = 0 ; i < commands.length ; i++ ){
            commands[i] = String.valueOf(args[i]);
        }

        ProcessBuilder pb = new ProcessBuilder();
        pb.command(new ZArray(commands,false) );
        try{
            Process p = pb.start();
            while ( p.isAlive() ){
                Thread.sleep(1000);
            }
            return p.exitValue();
        }catch (Exception e){
            // define the error in executing
            return Integer.MIN_VALUE ;
        }
    }

    public static ZProcess popen(Object...args){
        String[] commands = new String[args.length];
        for ( int i = 0 ; i < commands.length ; i++ ){
            commands[i] = String.valueOf(args[i]);
        }
        ZProcInfo info = new ZProcInfo(commands);
        return new ZProcess(info);
    }

    public final Process process;

    public final ZProcInfo procInfo;

    public ZProcess(ZProcInfo info){
        procInfo = info ;
        process = procInfo.process();
    }

    private void populateInfo(){
        try {
            // populate the status :: this is why Java sucks
            BufferedReader r = new BufferedReader( new InputStreamReader( process.getInputStream() ) );
            StringBuilder buf = new StringBuilder();
            String line ;
            while ( (line = r.readLine())!= null ){
                buf.append(line).append("\n");
            }
            procInfo.out = buf.toString();
            buf.setLength(0);
            r = new BufferedReader( new InputStreamReader( process.getErrorStream() ) );
            while ( (line = r.readLine())!= null ){
                buf.append(line).append("\n");
            }
            procInfo.err = buf.toString();
        }catch (Exception e){}
    }

    public void waitOn(){
        while ( process.isAlive() ){
            try {
                Thread.sleep(1000);
            }catch (Exception e){}
        }
    }

    public boolean waitOn(long timeOut){
        if ( timeOut <= 0 ) {
            waitOn();
            return true ;
        }
        final long sleepTime = timeOut/ 10 ;
        long curTime = System.currentTimeMillis();
        final long endTime = curTime + timeOut ;
        while (  curTime < endTime ){
            try {
                Thread.sleep(sleepTime);
                curTime = System.currentTimeMillis();
                if ( !process.isAlive() ) return true ;
            }catch (Exception e){}
        }
        return false ;
    }

    public boolean kill(){
        process.destroy();
        return !process.isAlive();
    }

    public ZProcInfo status(){
        if ( !process.isAlive() ) { populateInfo(); }
        return procInfo;
    }
}
