package zoomba.lang.core.operations;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import java.util.Arrays;
import java.util.EventObject;

/**
 */
public interface ZEventAware {

    class ZEventArg extends EventObject{

        public final Object method;

        public final ZArray args;

        public final Throwable error;

        public ZEventArg(Object source, Object method, Object[] args) {
            this( source, method, args, ZException.RETURN );
        }

        public ZEventArg(Object source, Object method, Object[] args, Throwable error) {
            super(source);
            this.method = method ;
            this.args = new ZArray(args,false) ;
            this.error = error ;
        }

        @Override
        public String toString() {
            return String.format(
                    "{ \"m\" : \"%s\" , \"a\" : %s , \"e\" : \"%s\"" ,
                    method , args.string(true) , error);
        }
    }

    void before(ZEventArg before);

    void after(ZEventArg after);

    interface TransactionAware extends ZEventAware{

        boolean success();

        boolean rollBack();
    }
}
