package zoomba.lang.core.operations;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 */
public interface ZCollection extends Collection , Comparable {

    enum Relation{
        SUB,
        SUPER,
        OVERLAP,
        EQUAL,
        INDEPENDENT
    }

    Map<Object,List> toMultiSet(Function hash);

    Set toSet(Function hash);

    Map<Object,Integer> toMultiSet();

    Set toSet();

    Relation relate(Collection c);

    ZCollection union(Collection c);

    ZCollection intersection(Collection c);

    ZCollection difference(Collection c);

    ZCollection product(Collection c);

    ZCollection join(Collection... cc);

    ZCollection join(Function predicate, Collection... cc);

    ZCollection join(Function predicate, Function map, Collection... cc);

    ZCollection map(Function map);

    ZCollection flatMap(Function map);

    ZCollection flatten();

    void forEach( Function apply );

    Object leftFold( Function fold , Object... arg);

    Object rightFold( Function fold , Object... arg);

    Object leftReduce( Function reduce);

    Object rightReduce( Function reduce);
    
    ZCollection[] partition( Function predicate );

    ZCollection select( Function predicate );

    int leftIndex(Function predicate);

    int rightIndex(Function predicate);

    Function.MonadicContainer find(Function predicate);

    ZCollection reverse();

    String string(String separator);

    ZCollection compose(Function... functions);
}
