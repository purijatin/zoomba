package zoomba.lang.core.operations;

import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import java.util.Comparator;

/**
 */
public interface Function {

    String THIS = "$" ;

    final class Nil{
        private Nil(){}

        @Override
        public String toString(){
            return "nil" ;
        }
    }

    Nil NIL = new Nil();

    interface  Mapper extends Function{

        Object map(Object...args);
    }

    interface  Predicate extends Function{

        boolean accept(Object...args);
    }

    interface MonadicContainer{

        boolean isNil();

        Object value();

    }

    class MonadicContainerBase implements MonadicContainer {

        public final Object value;

        @Override
        public final boolean isNil(){ return value instanceof Nil ; }

        public MonadicContainerBase(){
            this( NIL );
        }

        public MonadicContainerBase(Object v){
            value = v ;
        }

        @Override
        public Object value() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public String toString() {
            return String.valueOf(this.value);
        }
    }

    MonadicContainer SUCCESS = new MonadicContainerBase(true);

    MonadicContainer FAILURE = new MonadicContainerBase(false);

    MonadicContainer Void = new MonadicContainerBase();

    MonadicContainer NOTHING = Void ;

    String body();

    MonadicContainer execute(Object...args);

    String name();

    Nop NOP = new Nop();

    Identity IDENTITY = new Identity();

    CollectorIdentity COLLECTOR_IDENTITY = new CollectorIdentity();

    TruePredicate TRUE = new TruePredicate();

    FalsePredicate FALSE = new FalsePredicate();

    final class ComparatorLambda implements Comparator {

        public final Object col;

        public final Function instance ;

        public  ComparatorLambda(Object col, Function f){
            this.col = col ;
            this.instance = f ;
        }

        public Number num(Object o, Number index, Object partial){
            Object[] args = new Object[ ]{ index, o , col , partial};
            MonadicContainer mc = instance.execute(args);
            if ( mc instanceof ZException.MonadicException ) throw ((RuntimeException)mc);
            if ( mc.isNil() ) throw new UnsupportedOperationException("Scalar can not return void!");
            Object r = mc.value();
            if ( r instanceof Number ) return ((Number) r);
            return ZNumber.number(mc.value(),0);
        }

        @Override
        public int compare(Object o1, Object o2) {
            Object[] args = new Object[ ]{ -1, new Object[]{ o1, o2 } , col , NIL};
            MonadicContainer mc = instance.execute(args);
            if ( mc.isNil() ) throw new UnsupportedOperationException("Comparator can not return void!");
            Object r = mc.value();
            if ( r instanceof Number ) return ((Number) r).intValue() ;
            boolean lt = ZTypes.bool( r, false );
            if ( lt ) return -1;
            return 1 ;
        }
    }
}

final class Nop implements Function{

    final String body = String.format("def %s(){ return __args__ } ", name() );

    @Override
    public String name() {
        return "_0_" ;
    }


    @Override
    public String body() {
        return body ;
    }

    @Override
    public MonadicContainer execute(Object...args) {
        return Void ;
    }

}

final class Identity implements Function{

    final String body = String.format("def %s(){ return __args__ } ", name() );

    @Override
    public String body() {
        return body ;
    }

    @Override
    public MonadicContainer execute(Object...args) {
        return new MonadicContainerBase(args) ;
    }

    @Override
    public String name() {
        return "_1_" ;
    }

}

final class CollectorIdentity implements Function{

    @Override
    public String body() {
        return "return $.o ;";
    }

    @Override
    public MonadicContainer execute(Object... args) {
        return new MonadicContainerBase( args[1] );
    }

    @Override
    public String name() {
        return "CollectorIdentity" ;
    }
}


final class TruePredicate implements Function.Predicate {

    final static Function.MonadicContainer TRUE = new Function.MonadicContainerBase(true);

    @Override
    public String body() {
        return "def _true_(){ true }";
    }

    @Override
    public Function.MonadicContainer execute(Object...args) {
        return TRUE ;
    }

    @Override
    public String name() {
        return "true";
    }

    @Override
    public boolean accept(Object... args) {
        return true;
    }
}

final class FalsePredicate implements Function.Predicate {

    final static Function.MonadicContainer FALSE = new Function.MonadicContainerBase(false);

    @Override
    public String body() {
        return "def _true_(){ true }";
    }

    @Override
    public Function.MonadicContainer execute(Object...args) {
        return FALSE ;
    }

    @Override
    public String name() {
        return "false";
    }

    @Override
    public boolean accept(Object... args) {
        return false;
    }

}

