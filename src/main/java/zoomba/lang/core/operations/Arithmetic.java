package zoomba.lang.core.operations;

import org.apfloat.Apfloat;
import org.apfloat.Apint;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.types.*;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZString;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 */
public class Arithmetic {

    public static boolean isInfinity(Object o){
        return o instanceof Infinity ;
    }

    private static class Infinity extends Number {

        Infinity(){}

        @Override
        public int intValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public long longValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public float floatValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public double doubleValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }
    }

    static final class PositiveInfinity extends Infinity implements Comparable{

        private PositiveInfinity(){}

        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return 1;
        }

        @Override
        public String toString() {
            return "inf";
        }
    }

    static final class NegativeInfinity extends Infinity implements Comparable{

        private NegativeInfinity(){}

        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return -1;
        }

        @Override
        public String toString() {
            return "-inf";
        }
    }

    public static final PositiveInfinity POSITIVE_INFINITY = new PositiveInfinity();

    public static final NegativeInfinity NEGATIVE_INFINITY  = new NegativeInfinity();

    public interface BasicArithmeticAware extends Comparable {

        Object _add_(Object o);

        Object _sub_(Object o);

        Object _mul_(Object o);

        Object _div_(Object o);

        Object _pow_(Object o);

        void add_mutable(Object o);

        void sub_mutable(Object o);

        void mul_mutable(Object o);

        void div_mutable(Object o);
    }

    public interface NumericAware extends Comparable {

        Number _abs_();

        Number _not_();

        void not_mutable();

        Number _mod_(Number o);

        void mod_mutable(Number o);

        Number actual();

        Apfloat apFloat();

        Apint apInt();

        BigDecimal bigDecimal();

        BigInteger bigInteger();

        boolean fractional();

        Number ceil();

        Number floor();
    }

    public interface LogicAware {

        Object _and_(Object o);

        Object _or_(Object o);

        Object _xor_(Object o);

        void and_mutable(Object o);

        void or_mutable(Object o);

        void xor_mutable(Object o);
    }

    public void NULL_CHECK(Object o){
        if ( o == null ) throw new UnsupportedOperationException("Operation on null not supported!");
    }

    public int UNSUPPORTED_OPERATION(Object l, Object r, String op){
        String message = String.format( "Can not do operation ( %s ) : ( %s ) with ( %s ) !",
                op,l,r);
        throw new UnsupportedOperationException(message);
    }

    public Object add(Object left, Object right) {
        // start with arithmetic ...
        NULL_CHECK(left);
        if ( left instanceof CharSequence ){
            return left.toString() + String.valueOf(right);
        }
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._add_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._add_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    public Object addMutable(Object left, Object right) {
        // start with arithmetic ...
        NULL_CHECK(left);

        if ( left instanceof CharSequence ){
            if ( left instanceof ZString){
                ((ZString) left).add_mutable( right );
                return left ;
            }
            ZString zs = new ZString(left);
            zs.add_mutable( right );
            return zs;
        }
        try{
            if ( left instanceof Number ){
                if ( left instanceof ZNumber ){
                    ((ZNumber) left).add_mutable(right);
                    return left;
                }
                ZNumber l = new ZNumber(left);
                return l._add_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).add_mutable(right);
                return left ;
            }
        }catch (Exception e){ }
        return UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    public Object sub(Object left, Object right) {
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._sub_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._sub_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    public Object subMutable(Object left, Object right) {
        NULL_CHECK(left);
        try{
            if ( left instanceof Number ){
                if ( left instanceof ZNumber ){
                    ((ZNumber) left).sub_mutable(right);
                    return left;
                }
                ZNumber l = new ZNumber(left);
                return l._sub_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).sub_mutable(right);
                return left ;
            }
        }catch (Exception e){
        }
        return UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    public Object mul(Object left, Object right) {
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._mul_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._mul_(right);
            }

        }
        return UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    public Object mulMutable(Object left, Object right) {
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            l.mul_mutable(r);
            if ( !(left instanceof ZNumber) ) {
                return l.actual();
            }
            return l;
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).mul_mutable(right);
                return left;
            }

        }
        return UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    public Object div(Object left, Object right) {
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._div_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                return ((BasicArithmeticAware) left)._div_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    public Object divMutable(Object left, Object right) {
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            l.div_mutable(r);
            if ( !(left instanceof ZNumber) ) {
                return l.actual();
            }
            return l ;
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                ((BasicArithmeticAware) left).div_mutable(right);
                return left ;
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    public Object pow(Object left, Object right) {
        if ( left instanceof CharSequence ){
            ZString zs = new ZString((CharSequence)left);
            return zs._pow_(right);
        }
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._pow_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._pow_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"EXP");
    }

    public static Object collectionConvert(Object o){
        // do not convert ZObjects or Tuples and what not ...
        if ( o instanceof ZObject ||
                o instanceof ZMatrix.Tuple ) return o;

        // now rest
        if ( o.getClass().isArray() ){
            return new ZArray(o,false);
        }
        if ( o instanceof ZCollection ){
            return o;
        }
        if ( o instanceof List ){
            return new ZList((List)o);
        }
        if ( o instanceof Set ){
            return new ZList((Set)o);
        }
        if ( o instanceof Map ){
            return new ZMap((Map)o);
        }

        return o;
    }

    public int compare(Object left, Object right){
        // intercept ... infinities
        if ( left instanceof Infinity ){ return ((Comparable)left).compareTo(right) ; }
        if ( right == POSITIVE_INFINITY ) return -1;
        if ( right == NEGATIVE_INFINITY ) return 1;

        if ( left == null ){
            if ( right == null ) return 0;
            return -1;
        }
        if ( right == null ) return 1 ;

        if ( left instanceof Number ){
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l.compareTo(r);
        }
        if ( left instanceof Character ){
            if ( right instanceof Number ){
                int i = ((Number) right).intValue();
                return ((Character) left).charValue() - i;
            }
            return left.toString().compareTo( String.valueOf(right));
        }

        if ( left instanceof CharSequence ){
            ZString s = new ZString(left);
            return s.compareTo(right);
        }
        left = collectionConvert(left);
        if ( left instanceof Comparable ){
            return ((Comparable) left).compareTo(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"COMPARING!");
    }

    public boolean lt(Object left, Object right) {
        try{
            return compare(left,right) < 0 ;
        }catch (Throwable t){}
        return false;
    }

    public boolean le(Object left, Object right) {
        try{
            return compare(left,right) <= 0 ;
        }catch (Throwable t){}
        return false;
    }

    public boolean gt(Object left, Object right) {
        try{
            return compare(left,right) > 0 ;
        }catch (Throwable t){}
        return false;
    }

    public boolean ge(Object left, Object right) {
        try{
            return compare(left,right) >= 0 ;
        }catch (Throwable t){}
        return false;
    }

    public boolean eq(Object left, Object right) {
        try{
            if ( left instanceof Boolean ){
                return (boolean)left == ZTypes.bool(right) ;
            }
            return ( compare(left,right) == 0 ) ;
        }catch (Throwable t){
        }
        return left.equals(right);
    }

    public boolean ne(Object left, Object right) {
        return !eq(left, right);
    }

    public Object and(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left && ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._and_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
           return  ((LogicAware) left)._and_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"AND");
    }

    public Object andMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left && ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).and_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber(left);
            return n._and_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).and_mutable(right);
            return left ;
        }
        return UNSUPPORTED_OPERATION(left,right,"AND");
    }

    public Object or(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left || ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._or_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
            return  ((LogicAware) left)._or_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"OR");
    }

    public Object orMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left || ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).or_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber(left);
            return n._or_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).or_mutable(right);
            return left;
        }
        return UNSUPPORTED_OPERATION(left,right,"OR");
    }

    public Object xor(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._xor_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
            return  ((LogicAware) left)._xor_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"XOR");
    }

    public Object xorMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._xor_(right);
        }
        left = collectionConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).xor_mutable(right);
            return left;
        }
        return UNSUPPORTED_OPERATION(left,right,"XOR");
    }

    public Number not(Object o) {
        if ( o instanceof ZNumber ) { return ((ZNumber) o)._not_() ; }
        if ( o instanceof Number ){
            ZNumber n = new ZNumber(o);
            return n._not_();
        }
        return UNSUPPORTED_OPERATION(o,o,"NEGATE");
    }

    public Number mod(Object left, Object right) {
        NULL_CHECK(left);
        NULL_CHECK(right);
        ZNumber l = new ZNumber(left);
        ZNumber r = new ZNumber(right);
        return l._mod_(r);
    }

    public Number modMutable(Object left, Object right) {
        if ( left instanceof ZNumber && right instanceof Number ){
            ((ZNumber) left).mod_mutable( (Number) right );
            return (Number) left;
        }
        NULL_CHECK(left);
        NULL_CHECK(right);
        ZNumber l = new ZNumber(left);
        ZNumber r = new ZNumber(right);
        return l._mod_(r);
    }

    public boolean divides(Object left, Object right){
        if ( !( left instanceof Number && right instanceof Number ) ) return false ;
        ZNumber l = new ZNumber( left );
        ZNumber r = new ZNumber( right );
        if ( l.fractional() || r.fractional() ) return true ; // mathematically, we are correct
        return ( r._mod_( l ).intValue() == 0 ) ;
    }

    /**
     * Left is in as a element in the right
     * @param left element
     * @param right container
     * @return true if left in right, false if not
     */
    public boolean in(Object left, Object right){
        if ( right == null ) return false ;
        if ( right.getClass().isArray() ){
            ZArray za = new ZArray(right,false);
            return za.contains( left );
        }
        if ( right instanceof ZRange ){
            right = ((ZRange) right).asList();
        }
        if ( right instanceof Map){
            return ((Map) right).containsKey(left);
        }
        if ( right instanceof Collection){
            return ((Collection) right).contains(left);
        }
        if ( left == null ) return false ;

        if ( right instanceof CharSequence ){
            return right.toString().contains( String.valueOf(left));
        }
        return false ;
    }

    public boolean startsWith(Object left, Object right){
        if ( left == null ) return false ;
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().startsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left,false);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.indexOf(right) == 0 );
        }
        return false ;
    }

    public boolean endsWith(Object left, Object right){
        if ( left == null ) return false ;
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().endsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left,false);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.lastIndexOf(right) == zl.size() - 1 );
        }
        return false ;
    }
}
