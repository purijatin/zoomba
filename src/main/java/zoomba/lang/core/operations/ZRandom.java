package zoomba.lang.core.operations;
import org.apfloat.Apint;
import dk.brics.automaton.Automaton;
import dk.brics.automaton.State;
import dk.brics.automaton.Transition;
import dk.brics.automaton.RegExp;

import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.types.ZRange;

import static zoomba.lang.core.operations.Function.NIL;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class ZRandom extends SecureRandom {

    public static final String HASH_MD5 = "MD5";

    public static final ZRandom RANDOM = new ZRandom();

    class RandomIterator implements Iterator{

        final ZRandom r;

        final Object arg;

        public RandomIterator(ZRandom r, Object arg){
            this.r = r;
            this.arg = arg;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Object next() {
            return r.num(arg);
        }
    }

    public Iterator iterator(Object arg){
        return new RandomIterator( this, arg );
    }

    private static void generate(StringBuilder builder, Automaton automaton, State state, final int minSize ) {
        List<Transition> transitions = state.getSortedTransitions(true);
        if (transitions.size() == 0) {
            assert state.isAccept();
            return;
        }
        int nroptions = state.isAccept() ? transitions.size() : transitions.size() - 1;
        if ( nroptions <= 0 ){ nroptions = 1 ; }
        int option = RANDOM.nextInt ( nroptions );
        if (state.isAccept() && option == 0  ) {
            // 0 is considered stop
            if ( builder.length() < minSize ){
                generate(builder,automaton, automaton.getInitialState(), minSize );
            }
            return;
        }
        // Moving on to next transition
        Transition transition = transitions.get(option - (state.isAccept() ? 1 : 0));
        int base = transition.getMin();
        int offSet = transition.getMax() - transition.getMin() ;
        if ( offSet <= 0 ){ offSet = 1 ; }
        char c = (char) ( RANDOM.nextInt( offSet ) + base ) ;
        builder.append(c);
        generate(builder,automaton, transition.getDest(),minSize);
    }

    /**
     * See https://github.com/bluezio/xeger
     * @param regex the regular expression
     * @param minSize minimum size of the regex
     * @return a random string matching the regex
     */
    public static String generate(final String regex, final int minSize){
        Automaton automaton = new RegExp(regex).toAutomaton();
        StringBuilder builder = new StringBuilder();
        generate(builder, automaton, automaton.getInitialState(), minSize );
        String value = builder.toString();
        return value ;
    }

    public static Object random(Object...args){
        if ( args.length == 0 ) return RANDOM;
        Object o = args[0];
        if ( o == null ) return RANDOM;
        if ( args.length == 1) return RANDOM.select(args[0]);
        return RANDOM.select(args[0], ZNumber.integer(args[1],1 ).intValue() );
    }

    public static boolean shuffle(Object o){
        if ( o == null ) return false;
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if ( !( o instanceof List) ){
            return false ;
        }
        List l = (List)o;
        /* https://en.wikipedia.org/wiki/Fisher–Yates_shuffle  */
        int n = l.size();
        boolean swapped = false ;
        for(int i = 0 ; i <= n-2; i++){
            int j = RANDOM.nextInt(n - i ) + i;
            Object o_i = l.get(i);
            Object o_tmp = l.get(j);
            l.set(i,o_tmp);
            l.set(j,o_i);
            swapped = swapped || i!=j ;
        }
        return swapped ;
    }

    public static String hash(Object... args) {
        if (args.length == 0) return "";
        String algorithm = HASH_MD5;
        String text = String.valueOf(args[0]);
        if (args.length > 1) {
            algorithm = text;
            text = String.valueOf(args[1]);
        }
        if ("e64".equalsIgnoreCase(algorithm)) {
            // do base 64 encoding
            return Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
        }
        if ("d64".equalsIgnoreCase(algorithm)) {
            // do base 64 decoding
            byte[] barr = Base64.getDecoder().decode(text.getBytes(StandardCharsets.UTF_8));
            return new String(barr);
        }

        try {
            MessageDigest m = MessageDigest.getInstance(algorithm);
            m.update(text.getBytes(), 0, text.length());
            BigInteger bi = new BigInteger(1, m.digest());
            return bi.toString(16);
        } catch (Exception e) {

        }
        return new Integer(text.hashCode()).toString();
    }

    public Object select(Object o){
        if ( o instanceof Enum ){
            o = o.getClass().getEnumConstants();
        }
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if ( o instanceof CharSequence ){
            int size = ((CharSequence) o).length();
            return ((CharSequence) o).charAt( RANDOM.nextInt(size)  );
        }
        if (o instanceof List) {
            int size = ((List) o).size();
            return ((List) o).get( RANDOM.nextInt(size) );
        }
        if ( o instanceof Number ){
            return RANDOM.num(o);
        }
        if ( o instanceof Boolean ){
            return RANDOM.bool();
        }
        return RANDOM;
    }

    public Object select(Object o, int count){
        if ( count == 0 )  return RANDOM;
        if ( o instanceof String ){
            // generate the string based on regex ... how cool is that?
            return generate((String)o, count);
        }
        if ( count == 1 ) return select(o);

        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if (o instanceof List) {
            int size = ((List) o).size();
            if ( count >= size ) return o;
            HashSet<Integer> indices = new HashSet<>();
            while ( indices.size() != count  ){
                int i = RANDOM.nextInt( size );
                indices.add(i);
            }
            ZList l = new ZList();
            for ( Integer i : indices ){
                l.add( ((List) o).get(i));
            }
            indices.clear();
            return l;
        }
        return RANDOM ;
    }

    public boolean bool(){ return nextBoolean(); }

    public Number numRange(ZNumber fromIncluded, ZNumber toExcluded){
        Number range = (Number) toExcluded._sub_( fromIncluded );
        if ( range instanceof Integer ){
            int i = nextInt( range.intValue() ) ;
            return (Number) fromIncluded._add_( i );
        }
        ZNumber r = new ZNumber( range );
        Number partitions = (Number) r._div_( Integer.MAX_VALUE );
        // pick a random partition first ?
        Number p = numRange( new ZNumber(0), new ZNumber(partitions ) );
        ZNumber result = new ZNumber(p);
        result.mul_mutable( Integer.MAX_VALUE );
        result.add_mutable( fromIncluded );
        result.add_mutable( nextInt( Integer.MAX_VALUE ) );
        return result.actual();
    }

    public Number num(Object...args){
        if ( args.length == 0 ) return nextInt();
        if ( args.length == 2 ) return numRange( new ZNumber(args[0]) , new ZNumber(args[1]) );
        if ( args[0] instanceof Integer ){
            return nextInt((int)args[0]);
        }
        if ( args[0] instanceof Long ){
            return nextLong();
        }

        if ( args[0] instanceof Double ){
            return nextDouble();
        }
        if ( args[0] instanceof String ){
            String s  = (String) args[0];
            Number n = ZNumber.number(s);
            if ( n == null ) return nextInt();
            if ( n instanceof Integer ){
                // large... INT
                int in = n.intValue();
                StringBuilder buf = new StringBuilder();
                while ( in > 0 ){
                    buf.append( nextInt( Integer.MAX_VALUE ) );
                    --in;
                }
                return new Apint( buf.toString() );
            }
        }
        return nextGaussian();
    }

    public static void sortAscending(Object[] args, Function cmp){
        if ( args.length == 0 ) return ;
        Object o = args[0];
        if ( o == null ) return  ;
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if ( cmp == null ){
            if ( o instanceof List ){
                Collections.sort( (List)o);
            }
            return;
        }
        if ( o instanceof List ){
            Function.ComparatorLambda cl = new Function.ComparatorLambda(o,cmp);
            Collections.sort( (List)o, cl );
        }
    }

    public static void sortDescending(Object[] args, Function cmp){
        if ( args.length == 0 ) return ;
        Object o = args[0];
        if ( o == null ) return  ;
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if ( cmp == null ){
            if ( o instanceof List ){
                Collections.sort( (List)o, Collections.reverseOrder() );
            }
            return;
        }
        if ( o instanceof List ){
            Function.ComparatorLambda cl = new Function.ComparatorLambda(o,cmp);
            Collections.sort( (List)o, Collections.reverseOrder(cl) );
        }
    }

    public static Number sum(Function scalar, Object[] args){
        if ( args.length == 0 ) return 0;
        Object o;
        if ( args.length == 1){
            o = args[0];
        }else{
            o = args;
        }
        if ( o == null ) return 0;
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        Iterator i;
        if ( o instanceof Map ){
            o = ((Map)o).entrySet();
        }
        if ( o instanceof Iterable ) {
            i = ((Iterable) o).iterator();
        } else if ( o instanceof Iterator ){
            i = (Iterator)o;
        }else{
            return 0;
        }
        ZNumber zz = new ZNumber(0);
        if ( scalar != null ){
            Function.ComparatorLambda cl = new Function.ComparatorLambda(o,scalar);
            int index = -1;
            while (i.hasNext()) {
                Object x = i.next();
                index++;
                Number n;
                try {
                    n = cl.num(x, index, zz.actual());
                    zz.add_mutable(n);
                }catch ( ZException.MonadicException ze){
                    if ( !ze.isNil() ){
                        zz.add_mutable( ze.value() );
                    }
                    if ( ze instanceof ZException.Break ){
                        break;
                    }
                }
            }
        }
        else {
            while (i.hasNext()) {
                Object x = i.next();
                Number n = ZNumber.number(x, 0);
                zz.add_mutable(n);
            }
        }
        return zz.actual();
    }

    public static final Object[] EMPTY_PAIR = new Object[ ] {  Function.NIL , Function.NIL  };

    public static Object minMax(List<Function> anons,Object[] args){
        if ( args.length == 0 ) return 0;
        Object o;
        if ( args.length == 1){
            o = args[0];
        }else{
            o = args;
        }
        if ( o == null ) return EMPTY_PAIR;

        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        Iterator i;
        if ( o instanceof Map ){
            o = ((Map)o).entrySet();
        }
        if ( o instanceof Iterable ) {
            i = ((Iterable) o).iterator();
        } else if ( o instanceof Iterator ){
            i = (Iterator)o;
        }else{
            return EMPTY_PAIR;
        }

        if ( !i.hasNext() ){ return EMPTY_PAIR  ; }

        Object min = i.next();
        Object max = min;
        Function cmp = null ;
        Function map = null ;

        if ( !anons.isEmpty()){
            cmp = anons.get(0);
            if ( anons.size() > 1 ){
                map = anons.get(1);
            }
        }

        if ( cmp != null ){
            Function.ComparatorLambda cl = new Function.ComparatorLambda(o,cmp);
            while (i.hasNext()) {
                Object x = i.next();

                boolean smaller = cl.compare(x,min ) < 0 ;
                if ( smaller ){
                    if ( map != null ){
                        Function.MonadicContainer mc = map.execute(-1, x , o, NIL );
                        x = mc.value();
                    }
                    min = x ;
                }else{
                    boolean larger = cl.compare(max,x ) < 0  ;
                    if ( larger ){
                        if ( map != null ){
                            Function.MonadicContainer mc = map.execute(-1, x , o, NIL );
                            x = mc.value();
                        }
                        max = x ;
                    }
                }
            }
        }
        else {
            while (i.hasNext()) {
                Object x = i.next();
                if ( x instanceof Comparable ){
                    boolean smaller = ((Comparable) x).compareTo( min ) < 0 ;
                    if ( smaller ){
                        min = x ;
                    }else{
                        boolean larger = ((Comparable) x).compareTo( max ) > 0 ;
                        if ( larger ){
                            max = x ;
                        }
                    }
                }
            }
        }
        return new Object[]{ min, max } ;
    }

    public static Object tokens(List<Function> anons, Object[] args){
        if ( args.length == 0 ) return "";
        Pattern p;
        if ( args[0] instanceof Pattern ){
            p = (Pattern)args[args.length-1];
        }else{
            p = Pattern.compile(String.valueOf(args[args.length-1]) ) ;
        }
        if ( args.length == 1 ) return p;
        String matchString = String.valueOf( args[0] );
        ArrayList l = new ArrayList();
        Matcher m = p.matcher(matchString);
        while ( m.find() ){
            l.add( m.group() );
        }
        if ( anons.isEmpty() ) return l;
        return BaseZCollection.compose( l, new ZList(), anons );
    }
}
