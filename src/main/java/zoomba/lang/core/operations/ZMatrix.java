package zoomba.lang.core.operations;

import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.io.ZTextDataFile;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.io.ZTextDataFile;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.core.types.ZXml;

import java.io.PrintStream;
import java.util.*;
import java.util.regex.Pattern;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public interface ZMatrix extends Collection {

    Function ALL = Function.TRUE ;

    Function NONE = Function.FALSE ;

    String SPACING = "\u00F8\u00D8" ;

    final class Tuple implements Map{

        Map<String,Integer> columns;

        List values;

        final int size;

        public Tuple(Map<String,Integer> columns, List values){
            this.columns = columns ;
            this.values = values ;
            size = columns.size();
            if ( size != values.size() )
                throw new UnsupportedOperationException("Can not make tuple with non matching name/value sizes");
        }

        public List project(Object...columns){
            if ( columns.length == 0 ) return Collections.EMPTY_LIST ;
            Object[] p = new Object[ columns.length ];
            for ( int i = 0 ; i < columns.length ;i++ ){
                p[i] = get( columns[i] );
            }
            return new ZArray(p,false);
        }

        @Override
        public void clear() {

        }

        @Override
        public int size() {
            return size ;
        }

        @Override
        public boolean isEmpty() {
            return values.isEmpty() ;
        }

        @Override
        public boolean containsKey(Object key) {
            if ( key instanceof Integer ){
                int i = (int)key;
                return  ( i >=0 && i < size ) ;
            }
            if ( key instanceof String ){
                return columns.containsKey(key);
            }
            return false ;
        }

        @Override
        public boolean containsValue(Object value) {
            return values.contains(value );
        }

        @Override
        public Object get(Object key) {
            if ( key instanceof Integer ){
                int i = (int)key;
                if  ( i >=0 && i < size ) {
                    return values.get(i);
                }
                return Function.NIL;
            }
            if ( key instanceof String ){
                if ( columns.containsKey(key ) ) return values.get( columns.get(key) );
                return Function.NIL;
            }
            return Function.NIL;
        }

        @Override
        public Object put(Object key, Object value) {
            return Function.NIL ;
        }

        @Override
        public Object remove(Object key) {
            return Function.NIL;
        }

        @Override
        public void putAll(Map m) {

        }

        @Override
        public Set keySet() {
            return Collections.EMPTY_SET  ;
        }

        @Override
        public Collection values() {
            return values;
        }

        @Override
        public Set<Entry> entrySet() {
            return Collections.EMPTY_SET  ;
        }

        @Override
        public boolean equals(Object obj) {
            if ( obj == null ) return false ;
            if ( !(obj instanceof Tuple) ) return false ;
            Tuple o = (Tuple)obj;
            for ( int i = 0 ; i < size; i++ ){
                Object her = o.get(i);
                Object mine = get(i);
                if ( !mine.equals(her) ) return false ;
            }
            return true;
        }

        @Override
        public String toString() {
            return String.format( "< %s >" , this.values );
        }
    }

    final class MatrixIterator implements Iterator{

        final ZMatrix matrix ;

        int pos;

        public MatrixIterator(ZMatrix matrix){
            this.matrix = matrix ;
            pos = 0 ;
        }

        @Override
        public boolean hasNext() {
            return pos + 1 < matrix.rows();
        }

        @Override
        public Object next() {
            return matrix.tuple(++pos);
        }
    }

    Map<String,Integer> columns();

    int rows();

    Tuple tuple(int rowNum);

    List select(Function f, Object...columns);

    int index(Function f);

    Map<String,List<Integer>> keys(Function f);

    Map<String,List<Integer>> keys(Object...columns);

    final class Matrix extends BaseZMatrix{

        List headers;

        List<List> data;

        public Matrix(List headers, List data){
            this.headers = headers ;
            this.data = data ;
        }

        @Override
        public List row(int rowNum) {
            if ( rowNum == 0 ) return headers;
            return data.get(rowNum - 1 );
        }

        @Override
        public int rows() {
            return data.size() + 1 ;
        }

        @Override
        public String toString() {
            return String.format( "Matrix: [ %s , rows %d ]" , headers , rows() );
        }
    }

    abstract class BaseZMatrix implements ZMatrix{

        public abstract List row(int rowNum);

        protected Map<String,List<Integer>> keys;

        private Map<String,Integer> columns;

        public List<String> headers(){
            return row(0);
        }

        @Override
        public Map<String, Integer> columns() {
            if ( columns == null ){
                columns = new HashMap<>();
                List<String> data = headers();
                for ( int i = 0 ; i < data.size(); i++ ){
                    columns.put( data.get(i), i);
                }
                if ( data.size() != columns.size() ){
                    throw new UnsupportedOperationException("Duplicate Column Names!");
                }
            }
            return columns ;
        }

        @Override
        public Map<String, List<Integer>> keys(Object... columns) {
            if ( keys != null ) keys.clear();
            keys = new HashMap<>();
            if ( columns.length == 1 && columns[0] instanceof Function ){
                return keys((Function)columns[0]);
            }
            if ( columns.length == 0 ){
                int size = this.columns().size();
                // we must now use all the columns...
                columns = new Object[ size ];
                for ( int i = 0 ; i < size; i++ ){ columns[i] = i ;}
            }

            Iterator i = iterator();
            StringBuilder buf = new StringBuilder();
            int rowNum = 1 ;
            while ( i.hasNext() ){
                Tuple t = (Tuple)i.next();
                for ( Object c : columns ){
                    buf.append( t.get(c)).append(SPACING);
                }
                String key = buf.toString();
                buf.setLength(0);
                if ( keys.containsKey(key ) ){
                    List l = keys.get(key);
                    l.add( rowNum );
                }else{
                    List l = new ArrayList();
                    l.add(rowNum);
                    keys.put(key,l);
                }
                rowNum++;
            }
            return keys;
        }

        @Override
        public Map<String, List<Integer>> keys(Function f) {
            if ( keys != null ) keys.clear();
            keys = new HashMap<>();
            int index = 1 ;
            Iterator i = iterator() ;
            while ( i.hasNext() ){
                Object o = i.next();
                Function.MonadicContainer result = f.execute(index,o,this,keys);
                if ( !result.isNil() ){
                    String key = String.valueOf(result.value());
                    if ( keys.containsKey(key ) ){
                        List l = keys.get(key);
                        l.add( index );
                    }else{
                        List l = new ArrayList();
                        l.add(index);
                        keys.put(key,l);
                    }
                }
                if ( result instanceof ZException.Break) return keys ;
                index++;
            }
            return keys;
        }

        @Override
        public int index(Function f) {
            return BaseZCollection.leftIndex( this, f );
        }

        @Override
        public List select(Function f,Object...columns) {
            boolean shouldProject = ( columns.length != 0 ) ;
            int index = 1 ;
            ZList into = new ZList();
            Iterator i = iterator() ;
            while ( i.hasNext() ){
                Object o = i.next();
                Function.MonadicContainer result = f.execute(index,o,this,into);
                if ( !result.isNil() && ZTypes.bool(result.value() ) ){
                    List l ;
                    if ( shouldProject ){
                        l = ((Tuple)o).project(columns);
                    }else{
                        l = ((Tuple)o).values ;
                    }
                    into.add(l);
                }
                if ( result instanceof ZException.Break) return into;
                index++;
            }
            return into;
        }

        public ZMatrix project(Object...columns){
            return sub( ALL, columns );
        }

        public ZMatrix sub(Function f, Object...columns){
            List<List> data = select(f,columns);
            Tuple ht = new Tuple( columns() , headers() );
            return new Matrix( ht.project( columns ) , data );
        }

        @Override
        public Tuple tuple(int rowNum) {
            List row = row(rowNum);
            Map<String,Integer> columns = columns();
            return new Tuple(columns,row);
        }

        @Override
        public boolean add(Object o) {
            return false;
        }

        @Override
        public boolean addAll(Collection c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection c) {
            return false;
        }

        @Override
        public boolean isEmpty() {
            // header is not counted in...
            return rows() < 2 ;
        }

        @Override
        public Iterator iterator() {
            return new MatrixIterator(this);
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean removeAll(Collection c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection c) {
            return false;
        }

        @Override
        public int size() {
            return rows() ;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public Object[] toArray(Object[] a) {
            return new Object[0];
        }

        public void write(String file,String sep){
            PrintStream ps = (PrintStream)ZFileSystem.open(file,"w");
            for ( int i = 0 ; i < size() - 1 ; i++ ){
                ps.printf("%s\n", ZTypes.string( row(i), sep) );
            }
            ps.close();
        }

        public void write(String file){
            write(file,"\t");
        }
    }

    final class EmptyMatrix extends BaseZMatrix{

        @Override
        public List row(int rowNum) {
            return Collections.EMPTY_LIST ;
        }

        private EmptyMatrix(){}

        @Override
        public Map<String,Integer> columns() {
            return Collections.EMPTY_MAP  ;
        }

        @Override
        public int rows() {
            return 0;
        }


        @Override
        public List select(Function f, Object...columns) {
            return Collections.EMPTY_LIST;
        }

        @Override
        public int index(Function f) {
            return -1;
        }

        @Override
        public Iterator iterator() {
            return Collections.EMPTY_LIST.iterator();
        }

        @Override
        public String toString() {
            return "EmptyMatrix{}";
        }

    }

    EmptyMatrix EMPTY_MATRIX = new EmptyMatrix();

    interface ZMatrixLoader{

        Pattern loadPattern();

        ZMatrix load(String path, Object...args);

    }

    final class Loader{

        public static final Map<Pattern,ZMatrixLoader> loaders = new HashMap<>( );

        static {
            // some hard coding -- bad later should go to config
            add( ZTextDataFile.ZTextDataFileLoader.LOADER  );
        }

        public static boolean add(ZMatrixLoader l){
            Pattern p = l.loadPattern();
            if ( loaders.containsKey(p) ) return false ;
            loaders.put(p,l);
            return true;
        }

        public static boolean remove(ZMatrixLoader l){
            Pattern p = l.loadPattern();
            if ( !loaders.containsKey(p) ) return false ;
            loaders.remove(p);
            return true;
        }

        public static ZMatrix load(String path, Object...args){
            for ( Pattern p : loaders.keySet() ){
                if ( p.matcher(path).matches() ){
                    ZMatrixLoader loader = loaders.get(p);
                    return loader.load(path,args);
                }
            }
            return EMPTY_MATRIX;
        }

        public static ZMatrix load(Object...args){
            if ( args.length == 0 ) return EMPTY_MATRIX;
            if ( args[0] instanceof List ){
                // Load appropriate from data
                if ( args.length > 1 ){
                    return new Matrix( (List)args[0], (List)args[1] );
                }
                List<List> all = ((List<List>)args[0]);
                List headers = all.get(0);
                List<List> data  = all.subList(1,all.size());
                return new Matrix( headers, data );
            }
            String path = String.valueOf( args[0] );
            args = ZTypes.shiftArgsLeft(args);
            return load(path,args);
        }
    }

}
