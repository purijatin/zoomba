package zoomba.lang.core.operations;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.types.ZException;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;


/**
 */
public final class ZJVMAccess {

    public static Map dict(Object o){
        if ( o == null ) return null  ;
        Field[] fields = o.getClass().getDeclaredFields();
        Map m = new HashMap();
        for ( Field f : fields ){
            f.setAccessible(true);
            String name = f.getName();
            try {
                Object value = f.get(o);
                m.put(name,value);
            }catch (Exception e){ }
        }
        return Collections.unmodifiableMap(m);
    }

    public static boolean isPrimitiveMatch(Class pType, Class aType){
        if ( pType == boolean.class && aType == Boolean.class ) return true;
        if ( Number.class.isAssignableFrom(aType) ){
            return  pType == int.class   ||  pType == long.class ||
                    pType == short.class ||  pType == byte.class ||
                    pType == float.class ||  pType == double.class ;
        }
        if ( pType == char.class && aType == Character.class ) return true;
        return false;
    }

    public static boolean parameterMatches(Class[] pTypes, Object[] args){
        boolean match = true ;
        for ( int i = 0 ; i < args.length; i++ ){
            if ( args[i] == null ) continue; // legal
            Class pType = pTypes[i];
            Class aType = args[i].getClass();
            if ( pType.isAssignableFrom( aType ) ) continue;
            if ( pType.isPrimitive() && isPrimitiveMatch(pType,aType) ) continue ;
            match = false;
            break;
        }
        return match ;
    }

    public static Object construct(Object classType, Object[] args){
        try{
            Class clazz = null;
            if ( classType instanceof CharSequence ){
                clazz = Class.forName(classType.toString());
            }
            if ( clazz == null ){
                if ( classType instanceof Class ){
                    clazz = (Class) classType ;
                }
            }
            if ( clazz == null )
                throw new UnsupportedOperationException("Unknown type to create : " + classType.getClass());

            if ( args == null || args.length == 0 ){
                return clazz.newInstance();
            }
            Constructor[] constructors = clazz.getConstructors();
            for ( Constructor c : constructors ){
                try{
                    // violate every norm of OOPS!
                    c.setAccessible(true);
                }catch (Throwable t){}
                if ( args.length == c.getParameterCount() ){
                    // reasonable guess, now type match
                    Class[] pTypes =  c.getParameterTypes();
                    boolean match = parameterMatches(pTypes,args);
                    if ( match ) {
                        Object instance = c.newInstance(args);
                        return instance;
                    }
                }
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        throw new UnsupportedOperationException("args did not match any known constructor for class : " + classType.toString() );
    }

    public static void before(ZEventAware before, Method m, Object[] args){
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg( before, m, args);
        try {
            before.before(event);
        }catch (Throwable t){

        }
    }

    public static void after(ZEventAware after, Method m, Object[] args){
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg( after, m, args);
        try {
            after.after(event);
        }catch (Throwable t){

        }
    }

    public static Object callMethod(Object instance, String name, Object[] args){
        try{
            if ( instance == null )
                throw new UnsupportedOperationException("Can not call method on null! ");

            boolean isStatic = false ;
            Method[] methods ;
            Class clazz = null;
            if ( instance instanceof Class ){
                clazz = ((Class)instance);
                methods = ((Class)instance).getDeclaredMethods();
                isStatic = true ;
            }else{
                clazz = instance.getClass();
                methods = clazz.getDeclaredMethods();
            }

            while ( clazz != Object.class ){
                for ( Method m : methods ){
                    try{
                        m.setAccessible(true);
                    }catch (Throwable t){}

                    String mName = m.getName();
                    if ( mName.equals(name)  ){
                        boolean isVarArgs = m.isVarArgs();
                        boolean proceed = args.length == m.getParameterCount()
                                || isVarArgs ;
                        if ( !proceed ) continue;

                        // reasonable guess, now type match
                        Class[] pTypes =  m.getParameterTypes();
                        boolean match = isVarArgs  || parameterMatches(pTypes,args) ;
                        if ( match ) {
                            if ( isVarArgs ){
                                int numPar = m.getParameterCount();
                                boolean wrapLast = (args.length) > 0 &&  numPar <= args.length ;
                                // last stuff must be wrapped inside Object[] so...
                                if ( wrapLast ) {
                                    if ( numPar != 1 ) {
                                        Object varArgs = args[args.length - 1];
                                        args[args.length - 1] = new Object[]{varArgs};
                                    }else{
                                        // fully variable length, then wrap into the
                                        args = new Object[ ] { args };
                                    }
                                }
                                else{
                                    // last param must be a empty Object[] ...
                                    Object[] newArgs = new Object[ args.length + 1];
                                    for ( int i = 0 ; i < args.length ; i++ ){
                                        newArgs[i] = args[i];
                                    }
                                    newArgs[newArgs.length-1] = ZArray.EMPTY_ARRAY ;
                                    args = newArgs ;
                                }
                            }
                            Object instanceObject = isStatic ? null : instance ;
                            if ( instanceObject instanceof ZEventAware ){
                                before((ZEventAware)instanceObject, m, args);
                            }
                            try {
                                Object r = m.invoke(instanceObject, args);
                                if (m.getReturnType() == Void.TYPE) {
                                    return Function.Void;
                                }
                                return r;
                            }finally {
                                if ( instanceObject instanceof ZEventAware ){
                                    after((ZEventAware)instanceObject, m, args);
                                }
                            }
                        }
                    }
                }
                clazz = clazz.getSuperclass();
                methods = clazz.getDeclaredMethods();
            }

        }catch (Exception e){
            throw new RuntimeException(e);
        }
        throw new UnsupportedOperationException
                ("args did not match any known method! : " + name + " : in class : " + instance.getClass() );
    }

    public final static Function.MonadicContainer INVALID_ACCESSOR = new Function.MonadicContainerBase();

    public final static Function.MonadicContainer INVALID_PROPERTY = new Function.MonadicContainerBase();


    final static boolean VALID_ACCESS(Function.MonadicContainer c){
        return ( c != INVALID_ACCESSOR && c != INVALID_PROPERTY );
    }

    static Field getField(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Field[] fields = ((Class) o).getDeclaredFields();
            for ( Field f : fields ){
                f.setAccessible(true);
                if ( f.getName().equals(prop) ) return f;
            }
            return getField(((Class) o).getSuperclass(),prop);
        }
        return getField(o.getClass(),prop);
    }

    static Method getGetterMethod(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Method[] methods = ((Class) o).getDeclaredMethods();
            for ( Method m : methods ){
                m.setAccessible(true);
                if ( m.getName().equals(prop) && m.getParameterCount() == 0 ) return m;
            }
            return getGetterMethod(((Class) o).getSuperclass(),prop);
        }
        return getGetterMethod(o.getClass(),prop);
    }

    static Method getSetterMethod(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Method[] methods = ((Class) o).getDeclaredMethods();
            for ( Method m : methods ){
                m.setAccessible(true);
                if ( m.getName().equals(prop) && m.getParameterCount() > 0 ) return m;
            }
            return getSetterMethod(((Class) o).getSuperclass(),prop);
        }
        return getSetterMethod(o.getClass(),prop);
    }

    public interface NameProperty{

        Function.MonadicContainer getValue(Object instance, String propertyName);

        Function.MonadicContainer setValue(Object instance, String propertyName, Object value);

    }

    final static class FieldAccess implements NameProperty {

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            Field f = getField(instance,propertyName);
            if ( f == null ) return INVALID_PROPERTY ;
            try{
                return new Function.MonadicContainerBase( f.get(instance) ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            Field f = getField(instance,propertyName);
            if ( f == null ) return INVALID_PROPERTY ;
            try{
                f.set(instance,value) ;
                return Function.SUCCESS ;
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static FieldAccess FIELD_ACCESS = new FieldAccess() ;

    final static class MethodAccess implements NameProperty {

        public static String doCamelCasing(String propertyName){
            return  Character.toUpperCase( propertyName.charAt(0) ) + propertyName.substring(1) ;
        }

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            String methodName = "get" + doCamelCasing(propertyName);
            Method m = getGetterMethod(instance, methodName );
            if ( m == null ) {
                // perhaps it has a function instance.propertyName()?
                m = getGetterMethod(instance, propertyName );
                if ( m == null ) {
                    // perhaps boolean isXXX?
                    methodName = "is" + doCamelCasing(propertyName);
                    m = getGetterMethod(instance, methodName );
                    if ( m == null ) return INVALID_PROPERTY ;
                }
            }
            try{
                return new Function.MonadicContainerBase( m.invoke(instance) ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            String methodName = "set" + doCamelCasing(propertyName);
            Method m = getSetterMethod(instance, methodName );
            if ( m == null ) {
                // perhaps it has a function instance.propertyName()?
                m = getSetterMethod(instance, propertyName );
                if ( m == null ) return INVALID_PROPERTY ;
            }
            try{
                Object o = m.invoke(instance,value) ;
                if ( Void.TYPE.equals(m.getReturnType() ) ){
                    return Function.SUCCESS ;
                }
                return new Function.MonadicContainerBase(o);
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static MethodAccess METHOD_ACCESS = new MethodAccess();

    public interface IndexProperty{

        Function.MonadicContainer getIndexedValue(Object instance, Object index);

        Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value);
    }

    final static class PropertyBucket implements IndexProperty{

        public static Method getDuckGet(Object o){
            if ( o == null ) return null;
            if ( o instanceof Class ){
                if ( o == Object.class ) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for ( Method m : methods ){
                    m.setAccessible(true);
                    if ( m.getName().equals("get") && m.getParameterCount() == 1 ) return m;
                }
                return getDuckGet(((Class) o).getSuperclass());
            }
            return getDuckGet(o.getClass());
        }

        public static Method getDuckSet(Object o){
            if ( o == null ) return null;
            if ( o instanceof Class ){
                if ( o == Object.class ) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for ( Method m : methods ){
                    m.setAccessible(true);
                    if ( m.getName().equals("set") && m.getParameterCount() == 2 ) return m;
                }
                return getDuckSet(((Class) o).getSuperclass());
            }
            return getDuckSet(o.getClass());
        }


        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object propertyName) {
            try{
                if ( instance == null ) return INVALID_PROPERTY ;
                if ( instance instanceof Map ) return MAP_ACCESS.getIndexedValue(instance,propertyName);
                Method m =  getDuckGet( instance );
                if ( m == null ) return INVALID_PROPERTY ;
                return new Function.MonadicContainerBase( m.invoke(instance, propertyName )  ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object propertyName, Object value) {
            try{
                if ( instance == null ) return INVALID_PROPERTY ;
                if ( instance instanceof Map ) return MAP_ACCESS.setIndexedValue(instance,propertyName,value );
                Method m = getDuckSet( instance );
                if ( m == null ) return INVALID_PROPERTY ;
                Object o = m.invoke(instance, propertyName, value) ;
                if ( Void.TYPE.equals( m.getReturnType() ) ){
                    return Function.SUCCESS ;
                }
                return new Function.MonadicContainerBase(o) ;
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static PropertyBucket PROPERTY_BUCKET = new PropertyBucket();

    final static class MapAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if ( !(instance instanceof Map )) return INVALID_ACCESSOR ;
            if ( ((Map) instance).containsKey(index) )
                return new Function.MonadicContainerBase( ((Map) instance).get(index));
            return INVALID_PROPERTY ;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if ( !(instance instanceof Map )) return INVALID_ACCESSOR;
            Object o = ((Map) instance).put(index,value);
            return new Function.MonadicContainerBase(o) ;
        }
    }

    final static MapAccess MAP_ACCESS = new MapAccess();

    final static class ArrayAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (! instance.getClass().isArray() ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    return new Function.MonadicContainerBase(Array.get(instance, (int) index));
                }catch (ArrayIndexOutOfBoundsException e){
                    try {
                        int len = Array.getLength(instance);
                        return new Function.MonadicContainerBase(Array.get(instance, len + (int) index));
                    }catch (ArrayIndexOutOfBoundsException e1){
                    }
                    return INVALID_PROPERTY ;
                }
            }

            if ( index != null && index.getClass().isArray() ){
                try{
                    int left = (int)((Object[])index)[0];
                    int right = (int)((Object[])index)[1];
                    int len = Array.getLength(instance);
                    if ( left < 0 ){ left += len ; }
                    if ( right < 0 ){ right += len ; }
                    if ( left <= right && left >=0 ){
                        // inclusive
                        Object[] arr = new Object[ right -left + 1 ];
                        for ( int i = left; i<=right; i++ ){
                            arr[i-left] = Array.get(instance, i);
                        }
                        return new Function.MonadicContainerBase( arr );
                    }

                }catch (Exception e){

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (! instance.getClass().isArray() ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    Array.set(instance, (int) index, value);
                    return Function.SUCCESS;
                }catch (ArrayIndexOutOfBoundsException e){
                    return INVALID_PROPERTY ;
                }
            }
            return INVALID_ACCESSOR ;
        }
    }

    final static ArrayAccess ARRAY_ACCESS = new ArrayAccess();

    final static class ListAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (! (instance instanceof List) ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    return new Function.MonadicContainerBase(((List) instance).get((int) index));
                }catch (IndexOutOfBoundsException e){
                    return INVALID_PROPERTY ;
                }
            }
            if ( index != null && index.getClass().isArray() ){
                try{
                    List myList = ((List) instance);
                    int left = (int)((Object[])index)[0];
                    int right = (int)((Object[])index)[1];
                    int len = myList.size() ;
                    if ( left < 0 ){ left += len ; }
                    if ( right < 0 ){ right += len ; }
                    List slice = new ZList();
                    if ( left <= right && left >=0 ){
                        // inclusive
                        for ( int i = left; i<=right; i++ ){
                            slice.add( myList.get(i) );
                        }
                        return new Function.MonadicContainerBase( slice );
                    }

                }catch (Exception e){

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (! (instance instanceof List)  ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    Object o = ((List) instance).set((int) index, value);
                    return new Function.MonadicContainerBase(o);
                }catch (IndexOutOfBoundsException e){
                    return INVALID_PROPERTY ;
                }
            }
            return INVALID_PROPERTY ;
        }
    }

    final static ListAccess LIST_ACCESS = new ListAccess();

    public static Function.MonadicContainer getProperty(Object instance, Object property){
        if ( instance == null ) throw new UnsupportedOperationException("Can not get property of null!");
        String prop = String.valueOf(property);
        switch ( prop ){
            case "hashCode" :
                return new Function.MonadicContainerBase( instance.hashCode() );
            case "class" :
                return new Function.MonadicContainerBase(instance.getClass());
            case "toString" :
                return new Function.MonadicContainerBase(instance.toString());
        }

        Function.MonadicContainer r ;
        if ( instance.getClass().isArray() ){
            r = ARRAY_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;
            if ( "length".equals(property) ) return new Function.MonadicContainerBase( Array.getLength(instance));
        }
        boolean isMap = false ;
        if ( instance instanceof Map ){
            isMap = true ;
            r = MAP_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;

            switch (prop){
                case "size" :
                case "length" :
                    return new Function.MonadicContainerBase( ((Map) instance).size() );
                case "keys" :
                case "keySet":
                    return new Function.MonadicContainerBase( ((Map) instance).keySet() );
                case "values" :
                    return new Function.MonadicContainerBase( ((Map) instance).values() );
                case "entries" :
                case "entrySet" :
                    return new Function.MonadicContainerBase( ((Map) instance).entrySet() );
            }

        }
        if ( instance instanceof List ){
            r = LIST_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;
            switch (prop) {
                case "size":
                case "length":
                    return new Function.MonadicContainerBase(((List) instance).size());
            }
        }
        // for CharSequences
        if ( instance instanceof CharSequence ){
            String s = instance.toString();
            if ( property instanceof Integer ) {
                int index = (int) property;
                try {
                    return new Function.MonadicContainerBase(s.charAt(index));
                } catch (IndexOutOfBoundsException e) {
                    index += s.length(); // perhaps -ve index ?
                    try {
                        return new Function.MonadicContainerBase(s.charAt(index));
                    } catch (IndexOutOfBoundsException e1) {
                    }
                }
            }
            if ( property.getClass().isArray() ){
                try {
                    int start = (int) Array.get(property, 0);
                    int end = (int) Array.get(property, 1);
                    int len = s.length() ;
                    if ( start < 0 ) { start = len + start ; }
                    if ( end < 0 ) { end = len + end ; }
                    return new Function.MonadicContainerBase(s.substring(start, end + 1));
                }catch (Exception e){}
            }
        }

        // now try getter/setter
        r = METHOD_ACCESS.getValue(instance,prop);
        if ( VALID_ACCESS(r)) return r;
        if ( !isMap ) {
            // try Bucket Access only when NOT map?
            r = PROPERTY_BUCKET.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;
        }
        // and now, go back to field access
        r = FIELD_ACCESS.getValue(instance,prop);
        if ( VALID_ACCESS(r)) return r;
        // nothing... hence...
        return INVALID_PROPERTY ;
    }

    public static Function.MonadicContainer setProperty(Object instance, Object property, Object value){
        if ( instance == null ) throw new UnsupportedOperationException("Can not set property of null!");
        if ( instance.getClass().isArray() ){
            return ARRAY_ACCESS.setIndexedValue(instance,property,value);
        }
        if ( instance instanceof List ){
            return LIST_ACCESS.setIndexedValue(instance,property,value);
        }
        if ( instance instanceof Map ){
            return MAP_ACCESS.setIndexedValue(instance,property,value);
        }
        String prop = String.valueOf(property);

        Function.MonadicContainer r = METHOD_ACCESS.setValue(instance,prop,value);
        if ( VALID_ACCESS(r) ) return r;

        r = PROPERTY_BUCKET.setIndexedValue(instance,property,value);
        if ( VALID_ACCESS(r) ) return r;

        r = FIELD_ACCESS.setValue(instance,prop,value);
        if ( VALID_ACCESS(r) ) return r;

        return INVALID_PROPERTY ;
    }
}
