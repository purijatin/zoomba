package zoomba.lang.core.interpreter;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.parser.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import zoomba.lang.core.interpreter.ZContext.*;

/**
 */
public class ZScript implements Function {

    public static boolean IS_GLOBAL_VAR(String s){
        return s.length() > 1 && s.charAt(0) == '$' ;
    }

    public static final String MAIN_SCRIPT = "__ZS__" ;

    public static final Pattern CURRY_PATTERN = Pattern.compile("#\\{(?<p>[^\\{]+)\\}" , Pattern.MULTILINE );

    public final Map<String,Object> globals = new ConcurrentHashMap<>();

    public final ZAssertion assertion;


    /**
     * Read from a reader into a local buffer and return a String with
     * the contents of the reader.
     *
     * @param scriptReader to be read.
     * @return the contents of the reader as a String.
     * @throws IOException on any error reading the reader.
     */
    public static String readerToString(Reader scriptReader) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader;
        if (scriptReader instanceof BufferedReader) {
            reader = (BufferedReader) scriptReader;
        } else {
            reader = new BufferedReader(scriptReader);
        }
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append('\n');
            }
            return buffer.toString();
        } finally {
            try {
                reader.close();
            } catch (IOException xio) {
                // ignore
            }
        }

    }

    /**
     * Trims the expression from front and ending spaces.
     *
     * @param str expression to clean
     * @return trimmed expression ending in a semi-colon
     */
    public static String cleanExpression(CharSequence str) {
        if (str != null) {
            int start = 0;
            int end = str.length();
            if (end > 0) {
                // trim front spaces
                while (start < end && str.charAt(start) == ' ') {
                    ++start;
                }
                // trim ending spaces
                while (end > 0 && str.charAt(end - 1) == ' ') {
                    --end;
                }
                return str.subSequence(start, end).toString();
            }
            return "";
        }
        return null;
    }


    protected final Parser parser = new Parser(new StringReader(";")); //$NON-NLS-1$

    @Override
    public String body() {
        return ""; // later
    }

    @Override
    public String name() {
        return name ;
    }


    public final String baseDir;

    public final ZScript parent;

    ZInterpret current;

    protected String name;

    protected ASTZoombaScript script ;

    public ZScript(String text)  {
        try {
            baseDir = "." ;
            parent = null ;
            script = parse(text);
            name = "__INLINE__" ;
            assertion = new ZAssertion();
            populateMethods(script);
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
    }

    public ZScript(String fileLocation, ZScript parent, String as)  {
        File f = new File(fileLocation);
        try {
            // fix for one jar issues in loading
            String dir = f.getAbsoluteFile().getParent();
            File d = new File(dir);
            baseDir = d.getCanonicalPath();
            String scriptText = readerToString( new FileReader(f) );
            script = parse(scriptText);
            this.parent = parent;
            if ( parent == null ){
                assertion = new ZAssertion();
            }else{
                assertion = parent.assertion ;
            }
            name = as ;
            populateMethods(script);
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
    }

    public ZScript(String fileLocation, ZScript parent){
        this(fileLocation,parent,MAIN_SCRIPT);
    }

    protected Map<String,ZScriptMethod> methods;

    public Object get(String name){
        if ( methods.containsKey( name ) ) return methods.get(name);
        return null;
    }

    protected void populateMethods(ASTZoombaScript script){
        methods = new HashMap<>();
        int num = script.jjtGetNumChildren();
        for ( int i = 0 ; i < num; i++ ){
            Node n = script.jjtGetChild(i);
            if ( n instanceof ASTMethodDef){
                ZScriptMethod method = new ZScriptMethod(n);
                methods.put( method.name , method );
            }
        }
    }

    /**
     * Parses an expression.
     *
     * @param expression the expression to parse
     * @return the parsed tree
     * @throws ZException if any error occured during parsing
     */
    protected ASTZoombaScript parse(CharSequence expression) {
        String expr = cleanExpression(expression);
        ASTZoombaScript script = null;
        synchronized (parser) {

            try {
                Reader reader = new StringReader(expr);
                // use first calling method of JexlEngine as debug info
                script = parser.parse(reader);
                // reaccess in case local variables have been declared

            } catch (TokenMgrError xtme) {
                throw new ZException.Tokenization(xtme, expression.toString());
            } catch (ParseException xparse) {
                throw new ZException.Parsing(xparse, expression.toString());
            } finally {

            }
        }
        return script;
    }

    private boolean debug = false ;

    public boolean debug(){ return debug ; }

    public void debug(boolean debug){ this.debug = debug ; }

    protected FunctionContext externalContext = null ;

    public void setExternalContext(ZContext.FunctionContext context){
        externalContext = context ;
    }

    public FunctionContext getExternalContext() {
        return externalContext;
    }

    @Override
    public MonadicContainer execute(Object...args){
        ZContext functionContext ;
        ArgContext argContext = new ArgContext(args) ;
        if ( externalContext == null ) {
            functionContext = new FunctionContext(ZContext.EMPTY_CONTEXT, argContext);
        }else{
            functionContext = externalContext ;
        }
        current = new ZInterpret(this);
        current.prepareCall( functionContext );
        Object o = NIL;
        try {
            o = current.dance(script);
        }catch (Throwable t){
            o = t;
        }finally {
            functionContext = current.endCall();
            if ( externalContext == null ){
                functionContext.clear();
                argContext.clear();

            }else{
                System.gc();
            }

            if ( !(functionContext.parent() instanceof ZContext.EmptyContext) ){
                System.gc();
            }
        }
        return new MonadicContainerBase(o);
    }
}
