package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTBlock;

import java.util.*;

/**
 */
public abstract class AnonymousFunctionInstance implements Function, Comparator {

    public static final class AnonymousArg implements Map {

        static final Set<String> KEY_SET = new HashSet<>();

        static {
            KEY_SET.addAll( Arrays.asList( "$" , "parent", "i", "index" , "o" , "object" , "item" ,
                    "c", "context" , "p", "partial" , "previous", "prev" ,"seed", "left" , "right" , "l", "r" ) );
        }

        Object $;

        Number i;

        Object o;

        Object c;

        Object p;

        public AnonymousArg(){
            setArgs();
        }

        public void setArgs(Object... args) {
            if  ( args.length == 0 ){
                i = -1;
                o = NIL;
                c = NIL;
                p = NIL;
                return;
            }
            i = (Number) args[0];
            o = args[1] ;
            c = args[2] ;
            p = args[3];
        }

        public ZContext.ArgContext argContext(ZContext parent){
            if ( parent.has( THIS ) ){
                this.$ = parent.get(THIS).value();
            }else{
                // absolutely necessary
                this.$ = NIL ;
            }
            return new ZContext.ArgContext(
                    Arrays.asList( THIS ) ,
                    Arrays.asList( new MonadicContainerBase( this )) );
        }

        @Override
        public int size() {
            return 5;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean containsKey(Object key) {
            return KEY_SET.contains(key) || key instanceof Integer ;
        }

        @Override
        public boolean containsValue(Object value) {
            return values().contains( value );
        }

        @Override
        public Object get(Object key) {
            if ( key instanceof Integer ){
                // becomes a convenience for join
                if ( o instanceof Object[] ){ return ((Object[]) o)[(int)key]; }
            }
            switch ( String.valueOf(key) ){
                case "$":
                case "parent" :
                    return $;
                case "i" :
                case "index":
                    return i;
                case "o" :
                case "object":
                case "item" :
                    return o;
                case "c" :
                case "context":
                    return c;
                case "p":
                case "partial":
                case "previous" :
                case "prev":
                case "seed":
                    return p;
                case "left":
                case "l":
                    if ( o instanceof Object[] ){ return ((Object[]) o)[0]; }
                    if ( o instanceof List ){ return ((List) o).get(0); }
                    return NIL;
                case "right":
                case "r":
                    if ( o instanceof Object[] ){ return ((Object[]) o)[1]; }
                    if ( o instanceof List ){ return ((List) o).get(1); }
                    return NIL;
                default:
                    return NIL;
            }
        }

        @Override
        public Object put(Object key, Object value) {
            switch ( String.valueOf(key) ){
                case "$":
                case "parent" :
                    return $ = value ;
                case "i" :
                case "index":
                    return i = (Number) value ;
                case "o" :
                case "object":
                case "item" :
                    return o = value;
                case "c" :
                case "context":
                    return c = value ;
                case "p":
                case "partial":
                case "previous" :
                case "prev":
                case "seed":
                    return p = value ;

            }
            return NIL;
        }

        @Override
        public Object remove(Object key) {
            return NIL;
        }

        @Override
        public void putAll(Map m) {

        }

        @Override
        public void clear() {
        }

        @Override
        public Set keySet() {
            return  KEY_SET ;
        }

        @Override
        public Collection values() {
            return Arrays.asList( $ , i, o, c, p ) ;
        }

        @Override
        public Set<Entry> entrySet() {
            Set entries = new HashSet();
            for ( Object key : KEY_SET ){
                entries.add(  new ZMap.Pair( key, get(key) ) );
            }
            return entries ;
        }
    }

    public final ZInterpret interpret;

    public final ASTBlock methodBlock;

    protected AnonymousArg anonymousArg;

    ZContext.FunctionContext myContext;


    public AnonymousFunctionInstance(ZInterpret i, ASTBlock m) {
        interpret = i;
        methodBlock = m;
        anonymousArg = new AnonymousArg();
        ZContext parent = interpret.frames.peek();
        myContext = new ZContext.FunctionContext(parent,null);
    }

    @Override
    public MonadicContainer execute(Object... args) {
        ZInterpret methodInterpreter = interpret ;
        try {
            // set args
            anonymousArg.setArgs(args);

            myContext.argContext =  anonymousArg.argContext( myContext.parent );
            // thread mismatch...
            boolean threaded = (interpret.threadId !=  Thread.currentThread().getId()) ;

            if ( threaded ) {
                methodInterpreter = new ZInterpret(interpret);
            }
            methodInterpreter.prepareCall(myContext);
            Object r = methodBlock.jjtAccept(methodInterpreter, null);
            return new MonadicContainerBase(r);

        } catch (ZException.MonadicException me) {
            return me ;
        }
        finally {
            methodInterpreter.endCall();
            myContext.argContext.clear();
        }
    }

    @Override
    public String body() {
        return methodBlock.toString();
    }

    @Override
    public int compare(Object o1, Object o2) {
        MonadicContainer mc = execute(-1, new Object[]{o1,o2},NIL,NIL);
        if ( mc.isNil() ) throw new RuntimeException("Comparator must return a value!");
        Object o = mc.value();
        Number cmp = ZNumber.number( o );
        if ( cmp != null ) return cmp.intValue();
        Boolean b = ZTypes.bool( o );
        if ( b != null ){ return b ? -1 : 1 ; }
        throw new RuntimeException("Comparator must return an integer or boolean!");
    }

    public static final class MapperLambda extends AnonymousFunctionInstance implements Mapper {

        public MapperLambda(ZInterpret i, ASTBlock m) {
            super(i, m);
        }

        @Override
        public String name() {
            return "__map__";
        }

        @Override
        public Object map(Object... args) {
            MonadicContainer mc = execute(args);
            if (mc.isNil()) {
                return anonymousArg.o;
            }
            return mc.value();
        }
    }

    public static final class PredicateLambda extends AnonymousFunctionInstance implements Predicate {

        public PredicateLambda(ZInterpret i, ASTBlock m) {
            super(i, m);
        }

        @Override
        public String name() {
            return "__select__";
        }

        @Override
        public boolean accept(Object... args) {
            MonadicContainer mc = execute(args);
            return ZTypes.bool(mc.value(), false);
        }
    }
}
