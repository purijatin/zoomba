package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Function.Mapper;
import zoomba.lang.core.operations.Function.Predicate;
import zoomba.lang.core.operations.ZEventAware;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.*;
import zoomba.lang.core.interpreter.ZContext.*;

import java.util.*;

/**
 */
public class ZScriptMethod {

    public static final String ARGS_VAR = "@ARGS" ;

    public final String name;

    public final Node methodNode;

    public final Node block;

    public final Node paramsNode;

    public final boolean isAnonymous ;

    public final boolean isVar ;

    public final ZContext outer;

    public final Set<ZScriptMethod> before = new ZSet();

    public final Set<ZScriptMethod> after = new ZSet();

    /*
    private boolean isRecursive;

    private boolean isTailRecursive;

    public void findInnerMethodCalls(Node node, List<ASTMethodNode> calls){
        if ( node instanceof ASTMethodNode ){
            calls.add( (ASTMethodNode)node );
        }
        int num = node.jjtGetNumChildren();
        for ( int i = 0 ; i < num; i++ ){
            findInnerMethodCalls( node.jjtGetChild(i), calls );
        }
    }

    private void setRecursive(){
        if ( name.isEmpty() ){
            isTailRecursive = false ;
            isRecursive = false ;
            return;
        }
        List<ASTMethodNode> calls = new ArrayList<>();
        findInnerMethodCalls( block , calls );
        boolean rec = false ;
        boolean tailRec = false ;
        for ( int i = calls.size()-1 ; i >= 0 ; i-- ){
            rec =  name.equals( ( (ZoombaNode)calls.get(i).jjtGetChild(0)).image );
            if ( tailRec ){
                tailRec = false ;
                break;
            }
            tailRec = rec ;
        }
        isRecursive = rec ;
        isTailRecursive = tailRec ;
    }
    */

    public ZScriptMethod(Node node){
        this(node,null);
    }

    private void uniqueParams(Node node){
        int num = node.jjtGetNumChildren();
        HashSet s = new HashSet();
        for ( int i = 0 ; i < num; i++ ){
            ZoombaNode n = ((ASTIdentifier)node.jjtGetChild(i).jjtGetChild(0));
            String name  = n.image;
            if ( s.contains(name) ){
                throw new UnsupportedOperationException(
                        "Non Unique parameters to function! " + n.locationInfo() ) ;
            }
            s.add(name);
        }
        s.clear();
    }

    public ZScriptMethod(Node node, ZContext outer){
        methodNode = node ;
        this.outer = outer ;
        if ( methodNode instanceof ASTMethodDef ){
            Node firstChild = methodNode.jjtGetChild(0);
            if ( firstChild instanceof ASTIdentifier ){
                name = ((ASTIdentifier) firstChild).image;
                isVar = false;
            }else{
                name = "";
                isVar = true ;
            }
            isAnonymous = false ;
        }else{
            isVar = false;
            name = "" ;
            isAnonymous = true ;
            // anonymous
        }
        int num = methodNode.jjtGetNumChildren();
        block = methodNode.jjtGetChild(num-1);
        paramsNode = methodNode.jjtGetChild(num-2);
        uniqueParams(paramsNode);
        //setRecursive();
    }

    public ZScriptMethodInstance instance(ZInterpret interpret, Object instance){
        return new ZScriptMethodInstance(interpret,instance);
    }

    public ZScriptMethodInstance instance(ZInterpret interpret){
        return new ZScriptMethodInstance(interpret);
    }

    @Override
    public String toString() {
        return "" ;
    }

    public final class ZScriptMethodInstance implements Function , Mapper, Predicate , ZEventAware , Iterator {

        @Override
        public void after(ZEventArg a) {
            for ( ZScriptMethod zsm : after ){
                try{
                    ZScriptMethodInstance i = zsm.instance(this.interpret);
                    i.execute(a);
                }catch (Throwable t){

                }
            }
        }

        @Override
        public void before(ZEventArg b) {
            for ( ZScriptMethod zsm : before ){
                try{
                    ZScriptMethodInstance i = zsm.instance(this.interpret);
                    i.execute(b);
                }catch (Throwable t){
                }
            }
        }

        public final ZInterpret interpret;

        public ZScriptMethodInstance(ZInterpret interpret){
            this(interpret,NIL);
        }

        public ZScriptMethodInstance(ZInterpret interpret, Object instance){
            this.interpret = interpret ;
            this.instance = instance ;
        }

        ArgContext getArgs(Object... args){
            // now only positional
            ArgContext ctx = (ArgContext)paramsNode.jjtAccept(interpret,args);
            if ( NIL != instance ){
                ctx.setInstance( instance );
            }
            return ctx;
        }

        @Override
        public String body() {
            return "";
        }

        Object instance = NIL ;

        @Override
        public MonadicContainer execute(Object... args) {
            before( new ZEventArg( this, this, args) );
            ZContext.ArgContext argContext = getArgs(args);
            ZContext parent ;
            if ( outer == null ){
                parent = interpret.frames.peek();
            }else{
                parent = outer ;
            }
            ZInterpret methodInterpreter = interpret ;
            ZContext.FunctionContext functionContext = new ZContext.FunctionContext(parent,argContext);
            // thread mismatch...
            boolean threaded = (interpret.threadId !=  Thread.currentThread().getId()) ;
            if ( threaded ){
                methodInterpreter = new ZInterpret(interpret) ;
            }
            methodInterpreter.prepareCall(functionContext);
            Throwable error = ZException.RETURN ;
            try{
               Object r = block.jjtAccept(methodInterpreter,null);
               return new MonadicContainerBase(r);
            }catch (Throwable t){
               if ( t instanceof ZException.Return){
                   return new MonadicContainerBase(((ZException.Return) t).value );
               }
               if ( t instanceof ZException.MonadicException ){
                   return (ZException.MonadicException)t;
               }
               error = t; // set it up
               throw t; // throw it up
            }finally {
                methodInterpreter.endCall();
                // adjust for this --> error
                after( new ZEventArg( this, this, args , error ) );
                this.instance = NIL ;
            }
        }

        @Override
        public String name() {
            return name;
        }

        private Object iteratorValue;

        @Override
        public boolean hasNext() {
            iteratorValue = NIL ;
            Function.MonadicContainer mc = execute( ZArray.EMPTY_ARRAY );
            if ( mc == ZException.BREAK ){
                return false ;
            }
            iteratorValue = mc.value();
            return true;
        }

        @Override
        public Object next() {
            return iteratorValue;
        }

        public Object next(boolean callHas){
            boolean hasNext = false ;
            if ( callHas ) {
                hasNext = hasNext();
            }
            if ( hasNext ){
                return next();
            }
            return NIL ;
        }

        @Override
        public Object map(Object... args) {
            MonadicContainer container = execute(args);
            if (container.isNil()) {
                return args[0]; // must
            }
            return container.value();
        }

        @Override
        public boolean accept(Object... args) {
            MonadicContainer container = execute(args);
            return ZTypes.bool( container.value(), false );
        }

        @Override
        public String toString() {
            return String.format( "%s -> %s" , name.isEmpty() ?"anon" : name ,
                    ((ZoombaNode)methodNode).locationInfo() );
        }
    }
}
