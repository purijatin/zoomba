package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public interface ZContext {

    Function.MonadicContainer NOTHING = new Function.MonadicContainerBase();

    ZContext parent();

    Function.MonadicContainer get(String name);

    boolean has(String name);

    void set(String name, Object value);

    boolean unSet(String name);

    void clear();

    class EmptyContext implements ZContext{

        @Override
        public ZContext parent() {
            return this;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            return NOTHING ;
        }

        @Override
        public boolean has(String name) {
            return false;
        }

        @Override
        public void set(String name, Object value) { }

        @Override
        public boolean unSet(String name) {
            return false;
        }

        @Override
        public void clear() {

        }
    }

    EmptyContext  EMPTY_CONTEXT = new EmptyContext();

    class ArgContext implements ZContext{

        public static final ArgContext EMPTY_ARGS_CONTEXT = new ArgContext( ZArray.EMPTY_ARRAY  );

        Object[] values;

        Map<String,Integer> lookup;

        Function.MonadicContainer instance = NOTHING ;

        Function.MonadicContainer monadicValues;

        public void setInstance(Object instance){
            this.instance = new Function.MonadicContainerBase(instance);
        }

        public ArgContext(List<String> params, List<Function.MonadicContainer> argValues){
            lookup = new HashMap<>();
            values = new Object[argValues.size()];
            for ( int i = 0 ; i < values.length; i++ ){
                Function.MonadicContainer a = argValues.get(i);
                values[i] = a.value() ;
                lookup.put(params.get(i),i);
            }
            monadicValues = new Function.MonadicContainerBase(values);
        }

        public ArgContext(Object...args){
            lookup = Collections.EMPTY_MAP ;
            values = new Object[args.length];
            for ( int i = 0 ; i < values.length; i++ ){
                values[i] = args[i];
            }
            monadicValues = new Function.MonadicContainerBase(values);
        }

        @Override
        public ZContext parent() {
            return this;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            if ( !has(name) ) return NOTHING ;
            if ( ZScriptMethod.ARGS_VAR.equals(name) ){
                return monadicValues ;
            }
            if ( Function.THIS.equals(name) && !instance.isNil() ){
                return instance ;
            }
            int i = lookup.get(name);
            return new Function.MonadicContainerBase(values[i]);
        }

        @Override
        public boolean has(String name) {
            return  ( !instance.isNil() && Function.THIS.equals(name) ) ||
                    ( ZScriptMethod.ARGS_VAR.equals(name) || lookup.containsKey(name) ) ;
        }

        @Override
        public void set(String name, Object value) {
            if ( Function.THIS.equals(name) || ZScriptMethod.ARGS_VAR.equals(name) ) return;
            int i = lookup.get(name);
            values[i] = value ;
        }

        @Override
        public boolean unSet(String name) {
            return false;
        }

        @Override
        public void clear() {
            lookup.clear();
        }
    }

    class MapContext implements ZContext{

        public final Map<String,Object> map ;

        final ZContext parent;

        public MapContext(ZContext parent){
            map = new HashMap<>();
            this.parent = parent ;
        }

        public MapContext(){
            this(EMPTY_CONTEXT);
        }

        @Override
        public ZContext parent() {
            return parent;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            if ( map.containsKey(name) ){
                return new Function.MonadicContainerBase( map.get(name) );
            }
            if ( parent.has(name ) ){
                return parent.get(name);
            }
            return NOTHING ;
        }

        @Override
        public boolean has(String name) {
            // if I have ....
            if  ( map.containsKey(name) ) return true ;
            // or my parents have
            return parent.has(name);
        }

        @Override
        public void set(String name, Object value) {
            // straight forward, for this guy ...
            map.put(name,value);
        }

        @Override
        public boolean unSet(String name) {
            if ( map.containsKey(name) ){
                map.remove(name);
                return true ;
            }
            return false ;
        }

        @Override
        public void clear() {
            map.clear();
        }
    }

    class FunctionContext extends MapContext{

        public ArgContext argContext ;

        public FunctionContext(ZContext parent, ArgContext argContext){
            super(parent);
            this.argContext = argContext ;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            if ( argContext.has(name) ) return argContext.get(name);
            return super.get(name);
        }

        @Override
        public boolean has(String name) {
            return (argContext.has(name) || super.has(name) ) ;
        }

        @Override
        public void set(String name, Object value) {
            if ( argContext.has(name)){
                argContext.set(name,value);
                return;
            }
            super.set(name, value);
        }
    }
}
