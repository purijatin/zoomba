package zoomba.lang.core.interpreter;

import org.apfloat.Apfloat;
import zoomba.lang.core.collections.*;
import zoomba.lang.core.io.ZDataBase;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.operations.ZRandom;
import zoomba.lang.core.sys.ZProcess;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.*;
import zoomba.lang.parser.ZoombaNode;
import zoomba.lang.core.io.ZFileSystem;

import java.io.File;
import java.lang.reflect.Array;
import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;
import static zoomba.lang.core.operations.Function.NOTHING;

/**
 */
public interface ZMethodInterceptor {

    Function.MonadicContainer intercept(Object context, ZInterpret interpret,
                                        String methodName,
                                        Object[] args ,
                                        List<Function> anons,
                                        ZoombaNode callNode);

    Function.MonadicContainer UNSUCCESSFUL_INTERCEPT = new Function.MonadicContainerBase();

    Default DEFAULT_INTERCEPTOR = new Default();

    final class Default implements ZMethodInterceptor {

        public static final String ASYNC = "async" ;

        public static final String POLL = "poll" ;

        public static final String PRINTF = "printf" ;

        public static final String PRINTLN = "println" ;

        public static final String SIZE = "size" ;

        public static final String EMPTY = "empty" ;

        public static final String LIST = "list" ;

        public static final String SET = "set" ;

        public static final String SORTED_SET = "sset" ;

        public static final String MSET = "mset" ;

        public static final String DICT = "dict" ;

        public static final String SORTED_DICT = "sdict" ;

        public static final String SELECT = "select" ;

        public static final String PARTITION = "partition" ;

        public static final String JOIN = "join" ;

        public static final String FOLD = "fold" ;

        public static final String LFOLD = "lfold" ;

        public static final String RFOLD = "rfold" ;

        public static final String LREDUCE = "reduce" ;

        public static final String RREDUCE = "rreduce" ;

        public static final String FIND = "find" ;

        public static final String LINDEX = "index" ;

        public static final String EXISTS = "exists" ;

        public static final String RINDEX = "rindex" ;

        public static final String INT = "int" ;

        public static final String FLOAT = "float" ;

        public static final String INT_L = "INT" ;

        public static final String FLOAT_L = "FLOAT" ;

        public static final String NUM = "num" ;

        public static final String BOOL = "bool" ;

        public static final String TIME = "time" ;

        public static final String ROUND = "round" ;

        public static final String FLOOR = "floor" ;

        public static final String CEIL = "ceil" ;

        public static final String LOG = "log" ;

        public static final String STRING = "str" ;

        public static final String ENUM = "enum" ;

        public static final String THREAD = "thread" ;

        public static final String JSON = "json" ;

        public static final String XML = "xml" ;

        public static final String OPEN = "open" ;

        public static final String FILE = "file" ;

        public static final String READ = "read" ;

        public static final String WRITE = "write" ;

        public static final String RANDOM = "random" ;

        public static final String SHUFFLE = "shuffle" ;

        public static final String SORTA = "sorta" ;

        public static final String SORTD = "sortd" ;

        public static final String SUM = "sum" ;

        public static final String MINMAX = "minmax" ;

        public static final String SYSTEM = "system" ;

        public static final String POPEN = "popen" ;

        public static final String MATRIX = "matrix" ;

        public static final String BYE = "bye" ;

        public static final String RAISE = "raise" ;

        public static final String LOAD = "load" ;

        public static final String FROM = "from" ;

        public static final String HEAP = "heap" ;

        public static final String HASH = "hash" ;

        public static final String TOKENS = "tokens" ;

        public static final String SEQUENCE = "seq" ;

        public static final String SUB_SEQUENCES = "sequences" ;

        public static final String COMBINATION = "comb" ;

        public static final String PERMUTATION = "perm" ;

        private Default(){}

        public static Object from( List<Function> anons, Object... args){
            Object o;
            if ( args.length > 1 && args[1] instanceof Collection ){
                o = BaseZCollection.compose(args[0], (Collection)args[1], anons);
            }
            else if ( args.length > 0 ) {
                o = BaseZCollection.compose(args[0], null, anons);
            }else{
                o = Collections.EMPTY_LIST ;
            }
            return o;
        }

        public static Object read(Object...args){
            if ( args.length == 0 ) return NIL;
            String path = String.valueOf( args[0] );
            if ( path.contains( ":/") && path.startsWith("http") ){
                // web protocol
                int conTO = Integer.MAX_VALUE ;
                int readTO = Integer.MAX_VALUE ;
                if ( args.length > 1 ){
                    conTO = (int) ZNumber.integer( args[1], conTO );
                    if ( args.length > 2 ){
                        readTO = (int)ZNumber.integer( args[1], readTO );
                    }
                }
                try {
                    return ZWeb.readUrl(path, conTO, readTO);
                }catch (Exception e){
                    throw new RuntimeException(e);
                }
            }
            try {
                return ZFileSystem.read(args);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }

        static Object open(Object...args){
            if ( args.length == 0 ) return NIL;
            String path = String.valueOf( args[0] );
            if ( path.toLowerCase().contains("db:") ){
                String dbLoc = path.replaceAll("[dD][bB]\\:", "");
                return new ZDataBase( dbLoc );
            }
            if ( path.contains( ":/") && path.startsWith("http") ){
                // web protocol
                return new ZWeb( path );
            }
            // file later...
            return ZFileSystem.open(args);
        }

        static Thread thread(Function anon, Object... args){
            if ( args.length == 0 ){
                if ( anon == null ) return Thread.currentThread();
            }
            ZThread zt = null ;
            if ( anon != null ){
                zt = new ZThread( anon, args );
            }else{
                if ( args.length > 0 && args[0] instanceof Function ){
                    Function f = (Function)args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    zt = new ZThread(f,args);
                }
            }
            if ( zt == null ) throw new UnsupportedOperationException("Can not create thread from these args!");
            zt.start();
            return zt;
        }

        static boolean empty(Object o){
            if ( o == null ) return true;
            if ( o instanceof Collection ){ return ((Collection) o).isEmpty() ;}
            if ( o.getClass().isArray() ){
                return ( 0 == Array.getLength(o) );
            }
            if ( o instanceof CharSequence ){
                return ((CharSequence) o).length() == 0 ;
            }
            if ( o instanceof Map) return ((Map) o).isEmpty();
            if ( o instanceof Function.Nil) return true ;
            if ( o instanceof ZFileSystem.ZFile ){
                o = ((ZFileSystem.ZFile) o).file;
            }
            if ( o instanceof File ){
                if ( ((File) o).exists() ){
                    return ((File) o).length() == 0 ;
                } else {
                    return true ;
                }
            }
            return false ;
        }

        static Number size(Object ...args){
            if ( args.length == 0 ) return -1;
            Object o = args[0];
            if ( o == null ) return -1;
            if ( o instanceof Collection ){ return ((Collection) o).size() ;}
            if ( o.getClass().isArray() ){
                return  Array.getLength(o) ;
            }
            if ( o instanceof ZRange ){ return ((ZRange) o).size(); }
            if ( o instanceof CharSequence ){
                return ((CharSequence) o).length()  ;
            }
            if ( o instanceof Map) return ((Map) o).size();
            if ( o instanceof Function.Nil ) return 0 ;
            if ( o instanceof ZFileSystem.ZFile ){
                o = ((ZFileSystem.ZFile) o).file;
            }
            if ( o instanceof File ){
                if ( ((File) o).exists() ){
                    return ((File) o).length() ;
                } else {
                    return -1 ;
                }
            }
            if ( o instanceof Number ) {
                if ( args.length > 1 ){
                    Number n = ZNumber.integer( args[1] );
                    if ( n != null && n.intValue() > 1 ) {
                       return ZTypes.string(o, n.intValue() ).length() ;
                    }
                }
                return cardinality(o);
            }
            // fallback...
            throw new UnsupportedOperationException("Can not Computer Size for Object Type " + o.getClass() );
        }

        static Number cardinality(Object o){
            if ( o == null ) return 0;
            if ( o instanceof Number ){
                ZNumber zn = new ZNumber(o);
                return zn._abs_() ;
            }
            return size(o);
        }

        static void println(Object... f){
            if ( f == null ) { System.out.println("null"); return;  }
            if ( f.length == 0 ) { System.out.println(); return; }
            Object a = f[0];
            if ( a == null ) { System.out.println("null"); return;  }
            if ( a.getClass().isArray() ){
                a = new ZArray(a,false);
            }
            if ( a instanceof Apfloat){
                a = ((Apfloat) a).toString(true);
            }
            System.out.println(a);
        }

        static void printf(Object...args){
            if ( args.length == 0 ) return ;
            String format = String.valueOf( args[0] );
            args = ZTypes.shiftArgsLeft(args);
            System.out.printf(format,args);
        }

        static Collection join( List<Function> anons, Object... args){
            if ( args.length == 0 ) return new ZList() ;
            if ( args[0] == null || args.length < 2 ) return Collections.EMPTY_LIST ;
            ZCollection initial ;
            if ( args[0] instanceof ZRange ){
                args[0] = ((ZRange)args[0]).yield().asList();
            }

            if ( args[0].getClass().isArray() ){
                initial = new ZArray(args[0], false );
            }
            else if ( args[0] instanceof ZCollection ){
                initial = (ZCollection)args[0];
            }else if ( args[0] instanceof Iterable ){
                initial = new ZList((Iterable) args[0]);
            }else{
                throw new UnsupportedOperationException("Can not join non Collection!");
            }
            Collection[] cols = new Collection[ args.length -1 ];
            for ( int i = 0 ; i < cols.length ; i++ ){
                int j = i + 1 ;
                if ( args[j] instanceof ZRange ){
                    args[j] = ((ZRange)args[j]).yield().asList();
                }

                if ( args[j].getClass().isArray() ){
                    cols[i] = new ZArray(args[j], false );
                }
                else if ( args[j] instanceof ZCollection ){
                    cols[i] = (ZCollection)args[j];
                }else if ( args[i] instanceof Iterable ){
                    cols[i] = new ZList( (Iterable)args[j] );
                }else{
                    throw new UnsupportedOperationException("Can not join non Collection!");
                }
            }
            if ( anons.isEmpty() ) return initial.join(cols);
            if ( anons.size() == 1 ){
                return initial.join(anons.get(0),cols);
            }
            return initial.join(anons.get(0), anons.get(1), cols);
        }

        static List list( Function anon, Object... args){
            if ( args.length == 0 ) return new ZList() ;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit list from args, no flattening
            }
            if ( anon == null ){
                if ( a instanceof ZRange) return ((ZRange) a).list();
                if ( a instanceof Collection ){
                    return new ZList((Collection)a);
                }
                if ( a == null ) return Collections.EMPTY_LIST;
                return new ZList(a);
            }
            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList(); // intercept and convert
            }
            if ( a instanceof Iterable ){
                return (List) BaseZCollection.map(new ZList(), anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return (List)BaseZCollection.map(new ZList(), anon, ((Map) a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return (List)BaseZCollection.map(new ZList(), anon, new ZArray(a,false) );
            }
            throw new UnsupportedOperationException("Type can not be converted to a list!");
        }

        static Iterable file(Function anon, Object...args){
            if ( args.length == 0 ) return Collections.EMPTY_LIST ;
            Iterable iterable = ZFileSystem.file(String.valueOf(args[0]));
            if ( anon instanceof Function.Predicate){
                return BaseZCollection.select( new ZList(), anon, iterable );
            }
            if ( anon instanceof Function.Mapper){
                return BaseZCollection.map( new ZList(), anon, iterable );
            }
            return iterable ;
        }

        static Set set( Function anon, Object... args){
            if ( args.length == 0 ) return new ZSet() ;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit set from args, no flattening
            }
            if ( anon == null ){
                if ( a instanceof ZRange ) { a = ((ZRange)a).asList(); }
                if ( a instanceof Collection ){
                    return new ZSet((Collection)a);
                }
                if ( a == null ) return Collections.EMPTY_SET ;
                if ( a.getClass().isArray() ){
                    return new ZSet( new ZArray(a,false) );
                }
                return new ZSet(a);
            }
            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.set(new ZSet(), anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return BaseZCollection.set(new ZSet(), anon, ((Map) a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.set(new ZSet(), anon, new ZArray(a,false) );
            }
            throw new UnsupportedOperationException("Type can not be converted to a set!");
        }

        static Set sset( Function anon, Object... args){
            if ( args.length == 0 ) return new ZSet( new ZSortedSet() ) ;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit set from args, no flattening
            }
            if ( anon == null ){
                if ( a instanceof ZRange ) { a = ((ZRange)a).asList(); }
                if ( a instanceof Collection ){
                    return new ZSet( new ZSortedSet((Collection)a));
                }
                if ( a == null ) return Collections.EMPTY_SET ;
                if ( a.getClass().isArray() ){
                    return new ZSet( new ZSortedSet( new ZArray(a,false) ) );
                }
                return new ZSet( new ZSortedSet( ) );
            }
            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.set(new ZSet(new ZSortedSet()), anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return BaseZCollection.set(new ZSet(new ZSortedSet()), anon, ((Map) a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.set(new ZSet(new ZSortedSet()), anon, new ZArray(a,false) );
            }
            throw new UnsupportedOperationException("Type can not be converted to a Sorted Set!");
        }


        static Map multiSet( Function anon,Object[] args){
            if ( args.length == 0 ) return new ZMap() ;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit set from args, no flattening
            }
            if ( anon == null ){
                if ( a instanceof Iterable ){
                    return BaseZCollection.multiSet((Iterable) a);
                }
                if ( a.getClass().isArray() ){
                    return BaseZCollection.multiSet( new ZArray(a,false) );
                }
            }

            if ( a instanceof Iterable ){
                return BaseZCollection.multiSet(anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return BaseZCollection.multiSet(anon, ((Map)a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.multiSet(anon, new ZArray(a,false));
            }
            throw new UnsupportedOperationException("Type can not be converted to a multi set!");
        }

        static Collection select( Object[] args, Function anon, List<Function> anons){
            if (  anons.size() > 1 ){
                 return (Collection)from( anons, args );
            }
            if ( args.length == 0 ) return Collections.EMPTY_LIST ;
            Object a = args[0];
            if ( a == null || anon == null ) return Collections.EMPTY_LIST ;
            Collection collector ;
            boolean collectorSpecified = false ;
            if ( args.length > 1 && args[1] instanceof Collection ){
                collector = (Collection)args[1];
                collectorSpecified = true ;
            }
            else{
                collector = new ZList();
            }

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterable ){
                if ( a instanceof Set ){
                    if ( !collectorSpecified ){
                        collector = new ZSet();
                    }
                    return  BaseZCollection.select(collector, anon, (Set)a);
                }
                return  BaseZCollection.select(collector, anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return BaseZCollection.select(collector, anon, ((Map) a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.select(collector, anon, new ZArray(a,false) );
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        public static Collection[] EMPTY_PARTITIONS = new Collection[]{
                Collections.EMPTY_LIST , Collections.EMPTY_LIST  };

        static Collection[] partition(Function anon, Object[] args){
            if ( args.length == 0 ) return EMPTY_PARTITIONS;
            Object a = args[0];
            if ( args.length > 1 ){
                a = args ; // implicit set from args, no flattening
            }
            if ( a == null || anon == null ) return  EMPTY_PARTITIONS ;

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterable ){
                if ( a instanceof Set ){
                    return  BaseZCollection.partition(new Collection[] { new ZSet(), new ZSet() },
                            anon, (Iterable)a);
                }
                return  BaseZCollection.partition(new Collection[] { new ZList(),new ZList() },
                        anon, (Iterable)a);
            }
            if ( a instanceof Map ){
                return BaseZCollection.partition(new Collection[] { new ZList(),new ZList() },
                        anon, ((Map) a).entrySet() );
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.partition(new Collection[] { new ZList(),new ZList() },
                        anon, new ZArray(a,false) );
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object leftReduce( Object a, Function anon){
            if ( a == null || anon == null ) return NIL;

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterator ){
                a = BaseZCollection.fromIterator((Iterator)a);
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.leftReduce((Iterable)a,anon);
            }
            if ( a instanceof Map ){
                return BaseZCollection.leftReduce(((Map)a).entrySet(),anon);
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.leftReduce(new ZArray(a,false), anon );
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object leftFold( Object a, Function anon, Object...args){
            if ( a == null || anon == null ) return NIL;

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof Iterator ){
                a = BaseZCollection.fromIterator((Iterator)a);
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.leftFold((Iterable)a,anon,args);
            }
            if ( a instanceof Map ){
                return BaseZCollection.leftFold(((Map)a).entrySet(),anon,args);
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.leftFold(new ZArray(a,false), anon,args);
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object rightFold( Object a, Function anon, Object...args){
            if ( a == null || anon == null ) return NIL;

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof List ){
                return BaseZCollection.rightFold((List) a,anon,args);
            }

            if ( a.getClass().isArray() ){
                return BaseZCollection.rightFold(new ZArray(a,false), anon,args);
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        static Object rightReduce( Object a, Function anon){
            if ( a == null || anon == null ) return NIL;

            if ( a instanceof ZRange ){
                a = ((ZRange)a).asList();
            }
            if ( a instanceof List ){
                return BaseZCollection.rightReduce((List) a,anon);
            }

            if ( a.getClass().isArray() ){
                return BaseZCollection.rightReduce(new ZArray(a,false), anon );
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        static Function.MonadicContainer find(Object a, Function anon) {
            if ( a == null || anon == null )  return  NOTHING  ;
            if ( a instanceof ZRange ){
                a = ((ZRange) a).asList();
            }
            if ( a instanceof Iterator ){
                a = BaseZCollection.fromIterator((Iterator)a);
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.find((Iterable)a,anon);
            }
            if ( a instanceof Map ){
                return BaseZCollection.find(((Map)a).entrySet(),anon);
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.find(new ZArray(a,false), anon );
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static int leftIndex( Object a, Function anon, Object... args ) {
            if ( a == null )  return -1 ;
            if ( anon == null ){
                if ( a instanceof  Iterable ){
                    return BaseZCollection.leftIndex((Iterable)a, args[0] );
                }
                if ( a.getClass().isArray() ){
                    return BaseZCollection.leftIndex(new ZArray(a,false), args[0] );
                }
            }
            if ( a instanceof ZRange ){
                a = ((ZRange) a).asList();
            }
            if ( a instanceof Iterator ){
                a = BaseZCollection.fromIterator((Iterator)a);
            }
            if ( a instanceof Iterable ){
                return BaseZCollection.leftIndex((Iterable)a,anon);
            }
            if ( a instanceof Map ){
                return BaseZCollection.leftIndex(((Map)a).entrySet(),anon);
            }
            if ( a.getClass().isArray() ){
                return BaseZCollection.leftIndex(new ZArray(a,false), anon );
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static int rightIndex( Object a, Function anon, Object... args ) {
            if ( a == null )  return -1 ;
            if ( anon == null ){
                if ( a instanceof  List ){
                    return BaseZCollection.rightIndex((List)a, args[0] );
                }
                if ( a.getClass().isArray() ){
                    return BaseZCollection.rightIndex(new ZArray(a,false), args[0] );
                }
            }
            if ( a instanceof ZRange ){
                a = ((ZRange) a).asList();
            }
            if ( a instanceof List ){
                return BaseZCollection.rightIndex((List)a,anon);
            }

            if ( a.getClass().isArray() ){
                return BaseZCollection.rightIndex(new ZArray(a,false), anon );
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        @Override
        public Function.MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                                   Object[] args, List<Function> anons , ZoombaNode callNode) {
            Function anon = anons.isEmpty()? null : anons.get(0);
            switch ( methodName ){
                case EMPTY :
                    return new Function.MonadicContainerBase( empty(args[0]) ) ;
                case SIZE :
                    return new Function.MonadicContainerBase( size(args) );
                case PRINTF  :
                    printf(args);
                    return Function.Void ;
                case PRINTLN  :
                    println(args);
                    return Function.Void ;
                case LIST :
                    return new Function.MonadicContainerBase( list(anon,args ) );
                case SET:
                    return new Function.MonadicContainerBase( set(anon,args ) );
                case SORTED_SET:
                    return new Function.MonadicContainerBase( sset(anon,args ) );
                case DICT :
                    return new Function.MonadicContainerBase(ZMap.dict(anon,args));
                case SORTED_DICT :
                    return new Function.MonadicContainerBase(ZMap.sdict(anon,args));
                case SELECT :
                    return new Function.MonadicContainerBase( select(args, anon, anons ) );
                case FOLD: // it is boring to do lfold all the time, default should be fold <-> lfold
                case LFOLD :
                    Object o = args[0];
                    Object[] others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase( leftFold(o,anon,others));
                case BOOL :
                    return new Function.MonadicContainerBase(ZTypes.bool( args ) );
                case INT :
                    return new Function.MonadicContainerBase(ZNumber.integer( args ) );
                case FLOAT :
                    return new Function.MonadicContainerBase(ZNumber.floating( args ) );
                case INT_L :
                    return new Function.MonadicContainerBase(ZNumber.INT( args ) );
                case FLOAT_L :
                    return new Function.MonadicContainerBase(ZNumber.FLOAT( args ) );
                case NUM :
                    return new Function.MonadicContainerBase(ZNumber.number( args ) );
                case TIME:
                    return new Function.MonadicContainerBase( ZDate.time(args) );
                case FIND:
                    if ( args.length == 0 ) return Function.FAILURE ;
                    return new Function.MonadicContainerBase( find( args[0], anon ) ) ;
                case EXISTS:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    int i = leftIndex(o,anon,others);
                    if(i >= 0 ) return Function.SUCCESS ;
                    return Function.FAILURE ;
                case LINDEX:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = leftIndex(o,anon,others);
                    return new Function.MonadicContainerBase(i);

                case LREDUCE:
                    if ( args.length == 0 ) return Function.NOTHING ;
                    o = leftReduce( args[0],anon );
                    return new Function.MonadicContainerBase(o);

                case RREDUCE:
                    if ( args.length == 0 ) return Function.NOTHING ;
                    o = rightReduce( args[0],anon );
                    return new Function.MonadicContainerBase(o);

                case PARTITION :
                    return new Function.MonadicContainerBase( partition( anon, args ) );
                case MSET :
                    return new Function.MonadicContainerBase( multiSet( anon, args ) );
                case STRING :
                    if ( anon != null && args.length > 1 ){
                        return new Function.MonadicContainerBase(
                                ZTypes.string(anon, args[0], String.valueOf(args[1] ) ));
                    }
                    return new Function.MonadicContainerBase( ZTypes.string( args ) ) ;
                case JSON :
                    return new Function.MonadicContainerBase( ZTypes.json(args) );
                case OPEN :
                    return new Function.MonadicContainerBase(open(args));
                case RFOLD :
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase( rightFold(o,anon,others));
                case RINDEX :
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = rightIndex(o,anon,others);
                    return new Function.MonadicContainerBase(i);
                case JOIN:
                    return new Function.MonadicContainerBase( join(anons,args) ) ;
                case FROM:
                    return new Function.MonadicContainerBase( from(anons,args) );
                case ROUND:
                    Number num = (Number)args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(  ZNumber.round(num, args ) );
                case FLOOR:
                    return new Function.MonadicContainerBase( ZNumber.floor((Number)args[0]) ) ;
                case CEIL :
                    return new Function.MonadicContainerBase( ZNumber.ceil((Number)args[0]) ) ;
                case LOG :
                    return new Function.MonadicContainerBase( ZNumber.log(args) ) ;
                case THREAD :
                    return new Function.MonadicContainerBase( thread( anon, args ) ) ;
                case FILE :
                    return new Function.MonadicContainerBase( file( anon,args ) );
                case READ :
                    return new Function.MonadicContainerBase( read(args) );
                case WRITE :
                    return new Function.MonadicContainerBase( ZFileSystem.write( args ) );
                case XML :
                    return new Function.MonadicContainerBase( ZXml.xml( args ) );
                case RANDOM:
                    return new Function.MonadicContainerBase(ZRandom.random( args ));
                case SHUFFLE:
                    return new Function.MonadicContainerBase(ZRandom.shuffle( args[0] ) );
                case SORTA :
                    ZRandom.sortAscending(args, anon);
                    return Function.SUCCESS ;
                case SORTD :
                    ZRandom.sortDescending(args, anon);
                    return Function.SUCCESS ;
                case SUM :
                    return new Function.MonadicContainerBase( ZRandom.sum(anon, args) );
                case MINMAX:
                    if ( args.length == 0 ) return Function.FAILURE ;
                    return new Function.MonadicContainerBase( ZRandom.minMax(anons,args) );
                case SYSTEM:
                    return new Function.MonadicContainerBase(ZProcess.system(args));
                case POPEN:
                    return new Function.MonadicContainerBase(ZProcess.popen(args));
                case MATRIX:
                    return new Function.MonadicContainerBase(ZMatrix.Loader.load( args) );
                case ENUM:
                    return new Function.MonadicContainerBase(ZTypes.enumCast( args) );
                case BYE:
                    ZTypes.bye(args);
                    // unreachable code for sure
                    return Function.SUCCESS ;
                case RAISE:
                    ZTypes.raise(args);
                    // unreachable
                    break;
                case HEAP:
                    return new Function.MonadicContainerBase( ZHeap.heap(anon,args));
                case LOAD:
                    return ZTypes.loadJar(args);
                case HASH:
                    return new Function.MonadicContainerBase( ZRandom.hash(args) );
                case TOKENS:
                    return new Function.MonadicContainerBase( ZRandom.tokens(anons, args) );
                case SEQUENCE:
                    return new Function.MonadicContainerBase( ZSequence.BaseSequence.sequence(anon, args) );
                case POLL:
                    return new Function.MonadicContainerBase( ZThread.poll(anon, args) );
                case ASYNC:
                    if ( args.length != 0 && args[0] instanceof Function ) {
                        return new Function.MonadicContainerBase(ZThread.async(anon, (Function)args[0]) );
                    }
                    return Function.FAILURE ;
                case PERMUTATION:
                    return new Function.MonadicContainerBase( Combinatorics.permutations( args ) );
                case COMBINATION:
                    return new Function.MonadicContainerBase( Combinatorics.combinations( args ) );
                case SUB_SEQUENCES:
                    return new Function.MonadicContainerBase( new Combinatorics.Sequences( args ) );

            }
            return UNSUCCESSFUL_INTERCEPT  ;
        }
    }
}
