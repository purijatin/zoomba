package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ZoombaNode;

import java.util.EventObject;
import java.util.Set;

/**
 */
public final class ZAssertion {

    /**
     * Types of assertion
     */
    public enum AssertionType {
        /**
         * Test assertion : when true it passes, when false it fails
         */
        TEST,

        /**
         * Assertion : when true it passes, when false it fails,the current test script will be aborted
         */
        ASSERT,

        /**
         * Assertion : when true, the current test script will be aborted , false means assertion passed
         */
        PANIC;

        public boolean failed(boolean value){
            if ( this == TEST || this == ASSERT ) return !value ;
            return value ;
        }
    }


    /**
     * An Assertion Event object
     */
    public static class AssertionEvent extends EventObject {

        /**
         * Type of assertion
         */
        public  final  AssertionType type;

        /**
         * In case the caller passed any data
         */
        public final Object[] data;

        /**
         * The final value of this assertion
         */
        public final boolean value;

        /**
         * <pre>
         * What caused this assertion?
         * Either it is an expression evaluated, in which case it will be @{AssertionAssignment}
         * Else it is created by failure to evaluate block of code ( anonymous function )
         * where the resultant error would be stored.
         * </pre>
         */
        public final Throwable cause;

        /**
         * Creates an object
         * @param source source of the event
         * @param type what type the assertion is
         * @param value for test true passes, for abort false passes
         * @param cause cause of the assertion
         * @param data any data people wants to pass
         */
        public AssertionEvent(Object source, AssertionType type, boolean value, Throwable cause, Object[] data) {
            super(source);
            this.type = type;
            this.cause = cause ;
            this.data = data ;
            this.value = value ;
        }

        @Override
        public String toString(){
            boolean failed = type.failed(value);
            String ret = String.format("%s %s => { %s }", type, value,
                    ZTypes.string(data));
            if ( ZException.ZRuntimeAssertion.NOT_AN_EXCEPTION != cause ){
                ret += String.format( " | caused by : %s" , cause );
            }
            if ( failed ){
                return "!!!" + ret ;
            }
            return ret;
        }
    }

    /**
     * Anyone who wants to listen to assertions
     */
    public interface AssertionEventListener{

        void onAssertion(AssertionEvent assertionEvent);

    }

    /**
     * The event listeners
     */
    public final Set<AssertionEventListener> eventListeners;

    public ZAssertion(){
        eventListeners = new ZSet();
    }

    /**
     * Fires a abort assertion -
     * @param type Type of assertion
     * @param node the location where assertion was fired
     * @param anon the function which has to be evaluated
     * @param args any args one may pass
     */
    public void testAssert(AssertionType type, ZoombaNode node, Function anon, Object...args)  {
        Throwable cause = ZException.ZRuntimeAssertion.NOT_AN_EXCEPTION;
        boolean value  ;
        switch ( type ){
            case TEST:
            case ASSERT:
                value = false;
                break;
            default:
                value = true ;

        }

        if ( anon != null ){
            try {
                Function.MonadicContainer mc = anon.execute();
                value = ZTypes.bool(mc.value(), value);
            }catch (Throwable t){
                cause = t.getCause();
                if ( cause == null ){
                    cause = t;
                }
            }
        }else{
            if ( args.length > 0 ){
                Object v = args[0];
                args = ZTypes.shiftArgsLeft(args);
                if ( v instanceof Function ){
                    testAssert( type,node,(Function)v,args);
                    return;
                }
                value = ZTypes.bool(v, value);
            }
        }


        for ( AssertionEventListener listener : eventListeners ){
            AssertionEvent event = new AssertionEvent(this, type , value, cause, args);
            try {
                listener.onAssertion(event);
            }catch (Throwable t){
                System.err.printf("Error *%s* asserting to listener : %s [%s]\n",type, listener,t);
            }
        }

        switch (type){
            case PANIC:
                if ( value ) throw new ZException.Panic(node,args);
                break;
            case ASSERT:
                if ( !value ) throw new ZException.Assertion(node,args);
                break;
            case TEST:
                if ( !value ){
                    System.err.printf("Test Assertion Failed @ %s : { %s }\n",
                            node.locationInfo(), ZTypes.string(args));
                }
                break;
        }
    }
}
