package zoomba.lang.core.collections;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;
import java.lang.reflect.Array;
import java.util.*;

/**
 */
public class ZMap implements Map , Arithmetic.BasicArithmeticAware, Arithmetic.LogicAware, ZTypes.StringX {

    public static final ZMap EMPTY = new ZMap( Collections.EMPTY_MAP );

    public static class Pair extends ZArray implements Entry {

        public Pair(Object k, Object v){
            super( new Object[ ]{ k,v } );
        }

        public Pair(Entry e){
            this(e.getKey(), e.getValue());
        }

        public Pair(Object p){
            super(p);
        }

        @Override
        public Object getKey() {
            return get(0);
        }

        @Override
        public Object getValue() {
            return get(1);
        }

        @Override
        public Object setValue(Object value) {
            return set(1,value);
        }
    }

    public static Map fromEntries(Collection c){
        Iterator iterator = c.iterator();
        Map m = new HashMap<>();
        while ( iterator.hasNext() ){
            Object o = iterator.next();
            if ( o == null )
                throw new UnsupportedOperationException("Map entry can not be null!");
            if ( o instanceof Map.Entry ){
                m.put( ((Map.Entry) o).getKey(), ((Map.Entry) o).getValue() );
                continue;
            }
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof List ){
                List l = (List)o;
                if ( l.size() < 2 ) throw new UnsupportedOperationException("Map entry can not be less than 2!");
                m.put(l.get(0) ,l.get(1));
            }
        }
        return m;
    }

    public static Map joinEntries(Collection c1, Collection c2){
        Iterator iterator1 = c1.iterator();
        Iterator iterator2 = c2.iterator();

        Map m = new HashMap<>();
        while ( iterator1.hasNext() ){
            Object k = iterator1.next();
            if ( !iterator2.hasNext() ) {
                m.clear();
                throw new UnsupportedOperationException("Key,Value collection size mismatch!");
            }
            Object v = iterator2.next();
            m.put(k,v);
        }
        if ( iterator2.hasNext() ){
            m.clear();
            throw new UnsupportedOperationException("Key,Value collection size mismatch!");
        }
        return m;
    }

    public static Map dict(Function f, Object...args){
        if (args.length == 0 ) return new ZMap();
        if ( args[0] instanceof Map && f == null ){
            if ( args.length == 2 &&  ZTypes.bool(args[1],false)  ){
                return new ZMap( new HashMap( (Map)args[0]) );
            }
            if ( args[0] instanceof ZMap ){
                return new ZMap( ((ZMap)args[0]).map );
            }
            return new ZMap( (Map)args[0] );
        }
        if ( f != null ){
            Object o = args[0];
            if ( o == null ) return EMPTY ;
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof ZRange ){
                o = ((ZRange) o).asList();
            }
            else if ( o instanceof Map ){
                o = ((Map)o).entrySet();
            }
            if ( !(o instanceof Iterable) ){
                throw new UnsupportedOperationException("Can not covert to Map!");
            }
            return BaseZCollection.dict(new ZMap(), f, (Iterable)o );

        }
        if ( args.length == 1 ){
            if ( args[0].getClass().isArray() ){
                return new ZMap(args[0]) ;
            }
            if ( args[0] instanceof Collection ){
                return new ZMap((Collection)args[0]);
            }
            return ZJVMAccess.dict(args[0]);
        }
        return new ZMap(args[0],args[1]);
    }

    public static Map sdict(Function f, Object...args){
        if (args.length == 0 ) return new ZMap( new ZSortedMap() );
        if ( args[0] instanceof Map && f == null ){
            if ( args[0] instanceof ZMap ){
                return new ZMap( new ZSortedMap( ((ZMap)args[0]).map ) );
            }
            return new ZMap( new ZSortedMap( (Map)args[0] ) );
        }
        if ( f != null ){
            Object o = args[0];
            if ( o == null ) return EMPTY ;
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof ZRange ){
                o = ((ZRange) o).asList();
            }
            else if ( o instanceof Map ){
                o = ((Map)o).entrySet();
            }
            if ( !(o instanceof Iterable) ){
                throw new UnsupportedOperationException("Can not covert to Sorted Map!");
            }
            return BaseZCollection.dict(new ZMap( new ZSortedMap() ), f, (Iterable)o );
        }
        if ( args.length == 1 ){

            if ( args[0] instanceof Collection ){
                return new ZMap( new ZSortedMap( fromEntries( (Collection)args[0]) ) );
            }
        }
        throw new UnsupportedOperationException("Can not covert to Sorted Map!");
    }

    protected Map map;

    public static ZCollection.Relation relate(Map m1, Map m2){
        Set keySet1 = m1.keySet();
        Set keySet2 = m2.keySet();
        ZCollection.Relation relation = BaseZCollection.relate(keySet1,keySet2);
        if ( ZCollection.Relation.INDEPENDENT  == relation ) { return  relation ; }
        // do they really?
        Set intersection = BaseZCollection.intersection( keySet1,keySet2 );
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ) return ZCollection.Relation.INDEPENDENT ;
        }
        return relation;
    }

    public static ZCollection.Relation relate(Map m, Collection<Entry> c){
        ZCollection zc = new ZSet( m.entrySet() );
        return zc.relate(c);
    }

    public static ZMap intersection( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( ZTypes.equals(v1,v2) ){
                map.map.put(k,v1);
            }
        }
        return map;
    }

    public static Map intersectionMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        Set ks = new HashSet<>( m1.keySet() );
        for ( Object k : ks ) {
            if ( !intersection.contains(k) ){
                m1.remove(k);
            }
        }

        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( ZTypes.equals(v1,v2) ){
                m1.put(k,v1);
            }
        }
        return m1;
    }

    public static ZMap union( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, new Pair( v1, v2 ) );
            }else{
                map.map.put(k,v1);
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        return map;
    }

    public static Map unionMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                m1.put(k, new Pair( v1, v2 ) );
            }else{
                m1.put(k, v1 );
            }
        }

        for ( Object k : m2.keySet() ){
            if ( !m1.containsKey(k) ){
                m1.put(k, m2.get(k));
            }
        }
        return m1;
    }

    public static ZMap difference( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, v1 );
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }

        return map;
    }

    public static Map differenceMutable( Map m1, Map m2){
        for ( Object k : m2.keySet() ){
            if ( m1.containsKey(k)){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( ZTypes.equals(v1,v2)){
                    m1.remove(k);
                }
            }
        }
        return m1;
    }

    public static ZMap symmetricDifference( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        return map;
    }

    public static Map symmetricDifferenceMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        for ( Object k : m1.keySet() ){
            if ( intersection.contains(k)){
                m1.remove(k);
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k)){
                m1.put(k,m2.get(k));
            }
        }
        return m1;
    }

    public ZMap(Object arr){
        this( fromEntries( new ZArray(arr,false)));
    }

    public ZMap(Map m) {
        if ( m instanceof ZMap ){
            map = new HashMap<>(((ZMap) m).map);
        }else {
            map = m;
        }
    }

    public ZMap(Collection c) {
        map = fromEntries(c) ;
    }

    public ZMap(Object keys, Object values){
        if ( keys == null ) throw new UnsupportedOperationException("null as keys!");
        if ( values == null ) throw new UnsupportedOperationException("null as keys!");
        if ( keys.getClass().isArray() ){
            keys = new ZArray(keys,false);
        }
        if ( values.getClass().isArray() ){
            values = new ZArray(values,false);
        }
        if ( keys instanceof Collection && values instanceof Collection ) {
            map = joinEntries((Collection)keys,(Collection)values);
        }
        else{
            throw new UnsupportedOperationException("Unsupported types as keys or values!");
        }
    }

    public ZMap() {
        this(new HashMap());
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return map.get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        return map.put(key,value);
    }

    @Override
    public void putAll(Map m) {
        map.putAll(m);
    }

    @Override
    public Set keySet() {
        return new ZSet(map.keySet());
    }

    @Override
    public Collection values() {
        return new ZList(map.values());
    }

    @Override
    public Set entrySet() {
        return new ZSet(map.entrySet());
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1;
        if ( o == this ) return 0 ;
        if ( o instanceof Map ) {
            ZCollection.Relation r ;
            if ( o instanceof ZMap ){
                r = relate(this.map, ((ZMap) o).map);
            }else{
                r = relate(this.map, (Map) o);
            }
            if ( r == ZCollection.Relation.SUB ) return -1;
            if ( r == ZCollection.Relation.EQUAL ) return 0;
            if ( r == ZCollection.Relation.SUPER ) return 1;
            throw new UnsupportedOperationException("Can not compare unrelated Maps!");
        }
        if ( o instanceof Collection ) {
            ZCollection.Relation r = relate(this.map, (Collection<Entry>) o);
            if ( r == ZCollection.Relation.SUB ) return -1;
            if ( r == ZCollection.Relation.EQUAL ) return 0;
            if ( r == ZCollection.Relation.SUPER ) return 1;
            throw new UnsupportedOperationException("Can not compare unrelated Maps!");
        }
        throw new UnsupportedOperationException("Can not compare Map against another object!");
    }

    @Override
    public Object _add_(Object o) {
        if ( o instanceof Map ){
            return union(this.map,(Map)o);
        }
        add_mutable(o);
        return this;
    }

    @Override
    public Object _sub_(Object o) {
        if ( o instanceof Map ){
            return difference(this.map,(Map)o);
        }
        sub_mutable(o);
        return this;
    }

    @Override
    public Object _mul_(Object o) {
        throw new UnsupportedOperationException("Can not multiply Map!");
    }

    @Override
    public Object _div_(Object o) {
        Set s = new ZSet();
        for ( Object k : map.keySet() ){
            Object v = map.get(k);
            if ( ZTypes.equals(v,o) ){
                s.add(k);
            }
        }
        return s;
    }

    @Override
    public Object _pow_(Object o) {
        throw new UnsupportedOperationException("No support for exponentiation in Map!");
    }

    @Override
    public void add_mutable(Object o) {
        Pair p = new Pair(o);
        map.put(p.get(0),p.get(1));
    }

    @Override
    public void sub_mutable(Object o) {
        if ( o instanceof Map ){
            map = differenceMutable(map,(Map)o);
            return;
        }
        if ( map.containsKey( o ) ){
            map.remove(o);
        }
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply Map!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Division of a Map is immutable!");
    }

    @Override
    public Object _and_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        return intersection(this.map, (Map)o);
    }

    @Override
    public Object _or_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        return union(this.map, (Map)o);
    }

    @Override
    public Object _xor_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        return symmetricDifference(this.map, (Map)o);
    }

    @Override
    public void and_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        map = intersectionMutable(map,(Map)o);
    }

    @Override
    public void or_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        map = unionMutable(map,(Map)o);
    }

    @Override
    public void xor_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        map = symmetricDifferenceMutable(map,(Map)o);
    }

    @Override
    public String toString() {
        return map.toString();
    }

    @Override
    public String string(Object... args) {
        if  ( args.length == 0 ) return map.toString();
        boolean json = ZTypes.bool(args[0],false);
        if ( json ){
            return ZTypes.jsonString(this.map);
        }
        return map.toString();
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null ) return false;
        if ( obj == this ) return true;
        // check the class? Nonsense...
        return compareTo(obj) == 0 ;
    }
}
