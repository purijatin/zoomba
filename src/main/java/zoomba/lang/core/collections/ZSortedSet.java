package zoomba.lang.core.collections;
import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * Preserves the order of insertion
 */
public class ZSortedSet implements SortedSet {

    static class EmptySortedSet extends ZSortedSet{
        @Override
        public boolean add(Object o) {
            return false ;
        }
    }

    public static final EmptySortedSet EMPTY_SORTED_SET = new EmptySortedSet();

    final Map<Object,Integer> map;

    final List<Object> list;

    public ZSortedSet(Collection col){
        map = new HashMap<>();
        list = new ArrayList<>();
        int i = 0 ;
        for ( Object o : col ){
            if ( !map.containsKey( o ) ){
                list.add(o);
                map.put(o,i++);
            }
        }
    }

    public ZSortedSet(){
        this(Collections.EMPTY_LIST );
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey( o );
    }

    @Override
    public Iterator iterator() {
        return list.listIterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public Object[] toArray(Object[] a) {
        return list.toArray( a );
    }

    @Override
    public boolean add(Object o) {
        if ( map.containsKey(o) ) return false ;
        list.add(o);
        map.put(o, list.size()-1);
        return true ;
    }

    private void reInitMap(){
        map.clear();
        for ( int i = 0 ; i < list.size(); i++ ){
            map.put( list.get(i), i);
        }
    }

    @Override
    public boolean remove(Object o) {
        if ( !map.containsKey(o) ) return false ;
        int inx = map.get(o);
        list.remove( inx );
        map.remove(o);
        reInitMap();
        return true ;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean  contains = true ;
        for ( Object o : c ){
            contains &= map.containsKey(o);
        }
        return contains ;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean  added = true ;
        for ( Object o : c ){
            added &= add(o);
        }
        return added ;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean changed = false ;
        for ( Object o : c ){
            if ( ! map.containsKey( o ) ){
                changed |= remove(o);
            }
        }
        return changed ;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean  removed = true ;
        for ( Object o : c ){
            removed &= remove(o);
        }
        return removed ;
    }

    @Override
    public void clear() {
        map.clear();
        list.clear();
    }

    @Override
    public Comparator comparator() {
        return Comparator.naturalOrder() ;
    }

    @Override
    public SortedSet subSet(Object fromElement, Object toElement) {
        if ( !map.containsKey( fromElement) ) return EMPTY_SORTED_SET ;
        int from = map.get(fromElement);
        if ( !map.containsKey( toElement) ) return EMPTY_SORTED_SET ;
        int to = map.get(toElement);
        SortedSet s = new ZSortedSet();
        for ( int i = from ; i <= to ; i++ ){
            s.add( list.get(i) );
        }
        return s;
    }

    @Override
    public SortedSet headSet(Object toElement) {
        if ( !map.containsKey( toElement) ) return EMPTY_SORTED_SET ;
        int to = map.get(toElement);
        SortedSet s = new ZSortedSet();
        for ( int i = 0 ; i <= to ; i++ ){
            s.add( list.get(i) );
        }
        return s;
    }

    @Override
    public SortedSet tailSet(Object fromElement) {
        if ( !map.containsKey( fromElement) ) return EMPTY_SORTED_SET ;
        int from = map.get(fromElement);
        SortedSet s = new ZSortedSet();
        for ( int i = from ; i < list.size() ; i++ ){
            s.add( list.get(i) );
        }
        return s;
    }

    @Override
    public Object first() {
        return list.get( 0 );
    }

    public Object get(int index){
        try{
            return list.get( index );
        }catch (IndexOutOfBoundsException e){
            if ( index < 0 ) {
                return list.get(list.size() + index);
            }
            return NIL ;
        }
    }

    @Override
    public Object last() {
        return list.get( list.size() - 1 );
    }
}
