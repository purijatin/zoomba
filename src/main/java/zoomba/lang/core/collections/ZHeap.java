package zoomba.lang.core.collections;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 */
public class ZHeap extends ZList {

    public static ZHeap heap(Function f, Object...args){
        if ( args.length == 0 ) return new ZHeap(42); // waste...?
        if ( f == null ){
            if ( args.length == 1 )
                return new ZHeap(ZNumber.integer(args[0],42).intValue() );
            return new ZHeap( ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) );
        }
        if ( args.length == 1 )
            return new ZHeap(ZNumber.integer(args[0],42).intValue(), false, f );

        return new ZHeap(ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) , f );
    }

    final static class ComparatorNullAscending implements Comparator{
        @Override
        public int compare(Object o1, Object o2) {
            if ( o1 == null ){
                if ( o2 == null ) return 0;
                return -1;
            }
            if ( o2 == null ) return 1;
            if ( o1 instanceof Comparable ){
                return ((Comparable) o1).compareTo(o2);
            }
            if ( o2 instanceof Comparable ){
                return -1 * ((Comparable) o2).compareTo(o1);
            }
            throw new UnsupportedOperationException("Can not compare!");
        }

    }

    public static ComparatorNullAscending ASC_COMP = new ComparatorNullAscending();

    public final Comparator comparator;

    public final boolean min;

    public final int maxSize;

    /**
     * For almost sorted array with very small size less than 10ish ... this is faster!
     */
    protected void sort(){
        int len = size();
        if ( len <= 1 ) return;
        // TODO : ideal case would be doing insertion sort : one element would be off balance
        for( int i = 0 ; i < len ; i++ ){
            for  ( int j = i+1; j< len ; j++){
                Object I = get(i);
                Object J = get(j);
                if ( comparator.compare(I, J ) < 0 ){
                    set(j, I) ;
                    set(i, J) ;
                }
            }
        }
    }

    public ZHeap(int size){
        this(size,false);
    }

    public ZHeap(int size, boolean minHeap){
        this(size,minHeap,ASC_COMP);
    }

    public ZHeap(int size, boolean minHeap, Comparator comparator){
        super();
        maxSize = size ;
        min = minHeap ;
        this.comparator = min ? comparator.reversed(): comparator ;
    }

    public ZHeap(int size, boolean minHeap, Function f){
        super();
        maxSize = size ;
        Comparator cmp = new Function.ComparatorLambda( this, f) ;
        min = minHeap ;
        this.comparator = min ? cmp.reversed(): cmp ;
    }


    public  Object min(){
        return min? get(0) : get(size()-1);
    }

    public  Object max(){
        return min? get(size()-1) : get(0) ;
    }

    public Object top(){
        return get(size()-1);
    }

    @Override
    public Object get(int index) {
        try {
            return super.get(index);
        }catch (Exception e){
            return Function.NIL  ;
        }
    }

    @Override
    public boolean add(Object o) {
        if ( size() < maxSize ) {
            super.add(o);
            sort();
            return true ;
        }
        Object top = get(0);
        int cmp = comparator.compare( o, top ) ;
        if ( cmp >= 0 ) { return false ; }
        set(0,o);
        sort();
        return true ;
    }

    @Override
    public boolean remove(Object o) {
        if ( !contains(o) ) return false ;
        super.remove(o);
        sort();
        return true ;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean ret = true ;
        for ( Object item : c ){
            ret &= this.add(item );
        }
        return ret ;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean ret = true ;
        for ( Object item : c ){
            ret &= this.remove(item );
        }
        return ret ;
    }


    @Override
    public String toString() {
        return String.format("h<%s|%s>",top(), super.toString());
    }
}
