package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Arithmetic.*;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

import zoomba.lang.core.types.ZException.Break;
import zoomba.lang.core.types.ZException.Continue;

import static zoomba.lang.core.operations.Function.NIL;


/**
 */
public abstract class BaseZCollection implements ZCollection,
        BasicArithmeticAware, LogicAware, ZTypes.StringX {

    public static final class ZIteratorWrapper implements Iterable{

        final Iterator iterator;

        public ZIteratorWrapper( Iterator iterator){
            this.iterator = iterator ;
        }

        @Override
        public Iterator iterator() {
            return iterator ;
        }
    }

    public static Iterable fromIterator( Iterator iterator ){
        return new ZIteratorWrapper( iterator );
    }

    protected Collection col;

    public BaseZCollection(Object arr) {
        if (arr.getClass().isArray()) {
            col = new ZArray(arr, false);
        } else if ( arr instanceof Iterable ){
             col = list((Iterable)arr);
        } else {
            col = Arrays.asList(arr);
        }
    }

    public BaseZCollection(Collection c) {
        col = c;
    }

    public static List list(Iterable iterable) {
        Iterator i = iterable.iterator();
        List l = new ArrayList();
        while (i.hasNext()) {
            l.add(i.next());
        }
        return l;
    }

    public static Object[] array(Iterable iterable) {
        List l = list( iterable );
        Object[] arr = new Object[l.size()];
        arr = l.toArray(arr);
        l.clear();
        return arr;
    }

    public static Set intersection(Set a, Set b) {
        if (a.isEmpty() || b.isEmpty()) return Collections.EMPTY_SET;
        Set larger = a;
        Set smaller = b;
        if (larger.size() < smaller.size()) {
            larger = b;
            smaller = a;
        }
        Set intersection = new HashSet<>();
        Iterator i = smaller.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            if (larger.contains(o)) {
                intersection.add(o);
            }
        }
        return intersection;
    }

    public static Set union(Set a, Set b) {
        if (a.isEmpty()) return new HashSet<>(b);
        if (b.isEmpty()) return new HashSet<>(a);
        Set smaller = b;
        Set larger = a;
        if (a.size() < b.size()) {
            smaller = a;
            larger = b;
        }
        Set union = new HashSet<>(larger);
        Iterator i = smaller.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            union.add(o);
        }
        return union;
    }

    public static Set minus(Set a, Set b) {
        if (a.isEmpty()) return Collections.EMPTY_SET;
        Set minus = new HashSet<>(a);
        Iterator i = b.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            if (minus.contains(o)) {
                minus.remove(o);
            }
        }
        return minus;
    }

    public static Set set(Set s, Iterable c) {
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            s.add(o);
        }
        return s;
    }

    public static Map<Object, Integer> multiSet(Iterable c) {
        Map<Object, Integer> m = new HashMap<>();
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            if (!m.containsKey(o)) {
                m.put(o, 1);
            } else {
                m.put(o, m.get(o) + 1);
            }
        }
        return m;
    }

    public static Collection compose(Object o, Collection into, List<Function> composes) {
        if (o == null) return Collections.EMPTY_LIST;
        Function[] functions = new Function[composes.size()];
        functions = composes.toArray(functions);
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        } else if (o instanceof ZRange) {
            o = ((ZRange) o).asList();
        }
        boolean intoSpecified = false;
        if (into != null) {
            intoSpecified = true;
        } else {
            into = new ZList();
        }

        if (o instanceof Iterable) {
            if (o instanceof Set) {
                if (!intoSpecified) {
                    into = null;
                    into = new ZSet();
                }
                return compose(into, (Iterable) o, functions);
            }
            return compose(into, (Iterable) o, functions);
        }
        if (o instanceof Map) {
            return compose(into, ((Map) o).entrySet(), functions);
        }
        throw new UnsupportedOperationException("Can not find any iterable from object !");
    }

    public static Collection compose(Collection into, Iterable c, Function... functions) {
        Iterator i = c.iterator();
        int index = -1;
        while (i.hasNext()) {
            Object o = i.next();
            boolean reject = false;
            ++index;
            for (int f = 0; f < functions.length; f++) {
                boolean filter = functions[f] instanceof Function.Predicate;
                Function.MonadicContainer result = functions[f].execute(index, o, c, into);
                if (filter) {
                    if (result instanceof Break) {
                        if (!result.isNil()) {
                            if (ZTypes.bool(result.value(), false)) {
                                into.add(o);
                            }
                        }
                        return into;
                    } else if (result instanceof Continue) {
                        if (!result.isNil()) {
                            if (ZTypes.bool(result.value(), false)) {
                                into.add(o);
                            }
                        }
                        reject = true;
                        break;
                    }
                    if (!ZTypes.bool(result.value(), false)) {
                        reject = true;
                        break;
                    }

                } else {
                    // mapper
                    if (result instanceof Break) {
                        if (!result.isNil()) {
                            into.add(result.value());
                        }
                        return into;
                    } else if (result instanceof Continue) {
                        if (!result.isNil()) {
                            into.add(result.value());
                        }
                        break;
                    }
                    o = result.value();
                }
            }
            if (!reject) {
                into.add(o);
            }
        }
        return into;
    }

    static void putBackInMap(Map m, Object r){
        if ( r instanceof List ){
            m.put(((List) r).get(0), ((List) r).get(1));
        }else if ( r instanceof Map.Entry ){
            m.put( ((Map.Entry) r).getKey(), ((Map.Entry) r).getValue() );
        }
        else{
            Object[] pair = (Object[])r;
            m.put(pair[0], pair[1]);
        }
    }

    public static Map dict(Map m, Function mapper, Iterable c) {
        Iterator i = c.iterator();
        int index = -1;
        while (i.hasNext()) {
            index++;
            Object o = i.next();
            Function.MonadicContainer result = mapper.execute(index, o, c, m);
            boolean br;
            if ((br = result instanceof Break) || (result instanceof Continue)) {
                if (!result.isNil()) {
                    Object r = result.value();
                    putBackInMap(m,r);
                }
                if (br) break;
                continue;
            }
            if (result.isNil()) throw new UnsupportedOperationException("Mapper function can not be void!");
            Object r = result.value();
            putBackInMap(m,r);
        }
        return m;
    }

    public static Set set(Set s, Function hash, Iterable c) {
        return (Set)map(s,hash,c);
    }

    public static Map<Object, List> multiSet(Function hash, Iterable c) {

        Map<Object, List> map = new HashMap<>();
        Iterator i = c.iterator();
        int index = 0;
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = hash.execute(index, o, c, map);
            boolean br;

            if ((br = result instanceof Break) || (result instanceof Continue)) {
                if (!result.isNil()) {
                    if (map.containsKey(result.value())) {
                        map.get(result.value()).add(o);
                    } else {
                        List l = new ArrayList();
                        l.add(o);
                        map.put(result.value(), l);
                    }
                }
                if (br) break;
                continue;
            }
            if (result.isNil()) throw new UnsupportedOperationException("Function can not be void!");

            if (map.containsKey(result.value())) {
                map.get(result.value()).add(o);
            } else {
                List l = new ArrayList();
                l.add(o);
                map.put(result.value(), l);
            }
        }
        return map;

    }

    public static Relation relate(Collection c1, Collection c2) {
        // handle empty collections
        if (c1.isEmpty()) {
            if (c2.isEmpty()) return Relation.EQUAL;
            return Relation.SUB;
        }
        if (c1.isEmpty()) return Relation.SUPER;

        Map<Object, Integer> thisMap = multiSet(c1);
        Map<Object, Integer> thatMap = multiSet(c2);

        Set intersection = intersection(thisMap.keySet(), thatMap.keySet());
        if (intersection.isEmpty()) return Relation.INDEPENDENT;

        Set this_that = minus(thisMap.keySet(), thatMap.keySet());
        Set that_this = minus(thatMap.keySet(), thisMap.keySet());
        boolean thisSubThat = true;
        boolean thatSubThis = true;

        if (this_that.isEmpty() && that_this.isEmpty()) {
            // possibility of equal or something
            Iterator i = thisMap.keySet().iterator();
            while (i.hasNext()) {
                Object k = i.next();
                int thisCount = thisMap.get(k);
                int thatCount = thatMap.get(k);
                if (thisSubThat && thisCount > thatCount) {
                    thisSubThat = false;
                }
                if (thatSubThis && thatCount > thisCount) {
                    thatSubThis = false;
                }
                if (!thisSubThat && !thatSubThis) return Relation.OVERLAP;
            }
            if (thisSubThat && thatSubThis) return Relation.EQUAL;
            if (thisSubThat) return Relation.SUB;
            if (thatSubThis) return Relation.SUPER;
        }
        if (this_that.isEmpty()) {
            // this - that  = empty ==> that > this or overlap
            Iterator i = intersection.iterator();
            while (i.hasNext()) {
                Object k = i.next();
                int thisCount = thisMap.get(k);
                int thatCount = thatMap.get(k);

                if (thisCount > thatCount) {
                    return Relation.OVERLAP;
                }
            }
            return Relation.SUB;
        }
        if (that_this.isEmpty()) {
            Iterator i = intersection.iterator();
            while (i.hasNext()) {
                Object k = i.next();
                int thisCount = thisMap.get(k);
                int thatCount = thatMap.get(k);

                if (thatCount > thisCount) {
                    return Relation.OVERLAP;
                }
            }
            return Relation.SUPER;
        }

        return Relation.OVERLAP;
    }

    public static Collection union(Collection into, Iterable c1, Iterable c2) {
        Map<Object, Integer> m1 = multiSet(c1);
        Map<Object, Integer> m2 = multiSet(c2);
        Set u = union(m1.keySet(), m2.keySet());
        Iterator i = u.iterator();
        while (i.hasNext()) {
            Object k = i.next();
            int count1 = m1.containsKey(k) ? m1.get(k) : 0;
            int count2 = m2.containsKey(k) ? m2.get(k) : 0;
            if (count1 < count2) {
                for (int c = 0; c < count2; c++) {
                    into.add(k);
                }
            } else {
                for (int c = 0; c < count1; c++) {
                    into.add(k);
                }
            }
        }
        return into;
    }

    public static void unionMutable(Collection c1, Collection c2) {
        Map<Object, Integer> m1 = multiSet(c1);
        Map<Object, Integer> m2 = multiSet(c2);
        for (Object k : m2.keySet()) {
            int count2 = m2.get(k);
            if (m1.containsKey(k)) {
                int count1 = m1.get(k);
                count2 -= count1;
            }
            for (int i = 0; i < count2; i++) {
                c1.add(k);
            }
        }
    }

    public static void intersectionMutable(Collection c1, Collection c2) {
        Map<Object, Integer> m1 = multiSet(c1);
        Map<Object, Integer> m2 = multiSet(c2);
        Set intersection = intersection(m1.keySet(), m2.keySet());
        Set difference = minus(m1.keySet(), intersection);
        for (Object k : intersection) {
            int count1 = m1.get(k);
            int count2 = m2.get(k);
            if (count2 < count1) {
                int diff = count1 - count2;
                for (int i = 0; i < diff; i++) {
                    c1.remove(k);
                }
            }
        }
        for (Object k : difference) {
            int count1 = m1.get(k);
            for (int i = 0; i < count1; i++) {
                c1.remove(k);
            }
        }
    }

    public static Collection intersection(Collection into, Iterable c1, Iterable c2) {
        Map<Object, Integer> m1 = multiSet(c1);
        Map<Object, Integer> m2 = multiSet(c2);
        Set ii = intersection(m1.keySet(), m2.keySet());
        Iterator i = ii.iterator();
        while (i.hasNext()) {
            Object k = i.next();
            int count1 = m1.get(k);
            int count2 = m2.get(k);
            if (count1 < count2) {
                for (int c = 0; c < count1; c++) {
                    into.add(k);
                }
            } else {
                for (int c = 0; c < count2; c++) {
                    into.add(k);
                }
            }
        }
        return into;
    }

    public static Collection difference(Collection into, Iterable c1, Iterable c2) {
        Map<Object, Integer> m1 = multiSet(c1);
        Map<Object, Integer> m2 = multiSet(c2);
        Set c1_c2 = minus(m1.keySet(), m2.keySet());
        Iterator i = c1_c2.iterator();
        while (i.hasNext()) {
            Object k = i.next();
            int count1 = m1.get(k);
            for (int c = 0; c < count1; c++) {
                into.add(k);
            }
        }
        Set ii = intersection(m1.keySet(), m2.keySet());
        i = ii.iterator();
        while (i.hasNext()) {
            Object k = i.next();
            int count1 = m1.get(k);
            int count2 = m2.get(k);
            if (count1 > count2) {
                for (int c = 0; c < count1 - count2; c++) {
                    into.add(k);
                }
            }
        }
        return into;
    }

    public static Collection product(Collection into, Iterable c1, Iterable c2) {
        for (Object o1 : c1) {
            for (Object o2 : c2) {
                into.add(new ZArray(new Object[]{o1, o2}, false));
            }
        }
        return into;
    }

    static boolean next(Object[] tuple, List<Iterator> states, Iterable[] cc) {
        boolean carry = true;
        for (int i = cc.length - 1; i >= 0; i--) {
            if (!carry) break;
            Iterator iterator = states.get(i);
            if (iterator.hasNext()) {
                tuple[i] = iterator.next();
                carry = false;
            } else {
                if (i == 0) return true;
                carry = true;
                iterator = cc[i].iterator();
                tuple[i] = iterator.next();
                states.set(i, iterator);
            }
        }
        return false;
    }

    public static Collection join(Collection into, Iterable[] cc){
        return join(into,Function.TRUE,Function.COLLECTOR_IDENTITY,cc);
    }

    public static Collection join(Collection into, Function predicate, Iterable[] cc){
        return join(into,predicate,Function.COLLECTOR_IDENTITY,cc);
    }

    static void extractResult( Collection into, Function.MonadicContainer result , Object[] tuple ){
        if ( result.isNil() ) return;
        Object o = result.value();
        if (tuple.equals(o)) {
            List l = new ZArray(o);
            into.add(l);
        } else {
            into.add(o);
        }
    }

    public static Collection join(Collection into, Function predicate, Function map, Iterable[] cc) {
        if (cc.length < 2) return into;
        int index = 0;
        Object[] tuple = new Object[cc.length];
        List<Iterator> myStates = new ArrayList<>();
        for (int i = 0; i < cc.length; i++) {
            Iterator iterator = cc[i].iterator();
            if (!iterator.hasNext()) return into;
            tuple[i] = iterator.next();
            myStates.add(iterator);
        }
        boolean carry = false;
        while (!carry) {
            Function.MonadicContainer result = predicate.execute(index, tuple, cc, into);
            if (!result.isNil() && ZTypes.bool(result.value(), false)) {
                result = map.execute(index, tuple, cc, into);
                extractResult( into, result, tuple );
            }
            if (result instanceof Break) {
                extractResult( into, result, tuple );
                break;
            }
            carry = next(tuple, myStates, cc);
            index++;
        }
        return into;
    }

    public static Collection map(Collection into, Function map, Iterable c) {
        int index = 0;
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = map.execute(index, o, c, into);
            if (!result.isNil()) {
                into.add(result.value());
            }
            if (result instanceof Break) return into;
            index++;
        }
        return into;
    }

    public static Collection flatMap(Collection into, Function map, Iterable c) {
        int index = 0;
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            if (o != null) {
                if (o instanceof Collection) {
                    flatMap(into, map, (Collection) o);
                    continue;
                }
                if (o.getClass().isArray()) {
                    flatMap(into, map, new ZArray(o, false));
                    continue;
                }
            }
            Function.MonadicContainer result = map.execute(index, o, c, into);
            if (!result.isNil()) {
                into.add(result.value());
            }
            if (result instanceof Break) return into;
            index++;
        }
        return into;
    }

    public static Collection select(Collection into, Function predicate, Iterable c) {
        int index = 0;
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = predicate.execute(index, o, c, into);
            if (!result.isNil() && ZTypes.bool(result.value())) {
                into.add(o);
            }
            if (result instanceof Break) return into;
            index++;
        }
        return into;
    }

    public static Collection[] partition(Collection[] into, Function predicate, Iterable c) {
        int index = 0;
        Iterator i = c.iterator();
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = predicate.execute(index, o, c, into);
            if (!result.isNil() && ZTypes.bool(result.value())) {
                into[0].add(o);
            } else {
                into[1].add(o);
            }
            if (result instanceof Break) return into;
            index++;
        }
        return into;
    }

    public static Object leftFold(Iterable col, Function fold, Object... arg) {
        Iterator i = col.iterator();
        int index = 0;
        Object foldArg = arg.length > 0 ? arg[0] : NIL;
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = fold.execute(index, o, col, foldArg);
            if (result instanceof Break) {
                if (!result.isNil()) {
                    foldArg = result.value();
                }
                break;
            }
            if (!result.isNil()) {
                foldArg = result.value();
            }
            index++;
        }
        return foldArg;
    }

    public static Object rightFold(List col, Function fold, Object... arg) {
        ListIterator i = col.listIterator(col.size());
        int index = col.size() - 1;
        Object foldArg = arg.length > 0 ? arg[0] : NIL;
        while (i.hasPrevious()) {
            Object o = i.previous();
            Function.MonadicContainer result = fold.execute(index, o, col, foldArg);
            if (result instanceof Break) {
                if (!result.isNil()) {
                    foldArg = result.value();
                }
                break;
            }
            if (!result.isNil()) {
                foldArg = result.value();
            }
            index--;
        }
        return foldArg;
    }

    public static Object leftReduce(Iterable col, Function reduce) {
        Iterator i = col.iterator();
        int index = 0;
        if ( ! i.hasNext() ) return NIL ;
        Object o = i.next();
        index++ ;
        while (i.hasNext()) {
            Object cur = i.next();
            Function.MonadicContainer result = reduce.execute(index, cur, col, o);
            if (result instanceof Break) {
                if (!result.isNil()) {
                   return result.value();
                }
                break;
            }
            if (!result.isNil()) {
                o = result.value();
            }
            index++;
        }
        return o;
    }

    public static Object rightReduce(List col, Function reduce) {
        ListIterator i = col.listIterator(col.size());
        int index = col.size() - 1;
        if ( index < 0 ) return NIL ;
        Object o = i.previous();
        index--;
        while (i.hasPrevious()) {
            Object cur = i.previous();
            Function.MonadicContainer result = reduce.execute(index, cur, col, o );
            if (result instanceof Break) {
                if (!result.isNil()) {
                    return result.value();
                }
                break;
            }
            if (!result.isNil()) {
                o = result.value();
            }
            index--;
        }
        return o;
    }


    public static Function.MonadicContainer find(Iterable col, Function predicate) {
        Iterator i = col.iterator();
        int index = 0;
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = predicate.execute(index, o, col, NIL);
            if (!result.isNil() && ZTypes.bool(result.value(), false)) {
                return new Function.MonadicContainerBase(o);
            }
            if (result instanceof Break) {
                return Function.NOTHING  ;
            }
            index++;
        }
        return Function.NOTHING ;
    }

    public static int leftIndex(Iterable col, Object item) {
        Iterator i = col.iterator();
        int index = 0;
        while (i.hasNext()) {
            Object o = i.next();
            if (ZTypes.equals(item, o)) return index;
            index++;
        }
        return -1;
    }

    public static int leftIndex(Iterable col, Function predicate) {
        Iterator i = col.iterator();
        int index = 0;
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = predicate.execute(index, o, col, NIL);
            if (!result.isNil() && ZTypes.bool(result.value(), false)) {
                return index;
            }
            if (result instanceof Break) {
                return -1;
            }
            index++;
        }
        return -1;
    }

    public static int rightIndex(List col, Function predicate) {
        ListIterator i = col.listIterator(col.size());
        int index = col.size() - 1;
        while (i.hasPrevious()) {
            Object o = i.previous();
            Function.MonadicContainer result = predicate.execute(index, o, col, NIL);
            if (!result.isNil() && ZTypes.bool(result.value(), false)) {
                return index;
            }
            if (result instanceof Break) {
                return -1;
            }
            index--;
        }
        return -1;
    }

    public static int rightIndex(List col, Object item) {
        ListIterator i = col.listIterator(col.size());
        int index = col.size() - 1;
        while (i.hasPrevious()) {
            Object o = i.previous();
            if (ZTypes.equals(item, o)) return index;
            index--;
        }
        return -1;
    }

    public abstract ZCollection collector();

    public abstract ZCollection myCopy();

    public abstract Set setCollector();

    @Override
    public Set toSet(Function hash) {
        Set s = setCollector();
        return set(s, hash, this);
    }

    @Override
    public Set toSet() {
        Set s = setCollector();
        return set(s, this);
    }

    @Override
    public ZCollection union(Collection c) {
        ZCollection into = collector();
        return (ZCollection) union(into, this, c);
    }

    @Override
    public ZCollection intersection(Collection c) {
        ZCollection into = collector();
        return (ZCollection) intersection(into, this, c);
    }

    @Override
    public ZCollection difference(Collection c) {
        Collection into = collector();
        return (ZCollection) difference(into, this, c);
    }

    @Override
    public ZCollection product(Collection c) {
        Collection into = collector();
        return (ZCollection) product(into, this, c);
    }

    @Override
    public ZCollection join(Collection... cc) {
        return join(Function.TRUE, cc);
    }

    @Override
    public ZCollection join(Function predicate, Collection... cc) {
        ZCollection into = collector();
        Collection[] cols = new Collection[cc.length + 1];
        cols[0] = this;
        for (int i = 1; i < cols.length; i++) {
            cols[i] = cc[i - 1];
        }
        return (ZCollection) join(into, predicate, cols);
    }

    @Override
    public ZCollection join(Function predicate, Function map, Collection... cc) {
        ZCollection into = collector();
        Collection[] cols = new Collection[cc.length + 1];
        cols[0] = this;
        for (int i = 1; i < cols.length; i++) {
            cols[i] = cc[i - 1];
        }
        return (ZCollection) join(into, predicate, map, cols);
    }

    @Override
    public ZCollection map(Function map) {
        ZCollection into = collector();
        return (ZCollection) map(into, map, this);
    }

    @Override
    public ZCollection flatMap(Function map) {
        Collection into = collector();
        return (ZCollection) flatMap(into, map, this);
    }

    @Override
    public ZCollection flatten() {
        return flatMap(Function.COLLECTOR_IDENTITY);
    }

    @Override
    public ZCollection[] partition(Function predicate) {
        ZCollection[] cols = new ZCollection[]{collector(), collector()};
        return (ZCollection[]) partition(cols, predicate, this);
    }

    @Override
    public ZCollection select(Function predicate) {
        ZCollection into = collector();
        return (ZCollection) select(into, predicate, this);
    }

    @Override
    public int size() {
        return col.size();
    }

    @Override
    public boolean isEmpty() {
        return col.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return col.contains(o);
    }

    @Override
    public Iterator iterator() {
        return col.iterator();
    }

    @Override
    public Object[] toArray() {
        return col.toArray();
    }

    @Override
    public Object[] toArray(Object[] a) {
        return col.toArray(a);
    }

    @Override
    public boolean add(Object o) {
        return col.add(o);
    }

    @Override
    public boolean remove(Object o) {
        return col.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return col.containsAll(c);
    }

    @Override
    public boolean addAll(Collection c) {
        return col.addAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return col.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection c) {
        return col.retainAll(c);
    }

    @Override
    public void clear() {
        col.clear();
    }

    @Override
    public boolean removeIf(Predicate filter) {
        return col.removeIf(filter);
    }

    @Override
    public Spliterator spliterator() {
        return col.spliterator();
    }

    @Override
    public Stream stream() {
        return col.stream();
    }

    @Override
    public Stream parallelStream() {
        return col.parallelStream();
    }

    @Override
    public Map<Object, Integer> toMultiSet() {
        return multiSet(this);
    }

    @Override
    public Map<Object, List> toMultiSet(Function hash) {
        return multiSet(hash, this);
    }

    @Override
    public Relation relate(Collection c) {
        return relate(this, c);
    }

    @Override
    public void forEach(Function apply) {
        Iterator i = iterator();
        int index = 0;
        while (i.hasNext()) {
            Object o = i.next();
            Function.MonadicContainer result = apply.execute(index, o, this, NIL);
            if (result instanceof Break) {
                break;
            }
            index++;
        }
    }

    @Override
    public Object leftFold(Function fold, Object... arg) {
        return leftFold(this, fold, arg);
    }


    @Override
    public Function.MonadicContainer find(Function predicate) {
        return find(this, predicate);
    }

    @Override
    public int leftIndex(Function predicate) {
        return leftIndex(this, predicate);
    }

    @Override
    public Object rightFold(Function fold, Object... arg) {
        throw new UnsupportedOperationException("Can not traverse collection from Right!");
    }

    @Override
    public int rightIndex(Function predicate) {
        throw new UnsupportedOperationException("Can not search collection from Right!");
    }

    @Override
    public Object leftReduce(Function reduce) {
        return leftReduce(this,reduce);
    }

    @Override
    public Object rightReduce(Function reduce) {
        throw new UnsupportedOperationException("Can not reduce collection from Right!");
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) return 1;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        // by this it surely be
        if (o instanceof Collection) {
            if (((Collection) o).isEmpty()) {
                if (isEmpty()) return 0;
                return 1;
            }
            Relation r = relate((Collection) o);
            switch (r) {
                case EQUAL:
                    return 0;
                case SUB:
                    return -1;
                case SUPER:
                    return 1;
                default:
                    throw new UnsupportedOperationException("Can not compare Non Comparable Collections !");
            }
        }
        throw new UnsupportedOperationException("Can not compare against Non Collections !");
    }

    public abstract String containerFormatString();

    @Override
    public String string(String separator) {
        if (isEmpty()) return "";
        Iterator iterator = iterator();
        String v = String.valueOf(iterator.next());
        StringBuilder buf = new StringBuilder(v);
        while (iterator.hasNext()) {
            v = String.valueOf(iterator.next());
            buf.append(separator).append(v);
        }
        return buf.toString();
    }

    @Override
    public String toString() {
        String formatString = containerFormatString();
        return String.format(formatString, string(","));
    }

    @Override
    public Object _add_(Object o) {
        ZCollection c = myCopy();
        if (o != null) {
            if (o.getClass().isArray()) {
                o = new ZArray(o, false);
            }
            if (o instanceof Collection) {
                c.addAll((Collection) o);
                return c;
            }
        }
        c.add(o);
        return c;
    }

    @Override
    public Object _sub_(Object o) {
        ZCollection c = myCopy();
        if (o != null) {
            if (o.getClass().isArray()) {
                o = new ZArray(o, false);
            }
            if (o instanceof Collection) {
                c.removeAll((Collection) o);
                return c;
            }
        }
        c.remove(o);
        return c;
    }

    @Override
    public Object _mul_(Object o) {
        if (o == null) return null;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            ZCollection c = myCopy();
            return c.product((Collection) o);
        }
        throw new UnsupportedOperationException(
                "Can not take a product between a collection and a : " + o.getClass());
    }

    @Override
    public Object _div_(Object o) {
        throw new UnsupportedOperationException(
                "Can not do a division over Collection!");
    }

    @Override
    public Object _pow_(Object o) {
        if (o == null) return null;
        if (o instanceof Integer || o instanceof Long) {
            int l = Integer.valueOf(o.toString());
            if (l == 0) return collector();
            ZCollection c;
            if (l < 0) {
                c = reverse();
                l = -l;
            } else {
                c = myCopy();
            }
            if (l == 1) return c;
            // we should use join now
            Collection[] cc = new Collection[l - 1];
            for (int i = 0; i < cc.length; i++) {
                cc[i] = c;
            }
            c = c.join(cc);
            return c;
        }
        throw new UnsupportedOperationException(
                "Can not do a power over Collection using : " + o.getClass());
    }

    static Collection fromObject( Object o){
        if ( o == null ) return null;
        if ( o.getClass().isArray() ){
            return new ZArray(o,false);
        }
        if ( o instanceof Collection ){
            return (Collection)o;
        }
        return null;
    }

    @Override
    public void add_mutable(Object o) {
        if (this == o) {
            Collection c = myCopy();
            addAll(c);
            return;
        }
        Collection oc = fromObject( o );
        if (oc != null) {
            addAll( oc );
            return;
        }
        add(o);
    }

    @Override
    public void sub_mutable(Object o) {
        if (this == o) {
            clear();
            return;
        }
        Collection oc = fromObject( o );
        if (oc != null) {
            removeAll( oc );
            return;
        }
        remove(o);
    }

    @Override
    public void mul_mutable(Object o) {
        if (o == null) return;
        if (this == o) {
            Collection c = (Collection) _mul_(o);
            clear();
            addAll(c);
            return;
        }

        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            ZCollection c = myCopy();
            clear();
            product(this, c, (Collection) o);
            return;
        }
        throw new UnsupportedOperationException(
                "Can not do a multiplication of collection with : " + o.getClass());
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException(
                "Can not do a division over Collection!");
    }

    @Override
    public Object _and_(Object o) {
        if (o == null) return null;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            return intersection((Collection) o);
        }
        throw new UnsupportedOperationException(
                "Can not do 'AND' with non Collection!");
    }

    @Override
    public Object _or_(Object o) {
        if (o == null) return null;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            return union((Collection) o);
        }
        throw new UnsupportedOperationException(
                "Can not do 'OR' with non Collection!");

    }

    @Override
    public Object _xor_(Object o) {
        if (o == null) return null;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            ZCollection this_that = difference((Collection) o);
            Collection that_this = difference(collector(), (Collection) o, this);
            return this_that.union(that_this);
        }
        throw new UnsupportedOperationException(
                "Can not do 'XOR' with non Collection!");

    }

    @Override
    public void and_mutable(Object o) {
        if (o == null) return;
        if (this == o) return;

        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            intersectionMutable(this, (Collection) o);
            return;
        }
        throw new UnsupportedOperationException(
                "Can not do 'AND' with non Collection!");
    }

    @Override
    public void or_mutable(Object o) {
        if (o == null) return;
        if (this == o) return;
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            unionMutable(this, (Collection) o);
            return;
        }
        throw new UnsupportedOperationException(
                "Can not do 'OR' with non Collection!");

    }

    @Override
    public void xor_mutable(Object o) {
        if (o == null) return;
        if (this == o) {
            clear();
            return;
        }
        if (o.getClass().isArray()) {
            o = new ZArray(o, false);
        }
        if (o instanceof Collection) {
            Collection d = difference((Collection) o);
            sub_mutable(o);
            or_mutable(d);
            return;
        }
        throw new UnsupportedOperationException(
                "Can not do 'XOR' with non Collection!");

    }

    @Override
    public int hashCode() {
        if (col instanceof BaseZCollection) {
            // is a container type, thus...
            return ((BaseZCollection) col).col.hashCode();
        }
        return col.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        try {
            return (0 == compareTo(obj));
        } catch (UnsupportedOperationException e) {
            return false;
        }
    }

    @Override
    public String string(Object... args) {
        if (args.length == 0) {
            return toString();
        }
        boolean json = ZTypes.bool(args[0], false);
        if (json) return ZTypes.jsonString(this);
        return ZTypes.string(this, String.valueOf(args[0]));
    }

    @Override
    public ZCollection compose(Function... functions) {
        return (ZCollection) compose(collector(), this, functions);
    }
}
