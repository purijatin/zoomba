package zoomba.lang.core.collections;

import zoomba.lang.core.operations.ZCollection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 */
public class ZSet extends BaseZCollection implements Set {

    public ZSet() {
        super(new HashSet());
    }

    public ZSet(Object arr) {
        super(arr);
    }

    public ZSet(Set c) {
        super(c);
    }

    public ZSet(Collection c) {
        super(c);
        col = new HashSet<>(col);
    }

    @Override
    public ZCollection reverse() {
        return myCopy(); // does not actually reverses
    }

    @Override
    public ZCollection collector() {
        return new ZSet();
    }

    @Override
    public Set setCollector() {
        return new ZSet();
    }

    @Override
    public ZCollection myCopy() {
        return new ZSet(new HashSet<>(col) );
    }

    @Override
    public String containerFormatString() {
        return "{ %s }";
    }

}
