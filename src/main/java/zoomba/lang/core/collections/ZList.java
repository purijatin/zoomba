package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.core.types.ZException.Break;
import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class ZList extends BaseZCollection implements List{

    public ZList(Object arr) {
        super(arr);
        col = new ArrayList<>(col);
    }

    public ZList(Collection c) {
        super(c);
        col = new ArrayList<>(col);
    }

    public ZList(Iterator i) {
        super(new ArrayList() );
        while ( i.hasNext() ){
            col.add(i.next());
        }
    }

    public ZList(Iterable i) {
        this(i.iterator());
    }

    public ZList() {
        super(new ArrayList());
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return ((List)col).addAll(index, c );
    }

    @Override
    public Object get(int index) {
        try {
            return ((List) col).get(index);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return get(col.size() + index );
            }
            throw e;
        }
    }

    @Override
    public Object set(int index, Object element) {
        try {
            return ((List) col).set(index, element);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return set(col.size() + index, element );
            }
            throw e;
        }
    }

    @Override
    public void add(int index, Object element) {
        ((List)col).add(index,element);
    }

    @Override
    public Object remove(int index) {
        return ((List)col).remove(index);
    }

    @Override
    public int indexOf(Object o) {
        if ( o == null ) return ((List)col).indexOf(o);

        List l = null;
        if ( o.getClass().isArray() ){
             l = new ZArray(o,false);
        }else if ( o instanceof List ){
            l = (List)o;
        }
        if ( l == null ) return ((List)col).indexOf(o);

        if ( l.isEmpty() ) return 0;
        if ( isEmpty() ) return -1;

        int mySize = size();
        if ( l.size() > mySize ) return -1;
        int j = 0 ;
        int startIndex = 0 ;
        for ( int i = 0 ; i < size(); ){

            Object mine = get(i);
            Object her = l.get(j);
            if ( ZTypes.equals(mine,her ) ){
                if ( j == 0 ){ startIndex = j ; }
                i++;
                j++;
                if ( j == l.size() ) return startIndex ;
            }else{
                i++;
                j=0;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if ( o == null ) return ((List)col).lastIndexOf(o);

        List l = null;
        if ( o.getClass().isArray() ){
            l = new ZArray(o,false);
        }else if ( o instanceof List ){
            l = (List)o;
        }
        if ( l == null ) return ((List)col).lastIndexOf(o);

        if ( l.isEmpty() ) return size() - 1;
        if ( isEmpty() ) return -1;

        int mySize = size();
        if ( l.size() > mySize ) return -1;
        int j = l.size() - 1 ;
        int startIndex = size() - 1 ;
        for ( int i = size() -1 ; i >= 0 ; ){

            Object mine = get(i);
            Object her = l.get(j);
            if ( ZTypes.equals(mine,her ) ){
                if ( j == l.size() - 1 ){ startIndex = i ; }
                i--;
                j--;
                if ( j < 0 ) return startIndex ;
            }else{
                i--;
                j = l.size() -1 ;
            }
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return ((List)col).listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return ((List)col).listIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return ((List)col).subList(fromIndex,toIndex);
    }

    @Override
    public ZCollection collector() {
        return new ZList();
    }

    @Override
    public Set setCollector() {
        return new ZSet();
    }

    @Override
    public Object rightFold(Function fold, Object... arg) {
        return rightFold(this,fold,arg);
    }

    @Override
    public int rightIndex(Function predicate) {
        return rightIndex(this,predicate);
    }

    @Override
    public Object rightReduce(Function reduce) {
        return rightReduce( this, reduce);
    }

    @Override
    public ZCollection reverse() {
        ZList reverse = new ZList();
        for ( int i = size() - 1; i >= 0 ; i--){
            reverse.add( get(i));
        }
        return reverse ;
    }

    @Override
    public ZCollection myCopy() {
        return new ZList(new ArrayList<>(col));
    }

    @Override
    public String containerFormatString() {
        return "[ %s ]";
    }

    /*
    * Stack and Queue Manipulation Technique
    * https://upload.wikimedia.org/wikipedia/commons/5/52/Data_Queue.svg
    * */

    public boolean enqueue(Object item){
        return add(item);
    }

    public Object dequeue(){
        try {
            return remove(0);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    public Object front(){
        try {
            return get(0);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    public Object back(){
        try {
            return get(size()-1);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    public Object top(){
        return back();
    }

    public boolean push(Object item){
        return enqueue(item);
    }

    public Object pop(){
        try {
            return remove(size()-1);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }
}
