package zoomba.lang.core.collections;

import java.math.BigInteger;
import java.util.*;

/**
 */
public final class Combinatorics {

    final List<Comparable> items;

    protected final int r ;

    private Combinatorics(Object... args) {
        if ( args == null ) throw new UnsupportedOperationException("There should be items!");
        if ( args.length == 0 ) throw new UnsupportedOperationException("There should be items!");
        Object objects = args[0];
        Integer count = null;
        if ( args.length > 1 && args[1] instanceof Integer ){
            count = (Integer)args[1];
        }
        if ( objects instanceof Iterator ){
            objects = BaseZCollection.fromIterator( (Iterator)objects );
        }
        if ( objects instanceof Iterable ){
            objects = new ZList( (Iterable)objects );
        }

        if ( objects instanceof List){
            items = new ArrayList( (List)objects );
        }else if (objects.getClass().isArray() ) {
            items =  new ZArray( objects, true ) ;
        }else{
            throw new UnsupportedOperationException("There should be items!");
        }
        Set s = new HashSet<>(items);
        if (s.isEmpty() || s.size() != items.size())
            throw new UnsupportedOperationException("All items must be distinct!");
        Collections.sort(items,Collections.reverseOrder()); // sort it off
        if ( count == null ){
            r = s.size();
        }else{
            r = count ;
        }
    }

    public Combinatorics(Object objects ){
        this(objects,null);
    }

    static abstract class CombinatoricsIterator implements Iterator{

        protected Combinatorics combinatorics;

        protected Comparable[] next;

        protected BigInteger index ;

        public CombinatoricsIterator(Combinatorics combinatorics) {
            this.combinatorics = combinatorics;
            next = new Comparable[this.combinatorics.r];
            index = BigInteger.ZERO ;
        }

        protected boolean select(){
            index = index.add( BigInteger.ONE );
            String sr = index.toString(2);
            while ( sr.length() <= combinatorics.items.size() ){
                int oneCount = 0 ;
                for ( int i = 0 ; i < sr.length(); i++ ){
                    if ( sr.charAt(i) == '1' ){
                        oneCount ++;
                    }
                }
                if ( oneCount != combinatorics.r ){
                    index = index.add( BigInteger.ONE );
                    sr = index.toString(2);
                }else{
                    int offSet = combinatorics.items.size() - sr.length();
                    StringBuilder buf = new StringBuilder();
                    for ( int i = 0 ; i < offSet ; i++ ){
                        buf.append('0');
                    }
                    buf.append(sr);
                    sr = buf.toString();
                    int start = 0 ;
                    for ( int i = sr.length() - 1; i >=0 ; i-- ){
                        if ( sr.charAt(i) == '1' ){
                            next[start] = combinatorics.items.get(i);
                            start++;
                        }
                    }
                    return true ;
                }
            }
            return false ;
        }

        public abstract boolean nextHigherCombinatorics();

        @Override
        public boolean hasNext() {
            return nextHigherCombinatorics();
        }

        @Override
        public Object next() {
            return new ZArray( next, true );
        }

    }

    static final class CombinationIterator extends CombinatoricsIterator{

        private CombinationIterator(Combinatorics combinations) {
            super(combinations);
        }

        @Override
        public boolean nextHigherCombinatorics() {
            return select();
        }
    }

    static final class PermutationIterator extends CombinatoricsIterator {

        PermutationIterator(Combinatorics permutations) {
            super(permutations);
        }

        /**
         * http://stackoverflow.com/questions/1622532/algorithm-to-find-next-greater-permutation-of-a-given-string
         *
         * @return true if exists, false if does not
         */
        @Override
        public boolean nextHigherCombinatorics() {
            if ( next[0] == null  ){
                return select();
            }

            int i = next.length - 1;
            while (i > 0 && next[i - 1].compareTo(next[i]) >= 0) {
                i--;
            }

            if (i <= 0) {
                return select();
            }

            int j = next.length - 1;

            while (next[j].compareTo(next[i - 1]) <= 0) {
                j--;
            }

            Comparable temp = next[i - 1];
            next[i - 1] = next[j];
            next[j] = temp;

            j = next.length - 1;

            while (i < j) {
                temp = next[i];
                next[i] = next[j];
                next[j] = temp;
                i++;
                j--;
            }

            return true;
        }
    }

    static final class CombinatoricsIterable implements Iterable {

        CombinatoricsIterator c;

        public CombinatoricsIterable(CombinatoricsIterator c){
            this.c = c ;
        }

        @Override
        public Iterator iterator() {
            return c;
        }
    }

    public Iterable p(){
        return new CombinatoricsIterable( new PermutationIterator( this ) );
    }

    public Iterable c(){
        return new CombinatoricsIterable( new CombinationIterator( this ) );
    }

    public static Iterable permutations(Object... args){
        return new Combinatorics( args ).p();
    }

    public static Iterable combinations(Object... args){
        return new Combinatorics( args ).c();
    }

    public static final class Sequences implements Iterable{

        final List sequence ;

        public Sequences(Object[] args){
            if ( args.length == 0 || args[0] == null ) {
                sequence = Collections.emptyList();
                return;
            }
            Object s = args[0];
            if ( args.length > 1 ){
                s = args ;
            }
            if ( s instanceof Iterator ){
                s = BaseZCollection.fromIterator((Iterator)s);
            }

            if ( s instanceof Iterable ){
                sequence = new ZList( (Iterable) s);
            } else if ( s.getClass().isArray() ){
                sequence = new ZArray(s,false);
            } else {
                throw new RuntimeException("Sorry, not a sequence!");
            }
        }

        final class PowerSetIterator implements Iterator {

            Sequences subsequences ;

            private BigInteger index = BigInteger.ZERO ;

            final BigInteger maxSize ;

            PowerSetIterator( Sequences subsequences){
                 this.subsequences = subsequences ;
                 BigInteger two = new BigInteger( "2" );
                 maxSize = two.pow( this.subsequences.sequence.size() );
            }

            @Override
            public boolean hasNext() {
                index = index.add( BigInteger.ONE );
                return maxSize.compareTo( index ) > 0 ;
            }

            @Override
            public Object next() {
                String binRep = index.toString(2);
                StringBuilder buf = new StringBuilder();
                int len = subsequences.sequence.size();
                for ( int i = binRep.length(); i < len ; i++ ){
                    buf.append('0');
                }
                buf.append(binRep);
                binRep = buf.toString();
                List subSequence = new ArrayList();
                for ( int i = 0 ; i < len ; i++ ){
                    if ( binRep.charAt(i) == '1' ){
                       subSequence.add(  subsequences.sequence.get(i) ) ;
                    }
                }
                return  subSequence;
            }
        }

        @Override
        public Iterator iterator() {
            return new PowerSetIterator( this );
        }
    }
}
