package zoomba.lang.core.collections;
import zoomba.lang.core.types.ZTypes;
import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;

/**
 */
public class ZSortedMap implements SortedMap{

    static class EmptySortedMap extends ZSortedMap{
        @Override
        public Object put(Object key, Object value) {
            return null ;
        }
    }

    public static final EmptySortedMap EMPTY_SORTED_MAP = new EmptySortedMap();

    final Map<Object,Integer> map;

    final List<Entry> entryList;

    public ZSortedMap(){
        this(Collections.EMPTY_MAP);
    }

    public ZSortedMap(Map m ) {
        map = new HashMap<>();
        entryList = new ArrayList<>();
        int i = 0 ;
        for ( Object o : m.entrySet() ){
            Entry e = (Entry)o;
            map.put( e.getKey() , i++ );
            entryList.add(e);
        }
    }

    @Override
    public Comparator comparator() {
        return Comparator.naturalOrder();
    }

    @Override
    public SortedMap subMap(Object fromKey, Object toKey) {
        if (!map.containsKey( fromKey ) ) return EMPTY_SORTED_MAP ;
        int from = map.get( fromKey );
        if (!map.containsKey( toKey ) ) return EMPTY_SORTED_MAP ;

        int to = map.get( toKey );

        ZSortedMap m = new ZSortedMap();
        for ( int i = from ; i <= to ; i++ ){
            Entry e = entryList.get(i);
            m.put( e.getKey(), e.getValue() );
        }
        return m;
    }

    @Override
    public SortedMap headMap(Object toKey) {
        if (!map.containsKey( toKey ) ) return EMPTY_SORTED_MAP ;
        int inx = map.get( toKey );
        ZSortedMap m = new ZSortedMap();
        for ( int i = 0 ; i <= inx  ; i++ ){
            Entry e = entryList.get(i);
            m.put( e.getKey(), e.getValue() );
        }
        return m;
    }

    @Override
    public SortedMap tailMap(Object fromKey) {
        if (!map.containsKey( fromKey ) ) return EMPTY_SORTED_MAP ;
        int inx = map.get( fromKey );
        ZSortedMap m = new ZSortedMap();
        for ( int i = inx ; i < entryList.size() ; i++ ){
            Entry e = entryList.get(i);
            m.put( e.getKey(), e.getValue() );
        }
        return m;
    }

    @Override
    public Object firstKey() {
        return entryList.get(0).getKey() ;
    }

    @Override
    public Object lastKey() {
        return entryList.get( entryList.size() -1).getKey() ;
    }

    @Override
    public Set keySet() {
        Set ks = new ZSortedSet();
        for ( Entry e : entryList ){
            ks.add( e.getKey() );
        }
        return ks;
    }

    @Override
    public Collection values() {
        return new ArrayList( entryList );
    }

    @Override
    public Set<Entry> entrySet() {
        return new ZSortedSet( entryList );
    }

    @Override
    public int size() {
        return entryList.size();
    }

    @Override
    public boolean isEmpty() {
        return entryList.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        for ( Entry e : entryList ){
            if (ZTypes.equals( e.getValue() , value ) ) return true ;
        }
        return false ;
    }

    @Override
    public Object get(Object key) {
        if ( !map.containsKey(key ) ) return NIL ;
        int inx = map.get( key );
        return entryList.get(inx).getValue() ;
    }


    public Entry entry(int index) {
        try {
            return entryList.get(index);
        }catch (IndexOutOfBoundsException e){
            if ( index < 0 ){
                return entryList.get( entryList.size() + index);
            }
            return null;
        }
    }

    @Override
    public Object put(Object key, Object value) {
        if ( map.containsKey( key ) ){
            int inx = map.get( key );
            Entry e = entryList.get(inx);
            entryList.set( inx, new ZMap.Pair( key, value ) );
            return e.getValue() ;
        }
        entryList.add(new ZMap.Pair( key, value ) );
        map.put( key , entryList.size()-1 );
        return null;
    }

    private void reInitMap(){
        map.clear();
        for ( int i = 0 ; i < entryList.size(); i++ ){
            map.put( entryList.get(i).getKey(), i);
        }
    }

    @Override
    public Object remove(Object key) {
        if ( !map.containsKey(key ) ) return NIL ;
        int inx = map.get( key );
        Entry o = entryList.remove(inx);
        map.remove(key);
        reInitMap();
        return o.getValue() ;
    }

    @Override
    public void putAll(Map m) {
        for ( Object o : m.entrySet() ){
            Entry e = (Entry)o;
            put( e.getKey() , e.getValue() );
        }
    }

    @Override
    public void clear() {
        map.clear();
        entryList.clear();
    }

    @Override
    public String toString() {
        return ZTypes.jsonString( this );
    }
}
