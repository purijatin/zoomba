package zoomba.lang.parser;

/**
 */
public class ZoombaNode extends SimpleNode{
    /** A marker interface for literals.
     * @param <T> the literal type
     */
    public interface Literal<T> {
        T getLiteral();
    }
    /** token value. */
    public String image;

    public ZoombaNode(int id) {
        super(id);
    }

    public ZoombaNode(Parser p, int id) {
        super(p, id);
    }

    public String locationInfo(){
        Token t1 = jjtGetFirstToken();
        Token t2 = jjtGetLastToken();
        String locInfo = "" ;
        if ( t1.beginLine != t2.endLine ) {
            locInfo = String.format("at line %d, col %d to line %d, col %d",
                    t1.beginLine, t1.beginColumn, t2.endLine, t2.endColumn);
        }
        else{
            locInfo = String.format("at line %d, cols %d:%d",
                    t1.beginLine, t1.beginColumn,t2.endColumn);
        }
        return locInfo;
    }

    /**
     * Whether this node is a constant node
     * Its value can not change after the first evaluation and can be cached indefinitely.
     * @return true if constant, false otherwise
     */
    public final boolean isConstant() {
        return isConstant(this instanceof ZoombaNode.Literal<?>);
    }

    protected boolean isConstant(boolean literal) {
        if (literal) {
            if (children != null) {
                for (ZoombaNode child : children) {
                    if (child instanceof ASTReference) {
                        boolean is = child.isConstant(true);
                        if (!is) {
                            return false;
                        }
                    } else if (child instanceof ASTMapEntry) {
                        boolean is = child.isConstant(true);
                        if (!is) {
                            return false;
                        }
                    } else if (!child.isConstant()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
}
