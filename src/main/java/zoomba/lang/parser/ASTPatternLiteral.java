/* Generated By:JJTree: Do not edit this line. ASTPatternLiteral.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package zoomba.lang.parser;

import java.util.regex.Pattern;

public class ASTPatternLiteral extends ZoombaNode {

    Pattern pattern;

    public Pattern pattern(){ return pattern ; }

    boolean anywhere;

    public boolean anywhere(){ return anywhere ; }

    public ASTPatternLiteral(int id) {
        super(id);
    }

    public ASTPatternLiteral(Parser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. **/
    public Object jjtAccept(ParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }


    public boolean matches(String s){
        if (anywhere) {
            return pattern.matcher(s).find();
        } else {
            return pattern.matcher(s).matches();
        }
    }

    /***
     * ##xyz#globals
     * @param s
     */
    public void setPattern(String s){
        String sPat = s.substring(2);
        int endIndex = sPat.lastIndexOf('#');
        String modifiers = sPat.substring(endIndex+1);
        sPat  = sPat.substring(0,endIndex);
        anywhere = false ;
        int append = Pattern.DOTALL ;
        if ( modifiers.contains("i") ){
            append = append | Pattern.CASE_INSENSITIVE ;
        }
        if ( modifiers.contains("m") ){
            append = append | Pattern.MULTILINE ;
        }
        if ( modifiers.contains("g")){
            anywhere = true ;
        }
        pattern  = Pattern.compile(sPat,append);
    }
}
