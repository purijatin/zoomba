package zoomba.lang.parser;

import org.apfloat.Apfloat;
import org.apfloat.Apint;
import zoomba.lang.core.types.ZNumber;

/**
 */
public class ASTNumberLiteral extends ZoombaNode {

    /** The type literal value. */
    Number literal = null;

    public ASTNumberLiteral(int id) {
        super(id);
    }

    public ASTNumberLiteral(Parser p, int id) {
        super(p, id);
    }

    /** Accept the visitor. **/
    public Object jjtAccept(ParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    /**
     * Gets the literal value.
     * @return the number literal
     */
    public Number getLiteral() {
        return literal;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isConstant(boolean literal) {
        return true;
    }

    public boolean isInteger() {
        return literal instanceof Integer ;
    }

    /**
     * Sets this node as a natural literal
     * @param s the natural as string
     */
    public void setNatural(String s) {
        Apint an ;
        boolean l = s.endsWith("l");
        boolean L = s.endsWith("L") ;
        if ( l || L ){
            an = new Apint(s.substring(0,s.length()-1));
        }else{
            an = new Apint(s);
        }
        literal = an ;
        if ( L ){ return ; }
        if ( l ){
            literal = an.longValue();
            return;
        }
        literal = ZNumber.integer(an);
    }

    /**
     * Sets this node as a real literal.
     * Originally from OGNL.
     * @param s the real as string
     */
    public void setReal(String s) {
        boolean D = s.endsWith("D") ;
        if ( D ){
            literal = new Apfloat( s.substring(0,s.length()-1));
            return;
        }
        Apfloat af = new Apfloat(s);
        literal = ZNumber.floating(af);
    }
}
