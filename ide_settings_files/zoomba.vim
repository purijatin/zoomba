" Vim syntax file
" Language:    zm 
" Version:     0.1
" URL:		https://gitlab.com/non.est.sacra/zoomba/wikis/home
" Last Change: 2016 Oct 08
" Disclaimer:  It's an absolute basic, very simple and by far not finished
" syntax file! It only recognizes basic keywords and  constructs like comments
" any help is welcome

" Remove any old syntax stuff
syn clear

" syntax highlighting for words that are not identifiers:
" int unit double String Array byte short char long float
syn keyword zmExternal		import as
syn keyword zmConditional	if else where break continue poll
syn keyword zmRepeat			while for 
syn keyword zmType			bool enum int INT num float FLOAT str
syn keyword zmType			isa type del is

syn keyword zmStatement		return
syn keyword	zmBoolean		true false
syn keyword zmConstant		null
syn keyword	zmTypedef		$ $.o $.i $.p $.c @ARGS
syn keyword zmLangClass	    set mset ceil dict floor list select time load lfold rfold reduce rreduce open
syn keyword zmLangClass	    async system read write println printf json xml matrix #clock thread #atomic
syn keyword zmLangClass	    round index rindex sum minmax heap hash random shuffle assert panic test tokens

" TODO differentiate the keyword class from MyClass.class -> use a match here

syn keyword	zmOperator		new size empty def


" same number definition as in java.vim
syn match   zmNumber		"\<\(0[0-7]*\|0[xX]\x\+\|\d\+\)[lL]\=\>"
syn match   zmNumber     "\(\<\d\+\.\d*\|\.\d\+\)\([eE][-+]\=\d\+\)\=[fFdD]\="
syn match   zmNumber     "\<\d\+[eE][-+]\=\d\+[fFdD]\=\>"
syn match   zmNumber     "\<\d\+\([eE][-+]\=\d\+\)\=[fFdD]\>"

syn match   zmOperator  "!"
syn match   zmOperator  "-"
syn match   zmOperator  "+"
syn match   zmOperator  "/"
syn match   zmOperator  "%"
syn match   zmOperator  "|"
syn match   zmOperator  "&"
syn match   zmOperator  "="
syn match   zmOperator  "*"
syn match   zmOperator  "<"
syn match   zmOperator  ">"
syn match   zmOperator  "@"
syn match   zmOperator  "\~"
syn match   zmOperator  "^"
syn match   zmOperator  "\$"
syn match   zmOperator  "_"
syn match   zmOperator  ":"

syn region  zmString		start=+"+ end=+"+
syn region  zmString		start=+'+ end=+'+
syn region  zmString		start=+`+ end=+`+

" Functions
"	def [name] [(prototype)] {
"
syn match   zmFunction	"\s*\<def\>"

" Comments
syn region zmComment		start="/\*"	end="\*/"
syn match	zmLineComment	"//.*"


if !exists("did_zm_syntax_inits")
    let did_zm_syntax_inits = 1
    
    " The default methods for highlighting. Can be overridden later
    hi link zmExternal		Include
    hi link zmStatement		Statement
    hi link zmConditional	Conditional
	hi link zmRepeat			Repeat
    hi link zmType			Type
    hi link zmTypedef		Typedef
	hi link	zmBoolean		Boolean
    hi link zmFunction		Function
    hi link zmLangClass		Constant
	hi link	zmConstant		Constant
	hi link zmStorageClass 	StorageClass
	hi link zmOperator		Operator
    hi link zmNumber			Number
    hi link zmString			String
	hi link	zmComment		Comment
	hi link	zmLineComment	Comment
    hi link zmSpecial Keyword
    
endif

let b:current_syntax = "zm"

" if you want to override default methods for highlighting
"hi Conditional	term=bold ctermfg=Cyan guifg=#80a0ff
