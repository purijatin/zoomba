# The ZoomBA Programming Language 
Welcome! If you were searching for how to reduce code - for business, or for QA activities, 
you have come to the right place. 
In ZoomBA land, we are not only bothered about the lines of code,
we are even bothered about the number of characters! We promote provable,
 maintainable, minimal coding. That does not mean things are easy and it is 
 *codeless* as some marketing gimmick guys suggest. They certainly are not. 
But just like cycling, once you get hang of it, it becomes a habit.
ZoomBA promotes declarative thinking over imperative mindset.

## How to 
As of now, ZoomBA is in late Alpha. 
Alpha does not mean less quality for us. 
We are 65% branch covered, and 77% instruction covered.
And for the jargon lovers, we do follow test driven development.

![Code Coverage](cc.png)

### Start Using ZoomBA

* You need to have Java 8 runtime installed.
* Download the binary distribution from [here](https://gitlab.com/non.est.sacra/zoomba/blob/master/dist/zoomba.zip)
* UnZip the archive
* Simply run now :

     java -jar zoomba.lang.core-0.1-SNAPSHOT.one-jar.jar 
     
You should see the *(zoomba)* prompt.

### If you would like to Build

The project is built using standard maven technique.
Thus, 

     mvn clean install 

should do the trick of building and installing. 
There is no need to skip tests, albeit, one test might just fail due to 
concurrency. It is still alpha, remember?

### How to Use

Once the building is done, go to the target directory, and you can run 

     java -jar zoomba.lang.core-0.1-SNAPSHOT.one-jar.jar 

This should fire up the minimalistic REPL we have created.
Alternatively, you can alias the same command :

     alias zmb="java -jar /path/to/zoomba-jar/zoomba.lang.core-0.1-SNAPSHOT.one-jar.jar"     

Create a file hello.zm :

     println("Hello, ZoomBA!")

And now, you can use ZoomBA :

     $zmb hello.zm 
     "Hello, ZoomBA!"
     $

### Examples

The repository is rich in samples. See the samples folder in the source repo. Specifically see the test directory inside the samples folder.
They have everything - every feature ZoomBA is capable of. ZoomBA team maintains a [CareerCup profile](https://www.careercup.com/user?id=5687896666275840) where we answer standard trick algorithms in ZoomBA.
That is another starting point. Albeit only last 50 gets shown there. 

### Embedding in Other Languages

Again, see the test source code, or the Main.java file. They are descriptive enough, how to use ZoomBA from Java. As of now, JVM is the only runtime we do support. In the future, we would port the same to .NET/Mono.

### IDE Settings

We support vim, Sublime Text and Intellij Idea.
The corresponding files can be found in the source repo under 
[ide\_settings\_files](https://gitlab.com/non.est.sacra/zoomba/tree/master/ide_settings_files). That would start syntax highlighting.

### More Details and Documentation

ZoomBA is not our full time Job. This is a thankless job we are doing, 
and it would take time. A wiki is being constructed as of now, 
but we are very busy being ourselves.
But, we put up [ZoomBA99](https://gitlab.com/non.est.sacra/zoomba99)
which has samples on how to use ZoomBA for many examples.

## Licensing

ZoomBA is shipped with Apache 2. 
